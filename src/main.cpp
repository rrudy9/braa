#include <iostream>
#include <stdio.h>
#include <ctime>
#include <string.h>

#include <argtable3.h>
#include <common_project.h>

#ifdef BINARY_DIR 
std::string BD = BINARY_DIR;
#endif

#ifdef TEST_FOUND
bool testFound = true;
#else
bool testFound = false;
#endif



int main (int argc, char *argv[]){

    struct arg_lit
        * help = NULL,
        * CArrayAlgorithm = NULL,
        * CTestArrayAlgorithm = NULL,
        * StdArrayAlgorithm = NULL,
        * StdArrayTestAlgorithm = NULL,

        * CListAlgorithm = NULL,
        * CListTestAlgorithm = NULL,
        * StdListAlgorithm = NULL,
        * StdListTestAlgorithm = NULL,

        * CDoubleListAlgorithm = NULL,
        * CDoubleListTestAlgorithm = NULL,
        * StdDoubleListAlgorithm = NULL,
        * StdDoubleListTestAlgorithm = NULL,

        * CFifoAlgorithm = NULL,
        * CFifoTestAlgorithm = NULL,
        * StdFifoAlgorithm = NULL,
        * StdFifoTestAlgorithm = NULL,

        * CBinaryTreeAlgorithm = NULL,
        * CBinaryTreeTestAlgorithm = NULL,
        * CBTreeAlgorithm = NULL,
        * CBTreeTestAlgorithm = NULL,
        * CRedBlackTreeTreeAlgorithm = NULL,
        * CRedBlackTreeTestAlgorithm = NULL,

        * CCHashTableAlgorithm = NULL,
        * CCHashTableTestAlgorithm = NULL,
        * OAHashTableAlgorithm = NULL,
        * OAHashTableTestAlgorithm = NULL,

        * CAdjacencyGraphAlgorithm = NULL,
        * CAdjacencyGraphTestAlgorithm = NULL,
        * CMatrixGraphAlgorithm = NULL,
        * CMatrixGraphTestAlgorithm = NULL;

    struct arg_end* end = NULL ;


	void *argtable[] = {
		help    = arg_litn(NULL, "help", 0, 1, "display this help and exit"),
        CArrayAlgorithm = arg_litn(NULL, "array-c", 0, 1, "Access Array C algorithms implementation"),
        CTestArrayAlgorithm = arg_litn(NULL, "test-array-c", 0, 1, "Access Array C Test algorithms implementation"),
        StdArrayAlgorithm = arg_litn(NULL, "array-std", 0, 1, "Access Array Std algorithms implementation"),
        StdArrayTestAlgorithm = arg_litn(NULL, "test-array-std", 0, 1, "Access Array Std Test algorithms implementation"),

        CListAlgorithm = arg_litn(NULL, "list-c", 0, 1, "Access Linked List C algorithms implementation"),
        CListTestAlgorithm = arg_litn(NULL, "test-list-c", 0, 1, "Access Linked List C Test algorithms implementation"),
        StdListAlgorithm = arg_litn(NULL, "list-std", 0, 1, "Access Linked List Std algorithms implementation"),
        StdListTestAlgorithm = arg_litn(NULL, "test-list-std", 0, 1, "Access Linked List Std Test algorithms implementation"),

        CDoubleListAlgorithm = arg_litn(NULL, "double-list-c", 0, 1, "Access Double Linked List C algorithms implementation"),
        CDoubleListTestAlgorithm = arg_litn(NULL, "test-double-list-c", 0, 1, "Access Double Linked List C Test algorithms implementation"),
        StdDoubleListAlgorithm = arg_litn(NULL, "double-list-std", 0, 1, "Access Double Linked List Std algorithms implementation"),
        StdDoubleListTestAlgorithm = arg_litn(NULL, "test-double-list-std", 0, 1, "Access Double Linked List Std Test algorithms implementation"),

        CFifoAlgorithm = arg_litn(NULL, "fifo-c", 0, 1, "Access FIFO C algorithms implementation"),
        CFifoTestAlgorithm = arg_litn(NULL, "test-fifo-c", 0, 1, "Access FIFO C Test algorithms implementation"),
        StdFifoAlgorithm = arg_litn(NULL, "fifo-std", 0, 1, "Access FIFO Std algorithms implementation"),
        StdFifoTestAlgorithm = arg_litn(NULL, "test-fifo-std", 0, 1, "Access FIFO Std Test algorithms implementation"),

        CBinaryTreeAlgorithm = arg_litn(NULL, "binary-tree-c", 0, 1, "Access Binary Tree C algorithms implementation"),
        CBinaryTreeTestAlgorithm = arg_litn(NULL, "test-binary-tree-c", 0, 1, "Access Binary Tree C Test algorithms implementation"),
        CBTreeAlgorithm = arg_litn(NULL, "b-tree-c", 0, 1, "Access B-Tree C algorithms implementation"),
        CBTreeTestAlgorithm = arg_litn(NULL, "test-b-tree-c", 0, 1, "Access B-Tree C Test algorithms implementation"),
        CRedBlackTreeTreeAlgorithm = arg_litn(NULL, "red-black-tree-c", 0, 1, "Access Red Black Tree C algorithms implementation"),
        CRedBlackTreeTestAlgorithm = arg_litn(NULL, "test-red-black-tree-c", 0, 1, "Access Red Black Tree C Test algorithms implementation"),

        CCHashTableAlgorithm = arg_litn(NULL, "hash-table-chain-collision", 0, 1, "Access Hash Table Chaining Collision C algorithms implementation"),
        CCHashTableTestAlgorithm = arg_litn(NULL, "test-hash-table-chain-collision", 0, 1, "Access Hash Table Chaining Collision C  Test algorithms implementation"),
        OAHashTableAlgorithm = arg_litn(NULL, "hash-table-open-addressing", 0, 1, "Access Hash Table Open Adressing C algorithms implementation"),
        OAHashTableTestAlgorithm = arg_litn(NULL, "test-hash-table-open-addressing", 0, 1, "Access Hash Table Open Adressing C Test algorithms implementation"),

        CAdjacencyGraphAlgorithm = arg_litn(NULL, "graph-adj-list", 0, 1, "Access GRAPH Adjacency List algorithms implementation"),
        CAdjacencyGraphTestAlgorithm = arg_litn(NULL, "test-graph-adj-list", 0, 1, "Access GRAPH Adjacency List Test algorithms implementation"),
        CMatrixGraphAlgorithm = arg_litn(NULL, "graph-adj-mat", 0, 1, "Access GRAPH Adjacency Matrix algorithms implementation"),
        CMatrixGraphTestAlgorithm = arg_litn(NULL, "test-graph-adj-mat", 0, 1, "Access GRAPH Adjacency Matrix Test algorithms implementation"),

        end = arg_end(20)
    };

    	int exitcode = 0;
    	char progname[] = "Liste_Dev.exe";
    
    	int nerrors;
    	nerrors = arg_parse(argc,argv,argtable);
    

    /* If the parser returned any errors then display them and exit */
    if (nerrors > 0)
    {
        /* Display the error details contained in the arg_end struct.*/
        arg_print_errors(stderr, end, progname);
        fprintf(stderr,"Try '%s --help' for more information.\n", progname);
        exitcode = 1;
        goto exit;
    }

	if (argc ==1){
		printf("Please choose one of the available commands\n\n");
		arg_print_glossary(stdout, argtable, "  %-25s %s\n");
		exitcode = 0;
		goto exit;
	}

	/* special case: '--help' takes precedence over error reporting */
    if (help->count > 0)
    {
        printf("Usage: %s", progname);
        arg_print_syntax(stdout, argtable, "\n\n");
        arg_print_glossary(stdout, argtable, "  %-25s %s\n");
        exitcode = 0;
        goto exit;
    }
		
    if(CArrayAlgorithm->count > 0)
    {
        launchExecutable(CArrayAlgorithm->hdr.longopts, BD,"/array/c_version/",false);
		arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
		return 0 ; 
    }
            
    if (CTestArrayAlgorithm->count > 0)
    {
        if (testFound)
        {
            launchExecutable(CTestArrayAlgorithm->hdr.longopts, BD, "/array/c_version/", true);
        }
        else
        {
            fprintf(stderr, "Can't run tests , need to install CPPUTest\n");
        }

        arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
        return 0;
    }


    if (StdArrayAlgorithm->count > 0)
    {
        launchExecutable(StdArrayAlgorithm->hdr.longopts, BD, "/array/std_version/", false);
        arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
        return 0;
    }

    if (StdArrayTestAlgorithm->count > 0)
    {
        if (testFound)
        {
            launchExecutable(StdArrayTestAlgorithm->hdr.longopts, BD, "/array/std_version/", true);
        }
        else
        {
            fprintf(stderr, "Can't run tests , need to install CPPUTest\n");
        }

        arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
        return 0;
    }

    //////

    if (CListAlgorithm->count > 0)
    {
        launchExecutable(CListAlgorithm->hdr.longopts, BD, "/list/linked_list/c_version/", true);
        arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
        return 0;
    }

    if (CListTestAlgorithm->count > 0)
    {
        if (testFound)
        {
            launchExecutable(CListTestAlgorithm->hdr.longopts, BD, "/list/linked_list/c_version/", true);
        }
        else
        {
            fprintf(stderr, "Can't run tests , need to install CPPUTest\n");
        }

        arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
        return 0;
    }


    if (StdListAlgorithm->count > 0)
    {
        launchExecutable(StdListAlgorithm->hdr.longopts, BD, "/list/linked_list/std_version/", false);
        arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
        return 0;
    }

    if (StdListTestAlgorithm->count > 0)
    {
        if (testFound)
        {
            launchExecutable(StdListTestAlgorithm->hdr.longopts, BD, "/list/linked_list/std_version/", true);
        }
        else
        {
            fprintf(stderr, "Can't run tests , need to install CPPUTest\n");
        }

        arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
        return 0;
    }

    /////////


	
    if (CDoubleListAlgorithm->count > 0)
    {
        launchExecutable(CDoubleListAlgorithm->hdr.longopts, BD, "/list/double_linked_list/c_version/", true);
        arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
        return 0;
    }

    if (CDoubleListTestAlgorithm->count > 0)
    {
        if (testFound)
        {
            launchExecutable(CDoubleListTestAlgorithm->hdr.longopts, BD, "/list/double_linked_list/c_version/", true);
        }
        else
        {
            fprintf(stderr, "Can't run tests , need to install CPPUTest\n");
        }

        arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
        return 0;
    }


    if (StdDoubleListAlgorithm->count > 0)
    {
        launchExecutable(StdDoubleListAlgorithm->hdr.longopts, BD, "/list/double_linked_list/std_version/", false);
        arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
        return 0;
    }

    if (StdDoubleListTestAlgorithm->count > 0)
    {
        if (testFound)
        {
            launchExecutable(StdDoubleListTestAlgorithm->hdr.longopts, BD, "/list/double_linked_list/std_version/", true);
        }
        else
        {
            fprintf(stderr, "Can't run tests , need to install CPPUTest\n");
        }

        arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
        return 0;
    }

    /////////

    if (CFifoAlgorithm->count > 0)
    {
        launchExecutable(CFifoAlgorithm->hdr.longopts, BD, "/fifo/c_version/", true);
        arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
        return 0;
    }

    if (CFifoTestAlgorithm->count > 0)
    {
        if (testFound)
        {
            launchExecutable(CFifoTestAlgorithm->hdr.longopts, BD, "/fifo/c_version/", true);
        }
        else
        {
            fprintf(stderr, "Can't run tests , need to install CPPUTest\n");
        }

        arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
        return 0;
    }


    if (StdFifoAlgorithm->count > 0)
    {
        launchExecutable(StdFifoAlgorithm->hdr.longopts, BD, "/fifo/std_version/", false);
        arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
        return 0;
    }

    if (StdFifoTestAlgorithm->count > 0)
    {
        if (testFound)
        {
            launchExecutable(StdFifoTestAlgorithm->hdr.longopts, BD, "/fifo/std_version/", true);
        }
        else
        {
            fprintf(stderr, "Can't run tests , need to install CPPUTest\n");
        }

        arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
        return 0;
    }

    /////////

    if (CBinaryTreeAlgorithm->count > 0)
    {
        launchExecutable(CBinaryTreeAlgorithm->hdr.longopts, BD, "/tree/binary_tree/c_version/", true);
        arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
        return 0;
    }

    if (CBinaryTreeTestAlgorithm->count > 0)
    {
        if (testFound)
        {
            launchExecutable(CBinaryTreeTestAlgorithm->hdr.longopts, BD, "/tree/binary_tree/c_version/", true);
        }
        else
        {
            fprintf(stderr, "Can't run tests , need to install CPPUTest\n");
        }

        arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
        return 0;
    }


    if (CBTreeAlgorithm->count > 0)
    {
        launchExecutable(CBTreeAlgorithm->hdr.longopts, BD, "/tree/b_tree/c_version/", false);
        arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
        return 0;
    }

    if (CBTreeTestAlgorithm->count > 0)
    {
        if (testFound)
        {
            launchExecutable(CBTreeTestAlgorithm->hdr.longopts, BD, "/tree/b_tree/c_version/", true);
        }
        else
        {
            fprintf(stderr, "Can't run tests , need to install CPPUTest\n");
        }

        arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
        return 0;
    }

    if (CRedBlackTreeTreeAlgorithm->count > 0)
    {
        launchExecutable(CRedBlackTreeTreeAlgorithm->hdr.longopts, BD, "/tree/red_black_tree/c_version/", false);
        arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
        return 0;
    }

    if (CRedBlackTreeTestAlgorithm->count > 0)
    {
        if (testFound)
        {
            launchExecutable(CRedBlackTreeTestAlgorithm->hdr.longopts, BD, "/tree/red_black_tree/c_version/", true);
        }
        else
        {
            fprintf(stderr, "Can't run tests , need to install CPPUTest\n");
        }

        arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
        return 0;
    }


    if (CCHashTableAlgorithm->count > 0)
    {
        launchExecutable(CCHashTableAlgorithm->hdr.longopts, BD, "/hash_table/chaining_collision/c_version/", false);
        arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
        return 0;
    }

    if (CCHashTableTestAlgorithm->count > 0)
    {
        if (testFound)
        {
            launchExecutable(CCHashTableTestAlgorithm->hdr.longopts, BD, "/hash_table/chaining_collision/c_version/", true);
        }
        else
        {
            fprintf(stderr, "Can't run tests , need to install CPPUTest\n");
        }

        arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
        return 0;
    }


    if (OAHashTableAlgorithm->count > 0)
    {
        launchExecutable(OAHashTableAlgorithm->hdr.longopts, BD, "/hash_table/open_addressing/c_version/", false);
        arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
        return 0;
    }

    if (OAHashTableTestAlgorithm->count > 0)
    {
        if (testFound)
        {
            launchExecutable(OAHashTableTestAlgorithm->hdr.longopts, BD, "/hash_table/open_addressing/c_version/", true);
        }
        else
        {
            fprintf(stderr, "Can't run tests , need to install CPPUTest\n");
        }

        arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
        return 0;
    }

    if (CAdjacencyGraphAlgorithm->count > 0)
    {
        launchExecutable(CAdjacencyGraphAlgorithm->hdr.longopts, BD, "/graph/adjacency_list/c_version/", false);
        arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
        return 0;
    }

    if (CAdjacencyGraphTestAlgorithm->count > 0)
    {
        if (testFound)
        {
            launchExecutable(CAdjacencyGraphTestAlgorithm->hdr.longopts, BD, "/graph/adjacency_list/c_version/", true);
        }
        else
        {
            fprintf(stderr, "Can't run tests , need to install CPPUTest\n");
        }

        arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
        return 0;
    }


    if (CMatrixGraphAlgorithm->count > 0)
    {
        launchExecutable(CMatrixGraphAlgorithm->hdr.longopts, BD, "/graph/adjacency_matrix/c_version/", false);
        arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
        return 0;
    }

    if (CMatrixGraphTestAlgorithm->count > 0)
    {
        if (testFound)
        {
            launchExecutable(CMatrixGraphTestAlgorithm->hdr.longopts, BD, "/graph/adjacency_matrix/c_version/", true);
        }
        else
        {
            fprintf(stderr, "Can't run tests , need to install CPPUTest\n");
        }

        arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
        return 0;
    }





    
exit:
    /* deallocate each non-null entry in argtable[] */
    arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
    return exitcode;


}
