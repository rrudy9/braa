#include "../include/binary_tree_deletion_c.h"


void deleteSelectedValue(struct binary_tree** tree, int valToDelete, uint64_t& computeVal)
{
	++computeVal;

	if (!(*tree))
	{
		printf("Value not found\n");
		return; 
	}

	if ((*tree)->data > valToDelete)
		deleteSelectedValue(&(*tree)->left, valToDelete, computeVal);

	else if ((*tree)->data < valToDelete)
		deleteSelectedValue(&(*tree)->right, valToDelete, computeVal);


	if ((*tree)->data == valToDelete)
	{
		if ((*tree)->left == NULL)
		{
			struct binary_tree* listChain = (*tree)->right;
			free(*tree);
			*tree = listChain; 
			return;
		}
		else if ((*tree)->right == NULL)
		{
			struct binary_tree* listChain = (*tree)->left;
			free(*tree);
			*tree = listChain; 
			return;
		}

		struct binary_tree* takeMinValueFromRight = minValueNode((*tree)->right);

		(*tree)->data = takeMinValueFromRight->data;
		deleteSelectedValue(&(*tree)->right, takeMinValueFromRight->data, computeVal);

	}

}


void implementedDeletion(struct binary_tree** tree, int valToDelete) 
{
	size_t timeElasped = 0;
	uint64_t computeVal = 0;

	if (tree != NULL)
	{
		if (global_log_level == logLevel::debug)
		{
			printf("before deletion");
			uint64_t cc = 0;
			showPreFixPath(*tree, cc);
		}

		getTimeLapsComputeVariables(timeElasped, deleteSelectedValue, tree, valToDelete, computeVal);
		
		showComputeElapsedTime(computeVal, timeElasped, "Binary Tree Deletion", global_show_compute, global_log_level);

		if (global_log_level == logLevel::debug)
		{
			printf("after deletion");
			uint64_t cc = 0;
			showPreFixPath(*tree, cc);
		}

	}
	else
	{
		printf("Try To insert a value first, your binary tree is empty!\n");
	}

}
