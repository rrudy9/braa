#pragma once 

#include <binary_tree_common_c.h>


#ifdef TEST_FOUND 
#include "CppUTest/MemoryLeakDetectorMallocMacros.h"
#endif


void deleteSelectedValue(struct binary_tree** tree, int valToDelete, uint64_t& computeVal);

void implementedDeletion(struct binary_tree** tree, int valToDelete);