file(GLOB binary_tree_deletion_c 
	.cxx .hxx 
	src/*.cxx 
	include/*.h)

add_library(binary_tree_deletion_c_lib ${binary_tree_deletion_c})


set(linkingLibrary 
	${CPPUTEST_LDFLAGS}  
	binary_tree_common_c_lib
	common_project_lib
	)

set(linkingDirectories 
	"${CPPUTEST_INCLUDE_DIRS}"
	"${PROJECT_SOURCE_DIR}/tree/binary_tree/C_Version/common/include" 
	"${PROJECT_SOURCE_DIR}/common_project/include" 

	)

	
if (CPPUTEST_LIBRARIES)
	add_compile_definitions(TEST_FOUND="test")
endif(CPPUTEST_LIBRARIES)

target_link_libraries(binary_tree_deletion_c_lib PRIVATE ${linkingLibrary})

target_include_directories(binary_tree_deletion_c_lib PRIVATE ${linkingDirectories})

set_property(TARGET binary_tree_deletion_c_lib PROPERTY FOLDER "tree/binary_tree/C_Version/deletion")
