#include <stdlib.h>
#include <stdio.h>
#include <common_project.h>
#include <binary_tree_search_c.h>
#include <binary_tree_insertion_c.h>
#include <binary_tree_deletion_c.h>
#include <binary_tree_common_c.h>
#include <argtable3.h>
#include <vector>

#ifdef TEST_FOUND 
#include "CppUTest/MemoryLeakDetectorMallocMacros.h"
#endif

bool global_show_compute = false;
logLevel global_log_level = logLevel::info;

void getElementIntTree(std::vector<int>& stdVector, struct binary_tree* tree);

int main(int argc, char* argv[])
{
	
	struct arg_lit
		* help = NULL,
		* showCompute = NULL,
		* checkResultValidation = NULL,
		* prefixPath = NULL,
		* infixPath = NULL,
		* postfixPath = NULL;

	struct arg_str
		* logLevelValue = NULL;

	struct arg_int
		* setElementCount = NULL,
		* setRandSeedValue = NULL,
		* search = NULL,
		* insertion = NULL,
		* deletion = NULL;

	struct arg_end* end = NULL;
	
	struct Common_Project_Values binaryTreeC = { 100000 , 30 };
	struct binary_tree* binaryTree = NULL;

	void* argtable[] = {
		help = arg_litn(NULL, "help", 0, 1, "display this help for Binary Tree <C Version> and exit"),
		showCompute = arg_litn("c","show-compute" ,0, 1, "Show Compute"),
		checkResultValidation = arg_litn("t","run-test" ,0, 1, "Run Test for implemented algorithms"),
		logLevelValue = arg_strn("v", "verbose","<info,debug,error,fatal>", 0, 1, "Set Log Level (info, debug, error, fatal) (Default : info)"),
		setElementCount = arg_intn("b", "binary-tree-count","<n>" , 0, 1, "Set Binary Tree Number of element (Default Value: 100k),  Doesn't work when n = 0"),
		setRandSeedValue = arg_intn("r", "rand-seed","<n>", 0, 1, "Set value for Seed Random (Default Value: 30)"),

		search = arg_intn("s", "search","<value>", 0, 1, "Access Binary Tree C Search Algorithm implementation"),
		insertion = arg_intn("i", "insertion", "<value>" ,0, 1, "Access Binary Tree C Insertion Algorithm implementation"),
		deletion = arg_intn("d", "deletion","<value>", 0, 1, "Access Binary Tree C Deletion Algorithm implementation"),
		prefixPath = arg_litn("p", "prefix", 0, 1, "display your binary tree in a prefix way"),
		infixPath = arg_litn("f", "infix", 0, 1, "display your binary tree in a infix way"),
		postfixPath = arg_litn("x", "postfix", 0, 1, "display your binary tree in a post fix way"),

		end = arg_end(20)
	};


	int exitcode = 0;
	char progname[] = "binaryTreeC.exe";

	int nerrors;
	nerrors = arg_parse(argc, argv, argtable);

	if (argc == 1) {
		printf("Please choose one of the available commands\n\n");
		arg_print_glossary(stdout, argtable, "  %-25s %s\n");
		exitcode = 0;
		goto exit;
	}

	//If the parser returned any errors then display them and exit 
	if (nerrors > 0)
	{
		// Display the error details contained in the arg_end struct.
		arg_print_errors(stderr, end, progname);
		fprintf(stderr,"Try '%s --help' for more information.\n", progname);
		exitcode = 1;
		goto exit;
	}


	if (help->count > 0)
	{
		printf("Usage: %s", progname);
		arg_print_syntax(stdout, argtable, "\n\n");
		arg_print_glossary(stdout, argtable, "  %-25s %s\n");
		exitcode = 0;
		goto exit;
	}


	if (showCompute->count > 0)
	{
		global_show_compute = !global_show_compute;
	}

	if (logLevelValue->count > 0)
	{
		setLogLevel(*(logLevelValue)->sval, global_log_level);
	}

	//set project common value (random seed , and number of element in array
	if (setElementCount->count > 0)
	{
		binaryTreeC.elementCount = *(setElementCount)->ival;
	}

	if (setRandSeedValue->count > 0)
	{
		binaryTreeC.seedRand = *(setRandSeedValue)->ival;
	}

	srand(binaryTreeC.seedRand);
	showCommonProjectValue(binaryTreeC, global_log_level);
	createBinaryTree(&binaryTree, binaryTreeC); 


	if (search->count > 0)
	{
		implementedSearch(binaryTree, *(search)->ival);
	}

	if (insertion->count > 0)
	{
		implementedInsertion(&binaryTree, *(insertion)->ival);
	}

	if (deletion->count > 0)
	{
		implementedDeletion(&binaryTree, *(deletion)->ival);
	}

	if (checkResultValidation->count > 0)
	{
		std::vector<int> vectorForTesting = {};
		getElementIntTree(vectorForTesting, binaryTree);

		int indexOfPickUpRandomValue = rand() % (vectorForTesting.size() - 1);
		int pickUpRandomValue = vectorForTesting[indexOfPickUpRandomValue];

		uint64_t cc = 0; 
		
		deleteSelectedValue(&binaryTree, pickUpRandomValue, cc);
		
		//cc = 0;
		//if (getSearchedValue(binaryTree, pickUpRandomValue, cc))
	//	{
		//	fprintf(stderr, "Value found after deletion,  Error in deletion code\n");
		//	destroyBinaryTree(binaryTree);
		//	goto exit;
		//}

		std::vector<int> elementInTreeAfterRunningAlgorithm = {};
		getElementIntTree(elementInTreeAfterRunningAlgorithm, binaryTree);

		for (int i = 0; i < elementInTreeAfterRunningAlgorithm.size(); ++i)
		{
			if (i < indexOfPickUpRandomValue)
			{
				if (elementInTreeAfterRunningAlgorithm[i] != vectorForTesting[i])
				{
					fprintf(stderr, "Values are not the same , Error in deletion code\n");
					destroyBinaryTree(binaryTree);
					goto exit; 
				}
			}
			else
			{
				if (elementInTreeAfterRunningAlgorithm[i] != vectorForTesting[i + 1])
				{
					fprintf(stderr, "Values are not the same , Error in deletion code\n");
					destroyBinaryTree(binaryTree);
					goto exit;
				}
			}
		}
		
		cc = 0; 
		insertValue(&binaryTree, pickUpRandomValue, cc);

		//cc = 0;
	//	if (!getSearchedValue(binaryTree, pickUpRandomValue, cc))
	//	{
		///	fprintf(stderr, "Value not found after insertion,  Error in insertion code\n");
		//	destroyBinaryTree(binaryTree);
		//	goto exit;
		//}

		elementInTreeAfterRunningAlgorithm = {};
		getElementIntTree(elementInTreeAfterRunningAlgorithm, binaryTree);

		for (int i = 0; i < elementInTreeAfterRunningAlgorithm.size(); ++i)
		{
			if (elementInTreeAfterRunningAlgorithm[i] != vectorForTesting[i])
			{
				fprintf(stderr, "Values are not the same , Error in insertion code\n");
				destroyBinaryTree(binaryTree);
				goto exit;
			}
		}
		  
		printf("Test Success for Deletion, Insertion \n");



	}


	if (prefixPath->count > 0)
	{
		uint64_t compute = 0;
		size_t timeElapsed = 0; 
		getTimeLapsComputeVariables(timeElapsed, showPreFixPath, binaryTree, compute);
		printf("\n");
		showComputeElapsedTime(compute, timeElapsed, "PreFix Path", global_show_compute, global_log_level);
	}


	if (infixPath->count > 0)
	{
		uint64_t compute = 0;
		size_t timeElapsed = 0;
		getTimeLapsComputeVariables(timeElapsed, showInFixPath, binaryTree, compute);
		printf("\n");

		showComputeElapsedTime(compute, timeElapsed, "Infix Path", global_show_compute, global_log_level);
	}

	if (postfixPath->count > 0)
	{
		uint64_t compute = 0;
		size_t timeElapsed = 0;
		getTimeLapsComputeVariables(timeElapsed, showPostFixPath, binaryTree, compute);
		printf("\n");

		showComputeElapsedTime(compute, timeElapsed, "PostFix Path", global_show_compute, global_log_level);
	}


	arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
	destroyBinaryTree(binaryTree);

	return exitcode;

exit:
	/* deallocate each non-null entry in argtable[] */
	arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
	return exitcode;

}

void getElementIntTree(std::vector<int>& stdVector, struct binary_tree* tree)
{
	if (tree)
	{
		getElementIntTree(stdVector, tree->left);
		stdVector.push_back(tree->data);
		getElementIntTree(stdVector, tree->right);
	}
}