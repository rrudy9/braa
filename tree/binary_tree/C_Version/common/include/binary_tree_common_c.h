#ifndef BINARY_TREE_COMMON_C
#define BINARY_TREE_COMMON_C

#include <common_project.h>
#include <chrono>

#ifdef TEST_FOUND 
#include "CppUTest/MemoryLeakDetectorMallocMacros.h"
#endif

struct binary_tree
{
	int data;
	struct binary_tree* right;
	struct binary_tree* left;
};

void createBinaryTree(struct binary_tree** tree, struct Common_Project_Values projectVal);

void showPreFixPath(struct binary_tree* tree, uint64_t& computeVal);

void showInFixPath(struct binary_tree* tree, uint64_t& computeVal);

void showPostFixPath(struct binary_tree* tree, uint64_t& computeVal);

void destroyBinaryTree(struct binary_tree* tree);

struct binary_tree* minValueNode(struct binary_tree* node);

void getTimeLapsComputeVariables(size_t& elapsedTime, void (*f)(struct binary_tree**, int, uint64_t&), struct binary_tree** tree, int val, uint64_t& computeVal);
void getTimeLapsComputeVariables(size_t& elapsedTime, void (*f)(struct binary_tree*, uint64_t&), struct binary_tree* tree, uint64_t& computeVal);
bool getTimeLapsComputeVariables(size_t& elapsedTime, bool (*f)(struct binary_tree*, int, uint64_t&), struct binary_tree* tree, int val, uint64_t& computeVal);

int binaryTreeCount(struct binary_tree* tree);


#endif