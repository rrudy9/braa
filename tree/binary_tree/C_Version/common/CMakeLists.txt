file(GLOB binary_tree_common_c 
	.cxx .hxx 
	src/*.cxx 
	include/*.h)

add_library(binary_tree_common_c_lib ${binary_tree_common_c})


set(linkingLibrary 
	${CPPUTEST_LDFLAGS}  
	common_project_lib 
	binary_tree_insertion_c_lib
	)

set(linkingDirectories 
	"${CPPUTEST_INCLUDE_DIRS}"
	"${PROJECT_SOURCE_DIR}/tree/binary_tree/C_Version/insertion/include"
	"${PROJECT_SOURCE_DIR}/common_project/include" 
	)

	
if (CPPUTEST_LIBRARIES)
	add_compile_definitions(TEST_FOUND="test")
endif(CPPUTEST_LIBRARIES)




target_link_libraries(binary_tree_common_c_lib PRIVATE ${linkingLibrary})


target_include_directories(binary_tree_common_c_lib PRIVATE ${linkingDirectories})

set_property(TARGET binary_tree_common_c_lib PROPERTY FOLDER "tree/binary_tree/C_Version/common")
