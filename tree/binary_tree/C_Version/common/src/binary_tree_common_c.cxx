#include "../include/binary_tree_common_c.h"
#include <binary_tree_insertion_c.h>

void createBinaryTree(struct binary_tree** tree, struct Common_Project_Values projectVal)
{
	for (int i = 0; i < projectVal.elementCount; ++i)
	{
		uint64_t cc = 0;
		int randVal = rand() % 10001;
		insertValue(tree, randVal, cc);
	}

}

void showPreFixPath(struct binary_tree* tree, uint64_t& computeVal)
{
	++computeVal;

	if (tree)
	{
		printf("%i,", tree->data);
		showPreFixPath(tree->left, computeVal);
		showPreFixPath(tree->right, computeVal);
	}
}


void showInFixPath(struct binary_tree* tree, uint64_t& computeVal)
{
	++computeVal;

	if (tree)
	{
		showInFixPath(tree->left, computeVal);
		printf("%i,", tree->data);
		showInFixPath(tree->right, computeVal);
	}
}


void showPostFixPath(struct binary_tree* tree, uint64_t& computeVal)
{
	++computeVal;

	if (tree)
	{
		showPostFixPath(tree->left, computeVal);
		showPostFixPath(tree->right, computeVal);
		printf("%i,", tree->data);
	}
}


void destroyBinaryTree(struct binary_tree* tree)
{
	if (tree)
	{
		destroyBinaryTree(tree->left);
		destroyBinaryTree(tree->right);
		free(tree);
	}
}

struct binary_tree* minValueNode(struct binary_tree* node)
{
	struct binary_tree* cur = node;

	while (cur && cur->left != NULL)
		cur = cur->left;

	return cur;

}

void getTimeLapsComputeVariables(size_t& elapsedTime, void (*f)(struct binary_tree**, int, uint64_t&), struct binary_tree** tree, int val, uint64_t& computeVal)
{
	std::chrono::steady_clock::time_point t1 = std::chrono::high_resolution_clock::now();
	f(tree, val, computeVal);
	std::chrono::steady_clock::time_point t2 = std::chrono::high_resolution_clock::now();

	elapsedTime = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();
}


void getTimeLapsComputeVariables(size_t& elapsedTime, void (*f)(struct binary_tree*, uint64_t&), struct binary_tree* tree, uint64_t& computeVal)
{
	std::chrono::steady_clock::time_point t1 = std::chrono::high_resolution_clock::now();
	f(tree, computeVal);
	std::chrono::steady_clock::time_point t2 = std::chrono::high_resolution_clock::now();

	elapsedTime = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();
}

bool getTimeLapsComputeVariables(size_t& elapsedTime, bool (*f)(struct binary_tree*, int, uint64_t&), struct binary_tree* tree, int val, uint64_t& computeVal)
{
	bool returnVal = false; 
	std::chrono::steady_clock::time_point t1 = std::chrono::high_resolution_clock::now();
	returnVal = f(tree, val, computeVal);
	std::chrono::steady_clock::time_point t2 = std::chrono::high_resolution_clock::now();

	elapsedTime = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();
	
	return returnVal; 
}





int binaryTreeCount(struct binary_tree* tree)
{
	int c = 1;             //Node itself should be counted
	if (tree == NULL)
		return 0;
	else
	{
		c += binaryTreeCount(tree->left);
		c += binaryTreeCount(tree->right);
		return c;
	}

}