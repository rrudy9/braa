#include "../include/binary_tree_search_c.h"

bool getSearchedValue(struct binary_tree* tree, int val, uint64_t& computeVal)
{
	++computeVal;
	if (tree == NULL)
		return false;


	if (tree->data == val)
		return true;

	if (val < tree->data)
		return getSearchedValue(tree->left, val, computeVal);
	else
		return getSearchedValue(tree->right, val, computeVal);

}





void implementedSearch(struct binary_tree* tree, int valToSearch)
{
	size_t timeElasped = 0;
	uint64_t computeVal = 0;
	if (tree != NULL)
	{
		if (getTimeLapsComputeVariables(timeElasped, getSearchedValue, tree, valToSearch, computeVal))
		{
			if (global_log_level <= logLevel::debug)
				printf("Value found\n");

			if (global_log_level == logLevel::debug)
			{
				uint64_t cc = 0;
				showPreFixPath(tree,cc );
			}

			showComputeElapsedTime(computeVal, timeElasped, "Search Successfull", global_show_compute, global_log_level);

		}
		else
		{
			if (global_log_level <= logLevel::debug)
				printf("Value not found\n");

			if (global_log_level == logLevel::debug)
			{
				uint64_t cc = 0;
				showPreFixPath(tree, cc);
			}

			showComputeElapsedTime(computeVal, timeElasped, "Search Failed", global_show_compute, global_log_level );
		}
	}
	else
	{
		printf("Try To insert a value first, your binary tree is empty!\n");
	}
}


