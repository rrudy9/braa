#pragma once

#include <binary_tree_common_c.h>
#include <common_project.h>

#ifdef TEST_FOUND 
#include "CppUTest/MemoryLeakDetectorMallocMacros.h"
#endif

bool getSearchedValue(struct binary_tree* tree, int val, uint64_t& computeVal);

void implementedSearch(struct binary_tree* tree, int valToSearch);

