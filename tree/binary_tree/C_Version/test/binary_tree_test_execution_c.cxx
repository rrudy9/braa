#include "CppUTest/TestHarness.h"
#include "CppUTest/MemoryLeakDetectorMallocMacros.h"

#include <common_project.h>
#include <binary_tree_search_c.h>
#include <binary_tree_insertion_c.h>
#include <binary_tree_deletion_c.h>


TEST_GROUP(TEST_BINARY_TREE_C)
{
	struct Common_Project_Values binaryTreeC = { global_test_count_element, global_test_rand_seed };
	struct binary_tree* binaryTree = NULL;

	void setup()
	{
		showCommonProjectValue(binaryTreeC, global_log_level);
		createBinaryTree(&binaryTree, binaryTreeC);
	}

	void teardown()
	{
		destroyBinaryTree(binaryTree);
	}
};



TEST(TEST_BINARY_TREE_C, inFix)
{
	uint64_t cc = 0;
	size_t timeElapsed = 0;
	getTimeLapsComputeVariables(timeElapsed, showInFixPath, binaryTree, cc);

	showComputeElapsedTime(cc, timeElapsed, "Infix Path", global_show_compute, global_log_level);


}


TEST(TEST_BINARY_TREE_C, preFix)
{

	uint64_t cc = 0;
	size_t timeElapsed = 0;
	getTimeLapsComputeVariables(timeElapsed, showPreFixPath, binaryTree, cc);

	showComputeElapsedTime(cc, timeElapsed, "PreFix Path", global_show_compute, global_log_level);

}
TEST(TEST_BINARY_TREE_C, postFix)
{
	uint64_t cc = 0;
	size_t timeElapsed = 0;
	getTimeLapsComputeVariables(timeElapsed, showPostFixPath, binaryTree, cc);

	showComputeElapsedTime(cc, timeElapsed, "PostFix Path", global_show_compute, global_log_level);

}



TEST(TEST_BINARY_TREE_C, deletion)
{
	int valToDelete = rand() % 10011 + 1;

	if (global_log_level <= logLevel::debug)
		printf("val to delete %i\n", valToDelete);

	implementedDeletion(&binaryTree, valToDelete);

	CHECK(binaryTreeCount(binaryTree) <= binaryTreeC.elementCount);

}

TEST(TEST_BINARY_TREE_C, search)
{
	int valToSearch = rand() % 10011 + 1;

	if (global_log_level <= logLevel::debug)
		printf("val to search %i\n", valToSearch);

	implementedSearch(binaryTree, valToSearch);

	FAIL("force failing for search on purpose");

}

TEST(TEST_BINARY_TREE_C, insertion)
{
	int valToInsert = rand() % 10011 + 1;

	if (global_log_level <= logLevel::debug)
		printf("val to insert %i\n", valToInsert);

	implementedInsertion(&binaryTree, valToInsert);

	CHECK(binaryTreeCount(binaryTree) >= binaryTreeC.elementCount);
}

