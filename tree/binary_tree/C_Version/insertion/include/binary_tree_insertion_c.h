
#pragma once

#include <common_project.h>

#ifdef TEST_FOUND 
#include "CppUTest/MemoryLeakDetectorMallocMacros.h"
#endif

struct binary_tree;

void insertValue(struct binary_tree** tree, int val, uint64_t& computeVal);

void implementedInsertion(struct binary_tree** tree, int valToInsert);