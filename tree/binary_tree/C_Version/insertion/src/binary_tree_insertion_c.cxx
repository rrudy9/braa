#include "../include/binary_tree_insertion_c.h"
#include <binary_tree_common_c.h>

void insertValue(struct binary_tree** tree, int val, uint64_t& computeVal)
{
	++computeVal;

	if (!*tree)
	{
		struct binary_tree* newNode = (struct binary_tree*)malloc(sizeof(struct binary_tree));

		if (!newNode)
		{
			fprintf(stderr, "can't allocate your binary tree insertion node");
			exit(0);
		}

		newNode->data = val;
		newNode->left = newNode->right = NULL;
		*tree = newNode;
		return;
	}

	if (val < (*tree)->data)
	{
		insertValue(&(*tree)->left, val, computeVal);
	}
	else
	{
		insertValue(&(*tree)->right, val, computeVal);
	}
}


void implementedInsertion(struct binary_tree** tree, int valToInsert)
{
	size_t timeElasped = 0;
	uint64_t computeVal = 0;

	if (global_log_level == logLevel::debug)
	{
		printf("before insertion");
		uint64_t cc = 0;
		showPreFixPath(*tree, cc);
	}
	getTimeLapsComputeVariables(timeElasped, insertValue, tree, valToInsert, computeVal);

	if (global_log_level == logLevel::debug)
	{
		printf("after insertion");
		uint64_t cc = 0;
		showPreFixPath(*tree, cc);
	}

	showComputeElapsedTime(computeVal, timeElasped, "Binary Tree Insertion", global_show_compute, global_log_level);
	
}
