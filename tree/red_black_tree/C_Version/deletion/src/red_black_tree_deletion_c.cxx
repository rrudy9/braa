#include "../include/red_black_tree_deletion_c.h"


void rb_delete_fixup(struct red_black_tree* tree, struct tree_node* node, uint64_t& computeVal)
{
	++computeVal;

	while (node != tree->root && node->color == COLOR::Black)
	{
		++computeVal;

		if (node == node->parent->left)
		{
			struct tree_node* tmp = node->parent->right;
			if (tmp->color == COLOR::Red)
			{
				tmp->color = COLOR::Black;
				node->parent->color = COLOR::Red;
				leftRotate(tree, node->parent, computeVal);
				tmp = node->parent->right;
			}
			if (tmp->left->color == COLOR::Black && tmp->right->color == COLOR::Black)
			{
				tmp->color = COLOR::Red;
				node = node->parent;
			}
			else
			{
				if (tmp->right->color == COLOR::Black)
				{
					tmp->left->color = COLOR::Black;
					tmp->color = COLOR::Red;
					rightRotate(tree, tmp, computeVal);
					tmp = node->parent->right;
				}
				tmp->color = node->parent->color;
				node->parent->color = COLOR::Black;
				tmp->right->color = COLOR::Black;
				leftRotate(tree, node->parent, computeVal);
				node = tree->root;
			}
		}
		else
		{
			struct tree_node* tmp = node->parent->left;
			if (tmp->color == COLOR::Red)
			{
				tmp->color = COLOR::Black;
				node->parent->color = COLOR::Red;
				rightRotate(tree, node->parent,computeVal);
				tmp = node->parent->left;
			}
			if (tmp->right->color == COLOR::Black && tmp->left->color == COLOR::Black)
			{
				tmp->color = COLOR::Red;
				node = node->parent;
			}
			else
			{
				if (tmp->left->color == COLOR::Black)
				{
					tmp->right->color = COLOR::Black;
					tmp->color = COLOR::Red;
					leftRotate(tree, tmp, computeVal);
					tmp = node->parent->left;
				}
				tmp->color = node->parent->color;
				node->parent->color = COLOR::Black;
				tmp->left->color = COLOR::Black;
				rightRotate(tree, node->parent,computeVal);
				node = tree->root;
			}
		}
	}
	node->color = COLOR::Black;
}

void rb_delete(struct red_black_tree* tree, struct tree_node* node, uint64_t& computeVal)
{
	struct tree_node* tmp = node;
	struct tree_node* nNode;
	enum COLOR yOriginalColor = tmp->color;
	++computeVal;

	if (tmp->left == tree->NIL)
	{
		nNode = tmp->right;
		rb_transplant(tree, node, node->right, computeVal);
	}
	else if (node->right == tree->NIL)
	{
		nNode = node->left;
		rb_transplant(tree, node, node->left, computeVal);
	}
	else
	{
		tmp = minimum(tree, node->right, computeVal);
		yOriginalColor = tmp->color;
		nNode = tmp->right;
		if (tmp->parent == node)
		{
			nNode->parent = node;
		}
		else
		{

			rb_transplant(tree, tmp, tmp->right, computeVal);
			tmp->right = node->right;
			tmp->right->parent = tmp;
		}
		rb_transplant(tree, node, tmp, computeVal);
		tmp->left = node->left;
		tmp->left->parent = tmp;
		tmp->color = node->color;
	}
	if (yOriginalColor == COLOR::Black)
		rb_delete_fixup(tree, nNode, computeVal);

}

void implementedDeletion(struct red_black_tree* rBT, int valToDelete)
{
	size_t timeElasped = 0;
	uint64_t computeVal = 0;

	if (rBT->root != NULL)
	{
		struct tree_node* searchedNode = searchValue(rBT->root, valToDelete, computeVal);
		if (searchedNode)
		{
			if (global_log_level <= logLevel::debug)
				printf("found\n");

			getTimeLapsComputeVariables(timeElasped, rb_delete, rBT, searchedNode, computeVal);

			free(searchedNode);
		}
		else
		{
			if (global_log_level <= logLevel::debug)
				printf("not found\n");
		}
		
		showComputeElapsedTime(computeVal, timeElasped, "Deletion Red-BLack-tree", global_show_compute, global_log_level);

		if (global_log_level == logLevel::debug)
		{
			printf("check your b-tree (infix order) after deletion\n");
			printInOrder(rBT, (rBT)->root);
			printf("\n");

		}
	}
	else
	{
		printf("Try To insert a value first, your rbtree is empty!\n");
	}

}