#pragma once 

#include <common_project.h>
#include <red_black_tree_common_c.h>
#include <red_black_tree_search_c.h>

#ifdef TEST_FOUND 
#include "CppUTest/MemoryLeakDetectorMallocMacros.h"
#endif

void rb_delete_fixup(struct red_black_tree* tree, struct tree_node* node, uint64_t& computeVal);

void rb_delete(struct red_black_tree* tree, struct tree_node* node, uint64_t& computeVal);

void implementedDeletion(struct red_black_tree* rBT, int valToDelete);

