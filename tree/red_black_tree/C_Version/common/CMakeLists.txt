file(GLOB red_black_tree_common_c 
	.cxx .hxx 
	src/*.cxx 
	include/*.h)

add_library(red_black_tree_common_c_lib ${red_black_tree_common_c})


set(linkingLibrary 
	${CPPUTEST_LDFLAGS}  
	common_project_lib
	red_black_tree_insertion_c_lib
	)

set(linkingDirectories 
	"${CPPUTEST_INCLUDE_DIRS}"
	"${PROJECT_SOURCE_DIR}/common_project/include" 
	"${PROJECT_SOURCE_DIR}/tree/red_black_tree/C_Version/insertion/include"

	)

	
if (CPPUTEST_LIBRARIES)
	add_compile_definitions(TEST_FOUND="test")
endif(CPPUTEST_LIBRARIES)

target_link_libraries(red_black_tree_common_c_lib PRIVATE ${linkingLibrary})


target_include_directories(red_black_tree_common_c_lib PRIVATE ${linkingDirectories})

set_property(TARGET red_black_tree_common_c_lib PROPERTY FOLDER "tree/red_black_tree/C_Version/common")
