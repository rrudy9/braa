#ifndef RED_BLACK_TREE_COMMON_C
#define RED_BLACK_TREE_COMMON_C

#include <stdlib.h>
#include <stdio.h>
#include <chrono>
#include <common_project.h>

enum class COLOR { Red, Black };

struct tree_node
{
	int data;
	struct tree_node* right;
	struct tree_node* left;
	struct tree_node* parent;
	enum COLOR color;
};

struct red_black_tree
{
	struct tree_node* root;
	struct tree_node* NIL;
};


void createNode(struct tree_node** node,int data);

void createRedBlackTree(struct red_black_tree** rbTree, struct Common_Project_Values projectVal);


void leftRotate(struct red_black_tree* tree, struct tree_node* node, uint64_t& computeVal);

void rightRotate(struct red_black_tree* tree, struct tree_node* node, uint64_t& computeVal);

void rb_transplant(struct red_black_tree* tree, struct tree_node* node_1, struct tree_node* node_2, uint64_t& computeVal);

struct tree_node* minimum(struct red_black_tree* tree, struct tree_node* node, uint64_t& computeVal);

void printInOrder(struct red_black_tree* tree, struct tree_node* node);

int redBlackTreeCount(struct red_black_tree* tree, struct tree_node* node);

void freeRedBlackTree(struct red_black_tree* tree, struct tree_node* node);

void freeRBTree(struct red_black_tree* tree);

void destroyRBTree(struct red_black_tree* tree); 

void getTimeLapsComputeVariables(size_t& elapsedTime, void (*f)(struct red_black_tree*, struct tree_node*, uint64_t&), struct red_black_tree* tree, struct tree_node* node, uint64_t& computeVal);
void getTimeLapsComputeVariables(size_t& elapsedTime, void (*f)(struct red_black_tree*, int, uint64_t&), struct red_black_tree* tree, int val, uint64_t& computeVal);
struct tree_node* getTimeLapsComputeVariables(size_t& elapsedTime, struct tree_node* (*f)(struct tree_node*, int, uint64_t&), struct tree_node* tree, int val, uint64_t& computeVal);


#endif
