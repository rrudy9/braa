#include "../include/red_black_tree_common_c.h"
#include <red_black_tree_insertion_c.h>

void createNode(struct tree_node** node, int data)
{
	*node = (struct tree_node*)malloc(sizeof(tree_node));

	if (!(*node))
	{
		fprintf(stderr, "Can't allocate your node");
		exit(0);
	}

	(*node)->data = data;
	(*node)->left = NULL;
	(*node)->right = NULL;
	(*node)->parent = NULL;
	(*node)->color = COLOR::Red;
}

void createRedBlackTree(struct red_black_tree** rbTree, struct Common_Project_Values projectVal)
{
	*rbTree = (struct red_black_tree*)malloc(sizeof(struct red_black_tree));

	if (!(*rbTree))
	{
		fprintf(stderr, "Can't allocate your rb tree");
		exit(0);
	}

	struct tree_node* nilNode = (struct tree_node*)malloc(sizeof(struct tree_node));


	if (!nilNode)
	{
		fprintf(stderr, "Can't allocate your nil node");
		exit(0);
	}

	nilNode->left = NULL;
	nilNode->right = NULL;
	nilNode->parent = NULL;
	nilNode->color = COLOR::Black;
	nilNode->data = 0;
	(*rbTree)->NIL = nilNode;
	(*rbTree)->root = (*rbTree)->NIL;

	for (int i = 0; i < projectVal.elementCount; ++i)
	{
		int randVal = rand() % 10001;
		uint64_t cc = 0;
		insertValue(*rbTree, randVal, cc);
	}
}


void leftRotate(struct red_black_tree* tree, struct tree_node* node, uint64_t& computeVal)
{

	struct tree_node* tmp = node->right;
	node->right = tmp->left;
	++computeVal;

	if (tmp->left != tree->NIL)
	{
		tmp->left->parent = node;
	}

	tmp->parent = node->parent;

	// in case x is root
	if (node->parent == tree->NIL)
	{
		tree->root = tmp;
	}
	else if (node == node->parent->left)
	{
		//if x is left child
		node->parent->left = tmp;
	}
	else
	{	//x is right child
		node->parent->right = tmp;
	}

	tmp->left = node;
	node->parent = tmp;

}



void rightRotate(struct red_black_tree* tree, struct tree_node* node, uint64_t& computeVal)
{
	struct tree_node* tmp = node->left;
	node->left = tmp->right;

	++computeVal;

	if (tmp->right != tree->NIL)
	{
		tmp->right->parent = node;
	}
	tmp->parent = node->parent;

	if (node->parent == tree->NIL)
	{
		tree->root = tmp;	
	}
	else if (node == node->parent->right)
	{
		node->parent->right = tmp;
	}
	else
	{
		node->parent->left = tmp;
	}

	tmp->right = node;
	node->parent = tmp;
}


void rb_transplant(struct red_black_tree* tree, struct tree_node* node_1, struct tree_node* node_2, uint64_t& computeVal)
{
	++computeVal;

	if (node_1->parent == tree->NIL)
	{
		tree->root = node_2;
	}
	else if (node_1 == node_1->parent->left)
	{
		node_1->parent->left = node_2;
	}
	else
	{
		node_1->parent->right = node_2;
	}

	node_2->parent = node_1->parent;

}


struct tree_node* minimum(struct red_black_tree* tree, struct tree_node* node, uint64_t& computeVal)
{
	while (node->left != tree->NIL)
	{
		++computeVal;
		
		node = node->left;

	}

	return node;
}


void printInOrder(struct red_black_tree* tree, struct tree_node* node)
{
	if (node != tree->NIL)
	{
		printInOrder(tree, node->left);
		printf("%i,", node->data);
		printInOrder(tree, node->right);
	}

}

int redBlackTreeCount(struct red_black_tree* tree, struct tree_node* node)
{
	int c = 1;
	if (node == tree->NIL)
		return 0;
	else
	{
		c += redBlackTreeCount(tree,node->left);
		c += redBlackTreeCount(tree, node->right);
		return c;
	}

}

void freeRedBlackTree(struct red_black_tree* tree, struct tree_node* node)
{
	if (node != tree->NIL)
	{
		freeRedBlackTree(tree, node->left);
		freeRedBlackTree(tree, node->right);
		free(node);
	}
}

void freeRBTree(struct red_black_tree* tree)
{
	if (tree)
	{
		free(tree->NIL);
		free(tree);
	}
}
void destroyRBTree(struct red_black_tree* tree)
{
	freeRedBlackTree(tree, tree->root);
	freeRBTree(tree);
}

void getTimeLapsComputeVariables(size_t& elapsedTime, void (*f)(struct red_black_tree*, struct tree_node*, uint64_t&), struct red_black_tree* tree, struct tree_node* node, uint64_t& computeVal)
{

	std::chrono::steady_clock::time_point t1 = std::chrono::high_resolution_clock::now();
	f(tree, node, computeVal);
	std::chrono::steady_clock::time_point t2 = std::chrono::high_resolution_clock::now();

	elapsedTime = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();
}


void getTimeLapsComputeVariables(size_t& elapsedTime, void (*f)(struct red_black_tree*, int, uint64_t&), struct red_black_tree* tree, int val, uint64_t& computeVal)
{
	std::chrono::steady_clock::time_point t1 = std::chrono::high_resolution_clock::now();
	f(tree, val, computeVal);
	std::chrono::steady_clock::time_point t2 = std::chrono::high_resolution_clock::now();

	elapsedTime = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();
}

struct tree_node* getTimeLapsComputeVariables(size_t& elapsedTime, struct tree_node* (*f)(struct tree_node*, int, uint64_t&), struct tree_node* tree, int val, uint64_t& computeVal)
{
	struct tree_node* node = NULL; 
	std::chrono::steady_clock::time_point t1 = std::chrono::high_resolution_clock::now();
	node = f(tree, val, computeVal);
	std::chrono::steady_clock::time_point t2 = std::chrono::high_resolution_clock::now();

	elapsedTime = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();

	return node; 
}


