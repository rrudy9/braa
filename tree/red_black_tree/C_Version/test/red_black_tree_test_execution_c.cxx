#include "CppUTest/TestHarness.h"
#include "CppUTest/MemoryLeakDetectorMallocMacros.h"

#include <common_project.h>
#include <red_black_tree_search_c.h>
#include <red_black_tree_insertion_c.h>
#include <red_black_tree_deletion_c.h>
#include <red_black_tree_common_c.h>

TEST_GROUP(TEST_RED_BLACK_TREE_C)
{
	struct Common_Project_Values rBTreeCommonValue = { global_test_count_element, global_test_rand_seed };
	struct red_black_tree* tree = NULL;

	void setup()
	{
		showCommonProjectValue(rBTreeCommonValue, global_log_level);
		createRedBlackTree(&tree, rBTreeCommonValue);
	}

	void teardown()
	{
		destroyRBTree(tree);
	}
};




TEST(TEST_RED_BLACK_TREE_C, deletion)
{
	int valToDelete = rand() % 10011 + 1;

	if (global_log_level <= logLevel::debug)
		printf("val to delete %i\n", valToDelete);

	implementedDeletion(tree, valToDelete);

	CHECK(redBlackTreeCount(tree, tree->root) <= rBTreeCommonValue.elementCount);

}

TEST(TEST_RED_BLACK_TREE_C, search)
{
	int valToSearch = rand() % 10011 + 1;

	if (global_log_level <= logLevel::debug)
		printf("val to search %i\n", valToSearch);

	implementedSearch(tree, valToSearch);

	FAIL("force failing for search on purpose");

}

TEST(TEST_RED_BLACK_TREE_C, insertion)
{
	int valToInsert = rand() % 10011 + 1;

	if (global_log_level <= logLevel::debug)
		printf("val to insert %i\n", valToInsert);

	implementedInsertion(tree, valToInsert);

	CHECK(redBlackTreeCount(tree, tree->root) >= rBTreeCommonValue.elementCount);
}

