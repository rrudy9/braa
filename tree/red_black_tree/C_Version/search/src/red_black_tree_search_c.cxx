#include "../include/red_black_tree_search_c.h"


struct tree_node* searchValue(struct tree_node* node, int value, uint64_t& computeVal)
{
	++computeVal;

	if (node == NULL || node->data == value)
	{
		return node;
	}
	if (node->data < value)
	{
		return searchValue(node->right, value, computeVal);
	}

	return searchValue(node->left, value, computeVal);

}


void implementedSearch(struct red_black_tree* rBT, int valToSearch)
{
	size_t timeElasped = 0;
	uint64_t computeVal = 0;
	
	if (rBT->root != NULL)
	{
		struct tree_node* searchedNode = getTimeLapsComputeVariables(timeElasped, searchValue, rBT->root, valToSearch, computeVal);
		if (searchedNode)
		{
			if (global_log_level <= logLevel::debug)
				printf("found\n");
		}
		else
		{
			if (global_log_level <= logLevel::debug)
				printf("not found\n");
		}
		showComputeElapsedTime(computeVal, timeElasped, "Search Red-BLack-tree", global_show_compute, global_log_level);

	}
	else
	{
		printf("Try To insert a value first, your rbtree is empty!\n");
	}
}