#pragma once 

#include <common_project.h>
#include <red_black_tree_common_c.h>

#ifdef TEST_FOUND 
#include "CppUTest/MemoryLeakDetectorMallocMacros.h"
#endif

struct tree_node* searchValue(struct tree_node* node, int value, uint64_t& computeVal);

void implementedSearch(struct red_black_tree* rBT, int valToSearch);
