#include <stdlib.h>
#include <stdio.h>
#include <common_project.h>
#include <red_black_tree_search_c.h>
#include <red_black_tree_insertion_c.h>
#include <red_black_tree_deletion_c.h>
#include <red_black_tree_common_c.h>
#include <argtable3.h>
#include <vector>


#ifdef TEST_FOUND 
#include "CppUTest/MemoryLeakDetectorMallocMacros.h"
#endif

bool global_show_compute = false;
logLevel global_log_level = logLevel::info;

void getValueInTree(std::vector<int>& stdVector, struct red_black_tree* tree, struct tree_node* node);


int main(int argc, char* argv[])
{

	struct arg_lit
		* help = NULL,
		* checkResultValidation = NULL,
		* showRedBlackTree = NULL, 
		* showCompute = NULL;

	struct arg_str
		* logLevelValue = NULL;

	struct arg_int
		* setElementCount = NULL,
		* setRandSeedValue = NULL,
		* search = NULL,
		* insertion = NULL,
		* deletion = NULL;

	struct arg_end* end = NULL;

	struct Common_Project_Values rBTreeCommonValue = { 100000 , 30 };
	struct red_black_tree* tree = NULL;

	void* argtable[] = {
		help = arg_litn(NULL, "help", 0, 1, "display this help for Red Black Tree <C Version> and exit"),
		showCompute = arg_litn("c","show-compute" ,0, 1, "Show Compute"),
		checkResultValidation = arg_litn("t","run-test" ,0, 1, "Run Test for implemented algorithms"),
		logLevelValue = arg_strn("v", "verbose","<info,debug,error,fatal>", 0, 1, "Set Log Level (info, debug, error, fatal) (Default : info)"),
		setElementCount = arg_intn("b", "red-black-tree-count","<n>" , 0, 1, "Set Red Black Tree Number of element (Default Value: 100k),  Doesn't work when n = 0"),
		setRandSeedValue = arg_intn("r", "rand-seed","<n>", 0, 1, "Set value for Seed Random (Default Value: 30)"),

		search = arg_intn("s", "search","<value>", 0, 1, "Access Red Black Tree C Search Algorithm implementation"),
		insertion = arg_intn("i", "insertion", "<value>" ,0, 1, "Access Red Black Tree C Insertion Algorithm implementation"),
		deletion = arg_intn("d", "deletion","<value>", 0, 1, "Access Red Black Tree C Deletion Algorithm implementation"),
		showRedBlackTree = arg_litn("p", "show-rb-tree", 0, 1, "display your red black tree "),

		end = arg_end(20)
	};

	int exitcode = 0;
	char progname[] = "redBlackTreeC.exe";

	int nerrors;
	nerrors = arg_parse(argc, argv, argtable);

	if (argc == 1) {
		printf("Please choose one of the available commands\n\n");
		arg_print_glossary(stdout, argtable, "  %-25s %s\n");
		exitcode = 0;
		goto exit;
	}

	//If the parser returned any errors then display them and exit 
	if (nerrors > 0)
	{
		// Display the error details contained in the arg_end struct.
		arg_print_errors(stderr, end, progname);
		fprintf(stderr,"Try '%s --help' for more information.\n", progname);
		exitcode = 1;
		goto exit;
	}


	if (help->count > 0)
	{
		printf("Usage: %s", progname);
		arg_print_syntax(stdout, argtable, "\n\n");
		arg_print_glossary(stdout, argtable, "  %-25s %s\n");
		exitcode = 0;
		goto exit;
	}


	if (showCompute->count > 0)
	{
		global_show_compute = !global_show_compute;
	}

	if (logLevelValue->count > 0)
	{
		setLogLevel(*(logLevelValue)->sval, global_log_level);
	}

	//set project common value (random seed , and number of element in array
	if (setElementCount->count > 0)
	{
		rBTreeCommonValue.elementCount = *(setElementCount)->ival;
	}

	if (setRandSeedValue->count > 0)
	{
		rBTreeCommonValue.seedRand = *(setRandSeedValue)->ival;
	}
	
	srand(rBTreeCommonValue.seedRand);
	showCommonProjectValue(rBTreeCommonValue, global_log_level);
	createRedBlackTree(&tree, rBTreeCommonValue);

	if (search->count > 0)
	{
		implementedSearch(tree, *(search)->ival);
	}

	if (insertion->count > 0)
	{
		implementedInsertion(tree, *(insertion)->ival);
	}

	if (deletion->count > 0)
	{
		implementedDeletion(tree, *(deletion)->ival);
	}

	if (checkResultValidation->count > 0)
	{
		std::vector<int> elementInTree = { };
		getValueInTree(elementInTree, tree, tree->root);
		
		int indexOfPickUpRandomValue = 0;
		if (elementInTree.size()  > 1)
			indexOfPickUpRandomValue = rand() % (elementInTree.size() - 1);
		
		int pickUpRandomValue = elementInTree[indexOfPickUpRandomValue];

		uint64_t cc = 0;
		
		std::vector<int> elementInTreeAfterRunningAlgorithm = {};

		implementedDeletion(tree, pickUpRandomValue);
		getValueInTree(elementInTreeAfterRunningAlgorithm, tree, tree->root);

		for (int i = 0; i < elementInTreeAfterRunningAlgorithm.size(); ++i)
		{
			if (i < indexOfPickUpRandomValue)
			{
				if (elementInTreeAfterRunningAlgorithm[i] != elementInTree[i])
				{
					fprintf(stderr, "Values are not the same , Error in deletion code\n");
					destroyRBTree(tree);
					goto exit;
				}
			}
			else
			{
				if (elementInTreeAfterRunningAlgorithm[i] != elementInTree[i + 1])
				{
					fprintf(stderr, "Values are not the same , Error in deletion code\n");
					destroyRBTree(tree);
					goto exit;
				}
			}
		}
			
		insertValue(tree, pickUpRandomValue, cc);

		elementInTreeAfterRunningAlgorithm = {};
		getValueInTree(elementInTreeAfterRunningAlgorithm, tree, tree->root);

		for (int i = 0; i < elementInTreeAfterRunningAlgorithm.size(); ++i)
		{
			if (elementInTreeAfterRunningAlgorithm[i] != elementInTree[i])
			{
				fprintf(stderr, "Values are not the same , Error in insertion code\n");
				destroyRBTree(tree);
				goto exit;
			}
		}

		printf("Test Success for Deletion, Insertion \n");



	}

	if (showRedBlackTree->count > 0)
	{
		printInOrder(tree, tree->root);
	}


	arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
	destroyRBTree(tree);

	return exitcode;

exit:
	/* deallocate each non-null entry in argtable[] */
	arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
	return exitcode;
}


void getValueInTree(std::vector<int>& stdVector, struct red_black_tree* tree, struct tree_node* node)
{
	if (node != tree->NIL)
	{
		getValueInTree(stdVector, tree, node->left);
		stdVector.push_back(node->data);
		getValueInTree(stdVector, tree, node->right);
	}
}


