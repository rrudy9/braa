#include "../include/red_black_tree_insertion_c.h"
#include <red_black_tree_common_c.h>

void insertion_fixup(struct red_black_tree* tree, struct tree_node* node, uint64_t& computeVal)
{
	++computeVal;

	while (node->parent->color == COLOR::Red)
	{	++computeVal;

		if (node->parent == node->parent->parent->left)
		{
			//uncle of node
			struct tree_node* uncle = node->parent->parent->right;

			//case 1
			if (uncle->color == COLOR::Red)
			{
				node->parent->color = COLOR::Black;
				uncle->color = COLOR::Black;
				node->parent->parent->color = COLOR::Red;
				node = node->parent->parent;
			}
			//case 2 or case 3
			else
			{
				if (node == node->parent->right)
				{
					//case 2
					node = node->parent;
					leftRotate(tree, node, computeVal);
				}
				//case 3
				node->parent->color = COLOR::Black; //make parent black
				node->parent->parent->color = COLOR::Red;
				rightRotate(tree, node->parent->parent, computeVal);
			}
		}
		else
		{	
			//uncle of node
			struct tree_node* uncle = node->parent->parent->left;
			if (uncle->color == COLOR::Red)
			{
				node->parent->color = COLOR::Black;
				uncle->color = COLOR::Black;
				node->parent->parent->color = COLOR::Red;
				node = node->parent->parent;
			}
			else
			{
				if (node == node->parent->left)
				{
					node = node->parent;
					rightRotate(tree, node, computeVal);
				}
				node->parent->color = COLOR::Black;
				node->parent->parent->color = COLOR::Red;
				leftRotate(tree, node->parent->parent, computeVal);
			}

		}
	}
	tree->root->color = COLOR::Black;
}




void insert(struct red_black_tree* tree, struct tree_node* newNode, uint64_t& computeVal)
{
	struct tree_node* prevNode = tree->NIL;
	struct tree_node* listChain = tree->root;

	++computeVal;

	while (listChain != tree->NIL)
	{
		++computeVal;

		prevNode = listChain;

		if (newNode->data < listChain->data)
		{
			listChain = listChain->left;
		}
		else
		{
			listChain = listChain->right;
		}

	}

	newNode->parent = prevNode;

	if (prevNode == tree->NIL)
	{
		tree->root = newNode;
	}
	else if (newNode->data < prevNode->data) //data of left child is less than its parent 
	{
		prevNode->left = newNode;
	}
	else
	{
		prevNode->right = newNode;
	}

	newNode->right = tree->NIL;
	newNode->left = tree->NIL;

	insertion_fixup(tree, newNode, computeVal);
}



void insertValue(struct red_black_tree* tree, int value, uint64_t& computeVal)
{
	struct tree_node* newNode = NULL;
	createNode(&newNode,value);
	insert(tree, newNode, computeVal);

}



void implementedInsertion(struct red_black_tree* rBT, int valToInsert)
{
	size_t timeElasped = 0;
	uint64_t computeVal = 0;

	getTimeLapsComputeVariables(timeElasped, insertValue, rBT, valToInsert, computeVal);

	showComputeElapsedTime(computeVal, timeElasped, "Insertion Red-BLack-tree", global_show_compute, global_log_level);

	if (global_log_level == logLevel::debug)
	{
		printf("check your red-black-tree (infix order) after insertion\n");
		printInOrder(rBT, rBT->root);
		printf("\n");
	}
}