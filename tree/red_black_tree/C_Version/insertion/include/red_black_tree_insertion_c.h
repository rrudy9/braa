#pragma once 


#ifdef TEST_FOUND 
#include "CppUTest/MemoryLeakDetectorMallocMacros.h"
#endif

struct red_black_tree;

void insertion_fixup(struct red_black_tree* tree, struct tree_node* node, uint64_t& computeVal);

void insert(struct red_black_tree* tree, struct tree_node* newNode, uint64_t& computeVal);

void insertValue(struct red_black_tree* tree, int value, uint64_t& computeVal);

void implementedInsertion(struct red_black_tree* rBT, int valToInsert);
