#include "../include/b_tree_search_c.h"

/*
void searchNode(struct tree_node* node, int* pos, int item, uint64_t& computeVal)
{
	++computeVal;

	if (!node)
	{
		return;
	}

	if (item < node->item[1])
	{
		*pos = 0;
	}
	else
	{
		for (*pos = node->count; (item < node->item[*pos] && *pos > 1); --(*pos));
		++computeVal;
		if (item == node->item[*pos])
		{
			
			if (global_log_level <= logLevel::debug)
				printf("value %d found\n", item);
			return;
		}
		//else
	///	{
			//if (global_log_level <= logLevel::debug)
			//	printf("value %d not found\n", item);
		//}
	}
	searchNode(node->link[*pos], pos, item,computeVal);

	return;
}


void implementedSearch(struct b_tree* bT, int valToSearch)
{
	if (bT->root != NULL)
	{

		if (global_log_level == logLevel::debug)
		{
			printf("check your b-tree\n\n");
			printInOrder(bT->root);
		}

		size_t timeElasped = 0;
		uint64_t computeVal = 0;
		int pos = 0;

		getTimeLapsComputeVariables(timeElasped, searchNode, bT->root, &pos, valToSearch, computeVal);
		showComputeElapsedTime(computeVal, timeElasped, "Search b-tree", global_show_compute, global_log_level);
	}
	else
	{
		printf("Try To insert a value first, your b-tree is empty!\n");
	}
}


*/