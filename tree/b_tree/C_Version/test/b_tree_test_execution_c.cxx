#include "CppUTest/TestHarness.h"
#include "CppUTest/MemoryLeakDetectorMallocMacros.h"

#include <common_project.h>
#include <b_tree_search_c.h>
#include <b_tree_insertion_c.h>
#include <b_tree_deletion_c.h>
#include <b_tree_common_c.h>

TEST_GROUP(TEST_B_TREE_C)
{
	struct Common_Project_Values bTreeCommonValue = { global_test_count_element, global_test_rand_seed };
	struct b_tree* bTree = NULL;

	void setup()
	{
		showCommonProjectValue(bTreeCommonValue, global_log_level);
		createBTree(&bTree, bTreeCommonValue);
	}

	void teardown()
	{
		destroyBTree(bTree);
	}
};

TEST(TEST_B_TREE_C, deletion)
{
	int valToDelete = rand() % 10011 + 1;

	if (global_log_level <= logLevel::debug)
		printf("val to delete %i\n", valToDelete);

	//implementedDeletion(bTree, valToDelete);

}

TEST(TEST_B_TREE_C, search)
{
	int valToSearch = rand() % 10011 + 1;

	if (global_log_level <= logLevel::debug)
		printf("val to search %i\n", valToSearch);

	//implementedSearch(bTree, valToSearch);

	FAIL("force failing for search on purpose");

}

TEST(TEST_B_TREE_C, insertion)
{
	int valToInsert = rand() % 10011 + 1;

	if (global_log_level <= logLevel::debug)
		printf("val to insert %i\n", valToInsert);

//	implementedInsertion(bTree, valToInsert);

}

