#include "../include/b_tree_common_c.h"
#include <b_tree_insertion_c.h>

void setBTreeParameter(int bTreeParameter)
{
	b_tree_parameter = bTreeParameter; 
}

int getBTreeParameter()
{
	return b_tree_parameter; 
}


void createBTree(struct b_tree** tree, struct Common_Project_Values projectVal)
{
	for (int i = 0; i < projectVal.elementCount; ++i)
	{
		int randVal = rand() % 10001;
		uint64_t cc = 0; 
		insertValueToBTree(tree, randVal, cc); 
	}
}

void destroyBTree(struct b_tree* tree)
{
	int i = 0;
	if (tree)
	{
		for (i = 0; i < tree->numberOfKeysInTheNode; ++i)
		{
			destroyBTree(tree->nodeChildren[i]);
			
			free(tree->keysValue);

			tree->keysValue = NULL;
		}
		destroyBTree(tree->nodeChildren[i]);
		
		free(tree->nodeChildren);
		free(tree);
		tree = NULL; 
	}

}

void printInOrder(struct b_tree* tree)
{
	int i = 0; 
	if (tree)
	{
		for (i = 0; i < tree->numberOfKeysInTheNode; ++i)
		{
			printInOrder(tree->nodeChildren[i]);
			printf("%i,", tree->keysValue[i]);
		}
		printInOrder(tree->nodeChildren[i]);
	}
}


/*d
void initializeBTree(struct b_tree** bTree)
{
	*bTree = (struct b_tree*) malloc(sizeof(struct b_tree));
	
	if (*bTree != NULL)
		(*bTree)->root = NULL;
	else
	{
		fprintf(stderr, "can't allocate your b tree");
		exit(0);
	}
}

void createBTree(struct b_tree** bTree, struct Common_Project_Values projectVal)
{
	initializeBTree(bTree);

	for (int i = 0; i < projectVal.elementCount; ++i)
	{
		uint64_t c = 0;
		int randVal = rand() % 10001; 
		insertNode(*bTree, randVal, c);
	}
}

void createNode(struct b_tree** bTree, struct tree_node* child, int valToAdd)
{
	struct tree_node* newNode = (struct tree_node*)malloc(sizeof(struct tree_node));

	if (!newNode)
	{
		fprintf(stderr, "can't allocate your node");
		exit(0);
	}

	newNode->item[1] = valToAdd;
	newNode->count = 1;
	newNode->link[0] = (*bTree)->root;
	newNode->link[1] = child;

	(*bTree)->root = newNode;
}

void printInOrder(struct tree_node* node)
{
	int i = 0 ;
	if (node)
	{
		for (i = 0; i < node->count; ++i)
		{
			printInOrder(node->link[i]);
			printf("%i,", node->item[i + 1]);
		}
		printInOrder(node->link[i]);
	}
}

void freeBTree(struct tree_node* node)
{
	int i = 0;
	if (node)
	{
		for (i = 0; i < node->count; ++i)
		{
			freeBTree(node->link[i]);
			free(node->link[i]);
		}
		freeBTree(node->link[i]);
		free(node->link[i]);
	}
}

void freeTree(struct b_tree* tree)
{
	if (tree)
	{
		free(tree->root);
		free(tree);
	}
}

void destroyBTree(struct b_tree* tree)
{
	freeBTree(tree->root);
	freeTree(tree);
}


void getTimeLapsComputeVariables(size_t& elapsedTime, void (*f)(struct tree_node*, int, uint64_t&), struct tree_node* treeNode, int val, uint64_t& computeVal)
{

	std::chrono::steady_clock::time_point t1 = std::chrono::high_resolution_clock::now();
	f(treeNode, val, computeVal);
	std::chrono::steady_clock::time_point t2 = std::chrono::high_resolution_clock::now();

	elapsedTime = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();
}

void getTimeLapsComputeVariables(size_t& elapsedTime, void (*f)(struct tree_node*, int*, int, uint64_t&), struct tree_node* treeNode, int* pos, int val, uint64_t& computeVal)
{

	std::chrono::steady_clock::time_point t1 = std::chrono::high_resolution_clock::now();
	f(treeNode,pos,val, computeVal);
	std::chrono::steady_clock::time_point t2 = std::chrono::high_resolution_clock::now();

	elapsedTime = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();
}


void getTimeLapsComputeVariables(size_t& elapsedTime, void (*f)(struct b_tree*, int, uint64_t&), struct b_tree* tree, int val, uint64_t& computeVal)
{

	std::chrono::steady_clock::time_point t1 = std::chrono::high_resolution_clock::now();
	f(tree,val, computeVal);
	std::chrono::steady_clock::time_point t2 = std::chrono::high_resolution_clock::now();

	elapsedTime = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();
}

*/