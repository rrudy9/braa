#ifndef B_TREE_COMMON_C
#define B_TREE_COMMON_C

#include <stdlib.h>
#include <stdio.h>
#include <common_project.h>
#include <chrono>

#ifdef TEST_FOUND 
#include "CppUTest/MemoryLeakDetectorMallocMacros.h"
#endif

static int b_tree_parameter = 4;

void setBTreeParameter(int bTreeParameter);
int getBTreeParameter();

/*
	a non-root node can have (treeParameter - 1) to (2 * treeParamter -1 ) keys
	if a node is the root --> keys belongs to [1, (2* treeParamter - 1) ]
	non leaves have one more children than keys
	all leaves should be at the same level
*/

struct b_tree
{
	//uint64_t count;
	uint64_t numberOfKeysInTheNode;
	int* keysValue; // keys value in a node 
	struct b_tree** nodeChildren; 
	bool isLeaf;
};


void createBTree(struct b_tree** tree, struct Common_Project_Values projectVal); 

void destroyBTree(struct b_tree* tree);

void printInOrder(struct b_tree* tree);


/*
#define MAX 3
#define MIN 2

struct tree_node
{
	int item[MAX + 1];a
	int count;
	struct tree_node* link[MAX + 1];
};

struct b_tree
{
	struct tree_node* root;
};


void initializeBTree(struct b_tree** bTree);

void createBTree(struct b_tree** bTree, struct Common_Project_Values projectVal);

void createNode(struct b_tree** bTree, struct tree_node* node, int valToAdd);


void printInOrder(struct tree_node* node);
void freeBTree(struct tree_node* node);
void freeTree(struct b_tree* tree);

void destroyBTree(struct b_tree* tree); 

void getTimeLapsComputeVariables(size_t& elapsedTime, void (*f)(struct tree_node*, int, uint64_t&), struct tree_node* treeNode, int val, uint64_t& computeVal);
void getTimeLapsComputeVariables(size_t& elapsedTime, void (*f)(struct tree_node*, int*, int, uint64_t&), struct tree_node* treeNode, int*pos, int val, uint64_t& computeVal);
void getTimeLapsComputeVariables(size_t& elapsedTime, void (*f)(struct b_tree*, int, uint64_t&), struct b_tree* tree, int val, uint64_t& computeVal);

*/
#endif
