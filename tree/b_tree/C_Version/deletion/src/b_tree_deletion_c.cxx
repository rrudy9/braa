#include "../include/b_tree_deletion_c.h"

/*
void deleteNode(struct tree_node* node, int item, uint64_t& computeVal)
{
	struct tree_node* tmp = NULL;
	++computeVal;

	if (!delValFromNode(item, node, computeVal))
	{
		printf("value not found \n");
		return;
	}
	else
	{
		if (node->count == 0)
		{
			tmp = node;
			node = node->link[0];
			free(tmp);
		}
	}
	return;
}

int delValFromNode(int item, struct tree_node* node, uint64_t& computeVal)
{
	int pos = 0, flag = 0;
	++computeVal;

	if (node)
	{
		if (item < node->item[1])
		{
			pos = 0;
			flag = 0;
		}
		else
		{
			for (pos = node->count; (item < node->item[pos] && pos > 1); --pos);
			++computeVal;
			if (item == node->item[pos])
			{
				flag = 1;
			}
			else
			{
				flag = 0;
			}
		}
	}

	if (flag)
	{
		if (node->link[pos - 1])
		{
			copySuccessor(node, pos);
			flag = delValFromNode(node->item[pos], node->link[pos], computeVal);
			if (flag == 0)
			{
				if (global_log_level <= logLevel::debug)
					printf("given data is not present in b-tree\n");
			}
		}
		else
		{
			removeVal(node, pos, computeVal);
		}
	}
	else
	{
		flag = delValFromNode(item, node->link[pos], computeVal);
	}

	if (node->link[pos])
	{
		if (node->link[pos]->count < MIN)
		{
			adjustNode(node, pos, computeVal);
		}
	}
	return flag;
}

void adjustNode(struct tree_node* node, int pos, uint64_t& computeVal)
{
	++computeVal;

	if (!pos)
	{
		if (node->link[1]->count > MIN)
		{
			leftShift(node, 1, computeVal);
		}
		else
		{
			mergeNodes(node, 1, computeVal);
		}
	}
	else
	{
		if (node->count != pos)
		{
			if (node->link[pos - 1]->count > MIN)
			{
				rightShift(node, pos,computeVal);
			}
			else
			{
				if (node->link[pos + 1]->count > MIN)
				{
					leftShift(node, pos + 1, computeVal);
				}
				else
				{
					mergeNodes(node, pos, computeVal);
				}
			}
		}
		else
		{
			if (node->link[pos - 1]->count > MIN)
			{
				rightShift(node, pos,computeVal);
			}
			else
			{
				mergeNodes(node, pos, computeVal);
			}
		}
	}
}

void mergeNodes(struct tree_node* node, int pos, uint64_t& computeVal)
{
	int j = 1;
	struct tree_node* tNode = node->link[pos];
	struct tree_node* tNode2 = node->link[pos - 1];
	++tNode2->count;
	tNode2->item[tNode2->count] = node->item[pos];
	tNode2->link[tNode2->count] = node->link[0];

	++computeVal;

	while (j <= tNode->count)
	{
		++computeVal;

		++tNode2->count;
		tNode2->item[tNode2->count] = tNode->item[j];
		tNode2->link[tNode2->count] = tNode->link[j];
		++j;
	}

	j = pos;

	while (j < node->count)
	{
		++computeVal;

		node->item[j] = node->item[j + 1];
		node->link[j] = node->link[j + 1];
		++j;
	}

	--node->count;
	free(tNode);

}


void leftShift(struct tree_node* node, int pos, uint64_t& computeVal)
{
	int j = 1;
	struct tree_node* tNode = node->link[pos - 1];

	tNode->count++;
	tNode->item[tNode->count] = node->item[pos];
	tNode->link[tNode->count] = node->link[pos]->link[0];

	tNode = node->link[pos];
	node->item[pos] = tNode->item[1];
	tNode->link[0] = tNode->link[1];
	--tNode->count;

	++computeVal;

	while (j <= tNode->count)
	{
		++computeVal;

		tNode->item[j] = tNode->item[j + 1];
		tNode->link[j] = tNode->link[j + 1];
		++j;
	}

	return;

}

void rightShift(struct tree_node* node, int pos, uint64_t& computeVal)
{
	struct tree_node* tNode = node->link[pos];
	int j = tNode->count;
	
	++computeVal;

	while (j > 0)
	{
		++computeVal;

		tNode->item[j + 1] = tNode->item[j];
		tNode->link[j + 1] = tNode->link[j];
	}

	tNode->item[1] = node->item[pos];
	tNode->link[1] = tNode->link[0];
	++tNode->count;

	tNode = node->link[pos - 1];

	node->item[pos] = tNode->item[tNode->count];
	node->link[pos] = tNode->link[tNode->count];
	--tNode->count;

	return;

}
void removeVal(struct tree_node* node, int pos, uint64_t& computeVal)
{
	int i = pos + 1;
	++computeVal;

	while (i <= node->count)
	{
		++computeVal;

		node->item[i - 1] = node->item[i];
		node->link[i - 1] = node->link[i];
		++i;

	}
	--node->count;

}

void copySuccessor(struct tree_node* node, int pos)
{
	struct tree_node* tmp;
	tmp = node->link[pos];

	for (; tmp->link[0] != NULL;)
		tmp = tmp->link[0];
	node->item[pos] = tmp->item[1];

}


void implementedDeletion(struct b_tree* bT, int valToDelete)
{
	if (bT->root != NULL)
	{
		size_t timeElasped = 0;
		uint64_t computeVal = 0;

		if (global_log_level == logLevel::debug)
		{
			printf("check your b-tree (infix order)\n");
			printInOrder(bT->root);
		}

		// one memory leak on delete , will check it later

		getTimeLapsComputeVariables(timeElasped, deleteNode, bT->root, valToDelete, computeVal);
		printf("\n");
		showComputeElapsedTime(computeVal, timeElasped, "Deletion B-tree", global_show_compute, global_log_level);

		if (global_log_level == logLevel::debug)
		{
			printf("after delete Infix Order\n");
			printInOrder(bT->root);
			printf("\n");
		}
	}
	else
	{
		printf("Try To insert a value first, your b-tree is empty!\n");
	}
}

*/

