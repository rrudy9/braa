#pragma once 

#include <common_project.h>
#include <b_tree_common_c.h>

#ifdef TEST_FOUND 
#include "CppUTest/MemoryLeakDetectorMallocMacros.h"
#endif

/*

void deleteNode(struct tree_node* node, int item, uint64_t& computeVal);

int delValFromNode(int item, struct tree_node* node, uint64_t& computeVal);

void adjustNode(struct tree_node* node, int pos, uint64_t& computeVal);

void mergeNodes(struct tree_node* node, int pos, uint64_t& computeVal);
void leftShift(struct tree_node* node, int pos, uint64_t& computeVal);

void rightShift(struct tree_node* node, int pos, uint64_t& computeVal);
void removeVal(struct tree_node* node, int pos, uint64_t& computeVal);

void copySuccessor(struct tree_node* node, int pos);

void implementedDeletion(struct b_tree* bT, int valToDelete);
*/
