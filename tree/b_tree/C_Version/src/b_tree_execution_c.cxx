#include <stdlib.h>
#include <stdio.h>
#include <common_project.h>
#include <b_tree_search_c.h>
#include <b_tree_insertion_c.h>
#include <b_tree_deletion_c.h>
#include <b_tree_common_c.h>
#include <argtable3.h>
#include <vector>


#ifdef TEST_FOUND 
#include "CppUTest/MemoryLeakDetectorMallocMacros.h"
#endif

bool global_show_compute = false;
logLevel global_log_level = logLevel::info;

//void getValueInTree(std::vector<int>& stdVector, struct tree_node* node);

int main(int argc, char* argv[])
{

	struct arg_lit
		* help = NULL,
		* showCompute = NULL,
		* checkResultValidation = NULL,
		* showBTree = NULL;

	struct arg_str
		* logLevelValue = NULL;

	struct arg_int
		* setElementCount = NULL,
		* setRandSeedValue = NULL,
		* search = NULL,
		* insertion = NULL,
		* bTreeParmeter = NULL,
		* deletion = NULL;

	struct arg_end* end = NULL;

	struct Common_Project_Values bTreeCommonValue = { 100000 , 30 };
	struct b_tree* bTree = NULL;


	void* argtable[] = {
		help = arg_litn(NULL, "help", 0, 1, "display this help for B Tree <C Version> and exit"),
		showCompute = arg_litn("c","show-compute" ,0, 1, "Show Compute"),
		logLevelValue = arg_strn("v", "verbose","<info,debug,error,fatal>", 0, 1, "Set Log Level (info, debug, error, fatal) (Default : info)"),
		setElementCount = arg_intn("b", "b-tree-count","<n>" , 0, 1, "Set B Tree Number of element (Default Value: 100k),  Doesn't work when n = 0"),
		setRandSeedValue = arg_intn("r", "rand-seed","<n>", 0, 1, "Set value for Seed Random (Default Value: 30)"),
		checkResultValidation = arg_litn("t","run-test" ,0, 1, "Run Test for implemented algorithms"),

		bTreeParmeter = arg_intn("x", "parameter", "<value>" ,0, 1, "Choose your parameter for your B-Tree (Default Value: 4"),
		search = arg_intn("s", "search","<value>", 0, 1, "Access B Tree C Search Algorithm implementation"),
		insertion = arg_intn("i", "insertion", "<value>" ,0, 1, "Access B Tree C Insertion Algorithm implementation"),
		deletion = arg_intn("d", "deletion","<value>", 0, 1, "Access B Tree C Deletion Algorithm implementation"),
		showBTree = arg_litn("p", "show-rb-tree", 0, 1, "display your b-tree "),

		end = arg_end(20)
	};

	int exitcode = 0;
	char progname[] = "bTreeC.exe";

	int nerrors;
	nerrors = arg_parse(argc, argv, argtable);

	if (argc == 1) {
		printf("Please choose one of the available commands\n\n");
		arg_print_glossary(stdout, argtable, "  %-25s %s\n");
		exitcode = 0;
		goto exit;
	}

	//If the parser returned any errors then display them and exit 
	if (nerrors > 0)
	{
		// Display the error details contained in the arg_end struct.
		arg_print_errors(stderr, end, progname);
		fprintf(stderr,"Try '%s --help' for more information.\n", progname);
		exitcode = 1;
		goto exit;
	}


	if (help->count > 0)
	{
		printf("Usage: %s", progname);
		arg_print_syntax(stdout, argtable, "\n\n");
		arg_print_glossary(stdout, argtable, "  %-25s %s\n");
		exitcode = 0;
		goto exit;
	}

	if (showCompute->count > 0)
	{
		global_show_compute = !global_show_compute;
	}

	if (logLevelValue->count > 0)
	{
		setLogLevel(*(logLevelValue)->sval, global_log_level);
	}

	//set project common value (random seed , and number of element in array
	if (setElementCount->count > 0)
	{
//		if (*(setElementCount)->ival == 0) goto exit;

		bTreeCommonValue.elementCount = *(setElementCount)->ival;
	}

	if (setRandSeedValue->count > 0)
	{
		bTreeCommonValue.seedRand = *(setRandSeedValue)->ival;
	}

	if (bTreeParmeter->count > 0)
	{
		setBTreeParameter(*(bTreeParmeter)->ival);
	}


	srand(bTreeCommonValue.seedRand);
	showCommonProjectValue(bTreeCommonValue, global_log_level);
	printf("B-Tree Parameter : %i\n\n", getBTreeParameter());

	createBTree(&bTree, bTreeCommonValue);

	if (search->count > 0)
	{
///		implementedSearch(bTree, *(search)->ival);
	}

	if (insertion->count > 0)
	{
		uint64_t cc = 0;
		insertValueToBTree(&bTree, *(insertion)->ival,cc);
		insertValueToBTree(&bTree, 10, cc);
		insertValueToBTree(&bTree, 100, cc);
		insertValueToBTree(&bTree, 20, cc);
		insertValueToBTree(&bTree, 30, cc);
		insertValueToBTree(&bTree, 40, cc);
		insertValueToBTree(&bTree, 60, cc);
		insertValueToBTree(&bTree, 50, cc);

		insertValueToBTree(&bTree, 70, cc);

		
		/*
			insertValueToBTree(&bTree, 35, cc);
			insertValueToBTree(&bTree, 45, cc);
			insertValueToBTree(&bTree, 85, cc);

			insertValueToBTree(&bTree, 15, cc);
			insertValueToBTree(&bTree, 5, cc);
			insertValueToBTree(&bTree, 3, cc);
			insertValueToBTree(&bTree, 2, cc);

			insertValueToBTree(&bTree, 170, cc);

			insertValueToBTree(&bTree, 180, cc);
			*/
	
	}

	if (deletion->count > 0)
	{
	//	implementedDeletion(bTree, *(deletion)->ival);

	}


	if (checkResultValidation->count > 0)
	{
		/*
		std::vector<int> elementInTree = { };
		getValueInTree(elementInTree, bTree->root);

		int indexOfPickUpRandomValue = 0;
		if (elementInTree.size() > 1)
			indexOfPickUpRandomValue = rand() % (elementInTree.size() - 1);

		int pickUpRandomValue = elementInTree[indexOfPickUpRandomValue];

		std::vector<int> elementInTreeAfterRunningAlgorithm = {};

		printf("%i random value \n", pickUpRandomValue); 

		printInOrder(bTree->root);
		uint64_t cc = 0;
		deleteNode(bTree->root, pickUpRandomValue,cc);
		printf("\nafter delete \n");
		printInOrder(bTree->root);
		printf("\n");


		getValueInTree(elementInTreeAfterRunningAlgorithm, bTree->root);


		for (int i = 0; i < elementInTreeAfterRunningAlgorithm.size(); ++i)
		{
			if (i < indexOfPickUpRandomValue)
			{
				if (elementInTreeAfterRunningAlgorithm[i] != elementInTree[i])
				{
					fprintf(stderr, "Values are not the same , Error in deletion code\n");
					destroyBTree(bTree);
					goto exit;
				}
			}
			else
			{
				if (elementInTreeAfterRunningAlgorithm[i] != elementInTree[i + 1])
				{
					fprintf(stderr, "Values are not the same , Error in deletion code\n");
					destroyBTree(bTree);
					goto exit;
				}
			}
		}
		cc = 0; 
		insertNode(bTree, pickUpRandomValue, cc);

		elementInTreeAfterRunningAlgorithm = {};
		getValueInTree(elementInTreeAfterRunningAlgorithm, bTree->root);

		for (int i = 0; i < elementInTreeAfterRunningAlgorithm.size(); ++i)
		{
			if (elementInTreeAfterRunningAlgorithm[i] != elementInTree[i])
			{
				fprintf(stderr, "Values are not the same , Error in insertion code\n");
				destroyBTree(bTree);
				goto exit;
			}
		}

		printf("Test Success for Deletion, Insertion \n");
		*/

	}


	if (showBTree->count > 0)
	{
		printInOrder(bTree);
	}
	
	arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));

	destroyBTree(bTree);

	return exitcode;


exit:
	/* deallocate each non-null entry in argtable[] */
	arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
	return exitcode;


}

/*
void getValueInTree(std::vector<int>& stdVector, struct tree_node* node)
{
	int i = 0; 
	if (node)
	{
		for (i = 0; i < node->count; ++i)
		{
			getValueInTree(stdVector,node->link[i]);
			stdVector.push_back(node->item[i + 1]);
		}
		getValueInTree(stdVector,node->link[i]);
	}
}

*/
