#pragma once 

#include <common_project.h>

#ifdef TEST_FOUND 
#include "CppUTest/MemoryLeakDetectorMallocMacros.h"
#endif

struct b_tree;
/*
struct tree_node;


void splitNode(int valToInsert, int* pVal, int pos, struct tree_node* node, struct tree_node* child, struct tree_node** newNode, uint64_t& computeVal);

void addValToNode(int valToInsert, int pos, struct tree_node* node, struct tree_node* child, uint64_t& computeVal);

int setValueInNode(int valToInsert, int* pVal, struct tree_node* node, struct tree_node** child, uint64_t& computeVal);

void insertNode(struct b_tree* tree, int valToInsert, uint64_t& computeVal);

void implementedInsertion(struct b_tree* bT, int valToInsert);

*/

void insertValueToBTree(struct b_tree** tree, int valToInsert, uint64_t computeVal);

void insertionSortForTheKeyArray(int* array,  int numberOfKeys);

void insertValueToNodeIfNotFull(struct b_tree** tree, int valToInsert); 