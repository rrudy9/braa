#include "../include/b_tree_insertion_c.h"
#include <b_tree_common_c.h>

/*
void splitNode(int valToInsert, int* pVal, int pos, struct tree_node* node, struct tree_node* child, struct tree_node** newNode, uint64_t& computeVal)
{
	int median = 0 , j = 0 ;

	++computeVal;

	if (pos > MIN)
	{
		median = MIN + 1;
	}
	else
	{
		median = MIN;
	}

	*newNode = (struct tree_node*)malloc(sizeof(struct tree_node));
	j = median + 1;

	while (j <= MAX)
	{
		++computeVal;

		(*newNode)->item[j - median] = node->item[j];
		(*newNode)->link[j - median] = node->link[j];
		++j;
	}

	node->count = median;
	(*newNode)->count = MAX - median;

	if (pos <= MIN)
	{
		addValToNode(valToInsert, pos, node, child,computeVal);
	}
	else
	{
		addValToNode(valToInsert, pos - median, *newNode, child, computeVal);
	}

	*pVal = node->item[node->count];

	(*newNode)->link[0] = node->link[node->count];
	node->count--;

}

void addValToNode(int valToInsert, int pos, struct tree_node* node, struct tree_node* child, uint64_t& computeVal)
{
	int j = node->count;
	while (j > pos)
	{
		++computeVal;

		node->item[j + 1] = node->item[j];
		node->link[j + 1] = node->link[j];
		--j;
	}

	node->item[j + 1] = valToInsert;
	node->link[j + 1] = child;
	node->count++;
}


int setValueInNode(int valToInsert, int* pVal, struct tree_node* node, struct tree_node** child, uint64_t& computeVal)
{
	int pos = 0;
	++computeVal;

	if (!node)
	{
		*pVal = valToInsert;
		*child = NULL;
		return 1;
	}

	if (valToInsert < node->item[1])
	{
		pos = 0;
	}
	else
	{
		for (pos = node->count; (valToInsert < node->item[pos] && pos > 1); --pos);
		++computeVal;
		if (valToInsert == node->item[pos])
		{
			if (global_log_level == logLevel::debug)
				printf("Duplicate found\n");
			return 0;
		}
	}

	if (setValueInNode(valToInsert, pVal, node->link[pos], child, computeVal))
	{
		if (node->count < MAX)
		{
			addValToNode(*pVal, pos, node, *child, computeVal);
		}
		else
		{
			splitNode(*pVal, pVal, pos, node, *child, child, computeVal);
			return 1;
		}
	}
	return 0;

}

void insertNode(struct b_tree* tree, int valToInsert, uint64_t& computeVal)
{
	int flag = 0, i = 0;
	struct tree_node* child = NULL;
	flag = setValueInNode(valToInsert, &i, tree->root, &child, computeVal);
	if (flag)
		createNode(&tree, child, i);
}


void implementedInsertion(struct b_tree* bT, int valToInsert)
{
	size_t timeElasped = 0;
	uint64_t computeVal = 0;

	getTimeLapsComputeVariables(timeElasped, insertNode, bT, valToInsert, computeVal);
	showComputeElapsedTime(computeVal, timeElasped, "Insertion B-tree", global_show_compute, global_log_level);

	if (global_log_level == logLevel::debug)
	{
		printf("after Insertion Infix Order\n");
		printInOrder(bT->root);
		printf("\n");
	}
}*/


void insertValueToBTree(struct b_tree** tree, int valToInsert, uint64_t computeVal)
{
	if (!*tree)
	{
		*tree = (struct b_tree*)malloc(sizeof(struct b_tree));
		
		if (!(*tree)) { fprintf(stderr, "Can't alloocate your tree\n"); exit(0); }

		(*tree)->numberOfKeysInTheNode = 1;
		
		(*tree)->keysValue = (int*)malloc((*tree)->numberOfKeysInTheNode * sizeof(int));
		
		if (!(*tree)->keysValue) { fprintf(stderr, "Can't alloocate your key array in tree\n");  exit(0); }

		(*tree)->keysValue[(*tree)->numberOfKeysInTheNode - 1] = valToInsert;
		(*tree)->isLeaf = true;
		
		(*tree)->nodeChildren = (struct b_tree**)malloc( ( (*tree)->numberOfKeysInTheNode + 1) * sizeof(struct b_tree*)); // key + 1 children
		
		if (!(*tree)->nodeChildren) { fprintf(stderr, "Can't alloocate your kids in tree\n"); exit(0); }

		for (int i = 0; i <= (*tree)->numberOfKeysInTheNode; ++i)
			(*tree)->nodeChildren[i] = NULL;
	
		return;
	}
	
	//first case --> root
	// first node --> root can contain 1 to 2 * paramater - 1 
	if ((*tree)->isLeaf)
	{
		// && node children is different than null 
		if ((*tree)->numberOfKeysInTheNode < (2 * getBTreeParameter() - 1))
		{

			++(*tree)->numberOfKeysInTheNode;
			
			(*tree)->keysValue = (int*)realloc((*tree)->keysValue, (sizeof(int) * (*tree)->numberOfKeysInTheNode));
			
			if (!(*tree)->keysValue) { fprintf(stderr, "Can't reallocate your key array for root when its leaf\n"); exit(0); }
			
			(*tree)->keysValue[(*tree)->numberOfKeysInTheNode - 1] = valToInsert;
			insertionSortForTheKeyArray((*tree)->keysValue, (*tree)->numberOfKeysInTheNode); 
	
			(*tree)->nodeChildren = (struct b_tree**)realloc((*tree)->nodeChildren, ((*tree)->numberOfKeysInTheNode + 1) * sizeof(struct b_tree*));
			
			if (!(*tree)->nodeChildren) { fprintf(stderr, "Can't reallocate your kids for root when its is leaf\n"); exit(0); }

			// we already created one more above so at the ends we will have key +_ 1 
			(*tree)->nodeChildren[(*tree)->numberOfKeysInTheNode] = NULL;

			return;
		}
		else
		{

			// since a non root node can have only (tree parameter - 1 ) to (2 * tree parameter - 1) 
			// we're going to split the node

			/// take value at median 
			int medianValue = (*tree)->keysValue[(*tree)->numberOfKeysInTheNode / 2];
			(*tree)->isLeaf = false;

			/*
				since we are going to split and leave only one value in the root,
				we will automatically have (tree parameter - 1) key in each kid and reduce the kids
				from number of keys + 1 to 2 filled kids

				first round , root will always contain 1 key, will see later how we will filled it again
				(since it can have 1 to (2 * tree parameter - 1)
			*/


			int middleIndex = (*tree)->numberOfKeysInTheNode / 2;

			// we check if the value that we want to insert is bigger than the value in the middle of the key array

			(*tree)->nodeChildren = (struct b_tree**)realloc((*tree)->nodeChildren, 2 * sizeof(struct b_tree*));

			if (!(*tree)->nodeChildren) { fprintf(stderr, "Can't reallocate your kids for root when its is full\n"); exit(0); }


			// 2 because we already know that the first split attempt , will always have 2 kids and one key in the root
			for (int i = 0; i < 2; ++i)
			{
				//left & right (on 1)
				(*tree)->nodeChildren[i] = (struct b_tree*)malloc(sizeof(struct b_tree));
				
				if (!(*tree)->nodeChildren[i]) { fprintf(stderr, "Can't allocate your kids to split for root when its is full\n"); exit(0); }
				
				(*tree)->nodeChildren[i]->numberOfKeysInTheNode = (*tree)->numberOfKeysInTheNode / 2;
				(*tree)->nodeChildren[i]->isLeaf = true;

				(*tree)->nodeChildren[i]->nodeChildren = (struct b_tree**)malloc(((*tree)->nodeChildren[i]->numberOfKeysInTheNode + 1) * sizeof(struct b_tree*));
				
				if (!(*tree)->nodeChildren[i]->nodeChildren) { fprintf(stderr, "Can't allocate your kids for the kids root after splitting\n"); exit(0); }
				
				for (int j = 0; j <= (*tree)->nodeChildren[i]->numberOfKeysInTheNode; ++j)
					(*tree)->nodeChildren[i]->nodeChildren[j] = NULL;

				(*tree)->nodeChildren[i]->keysValue = (int*)malloc((*tree)->nodeChildren[i]->numberOfKeysInTheNode * sizeof(int));
				
				if (!(*tree)->nodeChildren[i]->keysValue) { fprintf(stderr, "Can't allocate your key array of the kids after splitting the root\n"); exit(0); }

				if(i == 0) 
					memmove((*tree)->nodeChildren[i]->keysValue, (*tree)->keysValue, (*tree)->nodeChildren[i]->numberOfKeysInTheNode * sizeof(int));
				else 
					memmove((*tree)->nodeChildren[i]->keysValue, (*tree)->keysValue + middleIndex + 1, (*tree)->nodeChildren[i]->numberOfKeysInTheNode * sizeof(int));

			}

			//set middle value to root 
			
			(*tree)->numberOfKeysInTheNode = 1;
			int rootValue = (*tree)->keysValue[middleIndex];
			
			(*tree)->keysValue = (int*)realloc((*tree)->keysValue, (*tree)->numberOfKeysInTheNode * sizeof(int));
		
		if (!(*tree)->keysValue) { fprintf(stderr, "Can't reallocate your root after splitting it\n"); exit(0); }
			
			(*tree)->keysValue[0] = rootValue;

			// insert value to the right node or to the left node
			if ((*tree)->keysValue[0] > valToInsert)
				insertValueToNodeIfNotFull(&(*tree)->nodeChildren[0], valToInsert);
			else
				insertValueToNodeIfNotFull(&(*tree)->nodeChildren[1], valToInsert);
			
			
			return;
		}
	}
	else
	{
		// if root is non leaf anymore --> we will see how we're going to manage it

		struct b_tree* treeNode = *tree;
		
		// since we already splited the values along the available key in the root , 
		// we will proceed in this way
		
		for (int rootIndexKeys = 0; rootIndexKeys < (*tree)->numberOfKeysInTheNode; ++rootIndexKeys)
		{
			
			if (rootIndexKeys == (*tree)->numberOfKeysInTheNode - rootIndexKeys && valToInsert > (*tree)->keysValue[rootIndexKeys])
			{
				// we will take the kid + 1 
				treeNode = (*tree)->nodeChildren[rootIndexKeys + 1];
			

				/*
				
				
				
				*/

			}
			else if (valToInsert < (*tree)->keysValue[rootIndexKeys])
			{
				// since we know that the root have only two key at first 
				treeNode = (*tree)->nodeChildren[rootIndexKeys];


				//we check if there is any leaf

				//
				// we are going to handle the 2 cases
				if (treeNode->isLeaf)
				{
					// if node is not full , we just insert the key  
					if (treeNode->numberOfKeysInTheNode < 2 * getBTreeParameter() - 1)
					{
						insertValueToNodeIfNotFull(&(*tree)->nodeChildren[rootIndexKeys], valToInsert);
						return; 
					}
					else
					{ 

						/*
							if full , we will proceed the following way
							we know that the root is not full and can have (2 * parameter - 1)

							so we will split the actual node by pushing the median value to the root
							while creating 3 kids to the root instead of 2 or more,
							we will have the kids that are less than the median value, a kid with values bigger than the median
						
						*/
						
						if ((*tree)->numberOfKeysInTheNode < 2 * getBTreeParameter() - 1)
						{
							int medianValueOfNode = treeNode->keysValue[treeNode->numberOfKeysInTheNode / 2];
							int midIndexAfterSplit = treeNode->numberOfKeysInTheNode / 2;
							++(*tree)->numberOfKeysInTheNode; 

							(*tree)->keysValue = (int*)realloc((*tree)->keysValue, (*tree)->numberOfKeysInTheNode * sizeof(int));
							if (!(*tree)->keysValue) { fprintf(stderr, "Can't reallocate your parent node when kid is full\n"); exit(0); }							
						
							memmove((*tree)->keysValue +( rootIndexKeys + 1 ) , (*tree)->keysValue + rootIndexKeys, (*tree)->numberOfKeysInTheNode * sizeof(int));						
							(*tree)->keysValue[rootIndexKeys] = medianValueOfNode;
					
							(*tree)->nodeChildren = (struct b_tree**)realloc((*tree)->nodeChildren, ( (*tree)->numberOfKeysInTheNode + 1) * sizeof(struct b_tree*));
							if (!(*tree)->nodeChildren) { fprintf(stderr, "Can't reallocate your new parents kids when kids is full and need to split\n"); exit(0); }
						
							(*tree)->nodeChildren[(*tree)->numberOfKeysInTheNode] = (struct b_tree*)malloc(sizeof(struct b_tree));
							if (!(*tree)->nodeChildren[(*tree)->numberOfKeysInTheNode]) { fprintf(stderr, "Can't reallocate your new parents last kid to handle shift memory operation \n"); exit(0); }
							
							memmove((*tree)->nodeChildren + (rootIndexKeys + 1), (*tree)->nodeChildren + rootIndexKeys, ((*tree)->numberOfKeysInTheNode + 2) * sizeof(int));
							(*tree)->nodeChildren[rootIndexKeys + 1] = (struct b_tree*)malloc(sizeof(struct b_tree));						
							
							if (!(*tree)->nodeChildren[rootIndexKeys + 1]) { fprintf(stderr, "Can't allocate your new kid parent the one that was it was shiftting\n"); exit(0); }
						
							(*tree)->nodeChildren[rootIndexKeys + 1]->keysValue = (int*) malloc (midIndexAfterSplit * sizeof(int));
							
							if (!(*tree)->nodeChildren[rootIndexKeys + 1]->keysValue) { fprintf(stderr, "Can't allocate your new array key for kid parent the one that was it was shiftting\n"); exit(0); }
						
							(*tree)->nodeChildren[rootIndexKeys + 1]->isLeaf = true;
							(*tree)->nodeChildren[rootIndexKeys + 1]->numberOfKeysInTheNode = midIndexAfterSplit;
							
							(*tree)->nodeChildren[rootIndexKeys + 1]->nodeChildren = (struct b_tree**)malloc(((*tree)->nodeChildren[rootIndexKeys + 1]->numberOfKeysInTheNode + 1) * sizeof(struct b_tree*));
							
							if (!(*tree)->nodeChildren[rootIndexKeys + 1]->nodeChildren) { fprintf(stderr, "Can't allocate your new kids for the node one that was it was shiftting\n"); exit(0); }
						
							for (int j = 0; j <= (*tree)->nodeChildren[rootIndexKeys + 1]->numberOfKeysInTheNode; ++j)
								(*tree)->nodeChildren[rootIndexKeys + 1]->nodeChildren[j] = NULL;

							memmove((*tree)->nodeChildren[rootIndexKeys + 1]->keysValue, treeNode->keysValue + midIndexAfterSplit + 1, midIndexAfterSplit * sizeof(int));

							treeNode->numberOfKeysInTheNode = midIndexAfterSplit;
							treeNode->keysValue = (int*)realloc(treeNode->keysValue, (treeNode->numberOfKeysInTheNode * sizeof(int)));							
							
							if (!treeNode->keysValue) { fprintf(stderr, "can't reallocate your parent after shift\n"); exit(0); }

							memmove(treeNode->keysValue, treeNode->keysValue, midIndexAfterSplit * sizeof(int));
				
							treeNode->nodeChildren = (struct b_tree**)malloc((treeNode->numberOfKeysInTheNode + 1) * sizeof(struct b_tree*));
							if (!treeNode->nodeChildren) { fprintf(stderr, "can't allocate the new kids for the parent after the shift\n"); exit(0); }
							
							for (int j = 0; j <= treeNode->numberOfKeysInTheNode; ++j)
								treeNode->nodeChildren[j] = NULL;

							// we insert to the most left or right
							if ((*tree)->keysValue[rootIndexKeys] > valToInsert)
								insertValueToNodeIfNotFull(&treeNode, valToInsert);
							else
								insertValueToNodeIfNotFull(&(*tree)->nodeChildren[rootIndexKeys + 1], valToInsert);


							return; 
						
						}
						else
						{
							// we split and create new leaf, but before lets check if the neighbor is full 
							// if not full, , push value from root to neighbor, push last value of actual node to root
							// and insert to the actual leaf where is one space left for it
							//
							// remember leaves should be as same level


							//lets check if the neighbor is not full and if it's not a leaf before creating a new node from the actual kid

							// treeNode the kids where we want to insert our key 
							// still in root in 2 order height
							for (int indexToFindNotFullNode = 0; indexToFindNotFullNode <= (*tree)->numberOfKeysInTheNode; ++indexToFindNotFullNode)
							{
								if ((*tree)->nodeChildren[indexToFindNotFullNode]->numberOfKeysInTheNode < 2 * getBTreeParameter() + 1)
	 							{

									// if the neighbor is after the leaf node that will use to insert the new val
									if (indexToFindNotFullNode > rootIndexKeys)
									{
										int getBackToIndexWhereWeWantToInsert = indexToFindNotFullNode;
										while (getBackToIndexWhereWeWantToInsert != rootIndexKeys)
										{
											struct b_tree* kidsChain = (*tree)->nodeChildren[getBackToIndexWhereWeWantToInsert];
											++kidsChain->numberOfKeysInTheNode;
											insertValueToNodeIfNotFull(&kidsChain, (*tree)->keysValue[getBackToIndexWhereWeWantToInsert - 1]);
											(*tree)->keysValue[getBackToIndexWhereWeWantToInsert - 1] = (*tree)->nodeChildren[getBackToIndexWhereWeWantToInsert - 1]
																											->keysValue[(*tree)->nodeChildren[getBackToIndexWhereWeWantToInsert - 1]
																												->numberOfKeysInTheNode];
											
											--(*tree)->nodeChildren[getBackToIndexWhereWeWantToInsert - 1]->numberOfKeysInTheNode;
											
											(*tree)->nodeChildren[getBackToIndexWhereWeWantToInsert - 1]->keysValue =
												(int*)realloc((*tree)->nodeChildren[getBackToIndexWhereWeWantToInsert - 1]->keysValue,
													(*tree)->nodeChildren[getBackToIndexWhereWeWantToInsert - 1]
														->numberOfKeysInTheNode * sizeof(int));
												
											--getBackToIndexWhereWeWantToInsert;
										}
										
										//   if (rootIndexKeys != 0 )
										(*tree)->keysValue[getBackToIndexWhereWeWantToInsert] = (*tree)->nodeChildren[getBackToIndexWhereWeWantToInsert]
																									->keysValue[(*tree)->nodeChildren[getBackToIndexWhereWeWantToInsert]
																										->numberOfKeysInTheNode];

										// insert in the actual node
										(*tree)->nodeChildren[getBackToIndexWhereWeWantToInsert]
											->keysValue[(*tree)->nodeChildren[getBackToIndexWhereWeWantToInsert]
												->numberOfKeysInTheNode] = valToInsert;

										insertionSortForTheKeyArray((*tree)->nodeChildren[getBackToIndexWhereWeWantToInsert]->keysValue, (*tree)->nodeChildren[getBackToIndexWhereWeWantToInsert]->numberOfKeysInTheNode);
										return;

									}
									else
									{
										// if the free neighbor is before the actual leaf to insert
										int getBackToIndexWhereWeWantToInsert = indexToFindNotFullNode;
										while (getBackToIndexWhereWeWantToInsert != rootIndexKeys)
										{
											struct b_tree* kidsChain = (*tree)->nodeChildren[getBackToIndexWhereWeWantToInsert];
											++kidsChain->numberOfKeysInTheNode;
											insertValueToNodeIfNotFull(&kidsChain, (*tree)->keysValue[getBackToIndexWhereWeWantToInsert]);
											(*tree)->keysValue[getBackToIndexWhereWeWantToInsert] = (*tree)->nodeChildren[getBackToIndexWhereWeWantToInsert + 1]->keysValue[0];
										
											memmove((*tree)->nodeChildren[getBackToIndexWhereWeWantToInsert + 1]->keysValue - 1, (*tree)->nodeChildren[getBackToIndexWhereWeWantToInsert + 1]->keysValue, (*tree)->nodeChildren[getBackToIndexWhereWeWantToInsert + 1]->numberOfKeysInTheNode * sizeof(int));

											--(*tree)->nodeChildren[getBackToIndexWhereWeWantToInsert + 1]->numberOfKeysInTheNode;

											(*tree)->nodeChildren[getBackToIndexWhereWeWantToInsert + 1]->keysValue = (int*)realloc((*tree)->nodeChildren[getBackToIndexWhereWeWantToInsert + 1]->keysValue, (*tree)->nodeChildren[getBackToIndexWhereWeWantToInsert + 1]->numberOfKeysInTheNode * sizeof(int));

											++getBackToIndexWhereWeWantToInsert;
										}

										insertValueToNodeIfNotFull(&(*tree)->nodeChildren[getBackToIndexWhereWeWantToInsert], valToInsert );
										return; 

									}
								}
							}

							// we continue, by splitting the actual node, since all leaves should be at the same level, 
							// we should split also all the other nodes,  to end up with a tree that contains a new height ordre ( + 1 from the previous tree)
						

						}
					}
				}
				else
				{
					// if tree is a non leaf , proceed to children while comparing


				}

			}
		}

	}

}

void insertValueToNodeIfNotFull(struct b_tree** tree, int valToInsert)
{
	++(*tree)->numberOfKeysInTheNode;
	
	(*tree)->keysValue = (int*)realloc((*tree)->keysValue, (*tree)->numberOfKeysInTheNode * sizeof(int));
	if (!(*tree)->keysValue) { fprintf(stderr, "can't reallocate your node key array\n"); exit(0); }
	
	(*tree)->nodeChildren = (struct b_tree**)realloc((*tree)->nodeChildren, ((*tree)->numberOfKeysInTheNode + 1) * sizeof(struct b_tree*));
	if (!(*tree)->nodeChildren) { fprintf(stderr, "can't reallocate your node kids\n"); exit(0); }

	(*tree)->nodeChildren[(*tree)->numberOfKeysInTheNode] = NULL;
	(*tree)->keysValue[(*tree)->numberOfKeysInTheNode - 1] = valToInsert;
	insertionSortForTheKeyArray((*tree)->keysValue, (*tree)->numberOfKeysInTheNode);

}

void insertionSortForTheKeyArray(int* array, int numberOfKeys)
{

	for (int index = 1; index < numberOfKeys; ++index)
	{
		int key = array[index];
		int j = index - 1;


		while ((j >= 0) && (array[j] > key))
		{

			array[j + 1] = array[j];
			j = j - 1;
		}
		array[j + 1] = key;
	}
}