#include "../include/common_project.h"


char* getInput()
{

	char* input = 0;
	size_t cur_len = 0;
	size_t cur_max = 0;
	char bufferInput[10];
	char* extra = NULL;
	fflush(stdout);
	while (fgets(bufferInput, sizeof(bufferInput), stdin) != NULL)
	{
		size_t buf_len = strlen(bufferInput);
		if (cur_len + buf_len + 1 > cur_max)
		{

			size_t new_len = cur_max * 2 + 1;
			if (buf_len + 1 > new_len)
				new_len = buf_len + 1;
			extra = (char*)realloc(input, new_len);

			if (extra == 0)
				break;
			input = extra;
			cur_max = new_len;
		}
		if (input)
			strcpy(input + cur_len, bufferInput);

		cur_len += buf_len;

		if (input && input[cur_len - 1] == '\n')
			break;
	}

	markEndOfInput(input);
	//fflush(stdin); 
	return input;
}

void markEndOfInput(char* str)
{
	char* p = strchr(str, '\n');

	if (p)
	{
		*p = 0;
	}
	else
	{
		int c = 0;
		while ((c = getchar()) != '\n' && c != EOF)
		{

		}
	}

}


void getTestName(char* str)
{
	int i = 0, j = 0;
	size_t len = strlen(str);
	while (i < len)
	{
		if (str[i] == '-')
		{
			j = i;
			str[i + 1] = toupper(str[i + 1]);
			while (j < len)
			{
				str[j] = str[j + 1];
				++j;
			}
			--len;
			--i;
		}
		++i;
	}
}


void getIntegerInputVal(char* hint, int& getNumber)
{
	printf("%s", hint);
	char* c = getInput();

	int ret = 0;
	int number = 0;
	char* endptr = NULL;

	ret = strtol(c, &endptr, 10);

	if (*endptr == '\0')
	{
		sscanf_s(c, "%d", &getNumber);
	}
	else
	{
		getIntegerInputVal("please type a valid number, try again : ", getNumber);
	}

	free(c);

}

void launchExecutable(const char* currentTestName, std::string& binaryDir, const char* folderName, bool testExecutable)
{
	char* executableName = (char*)malloc(strlen(currentTestName) + 1 * sizeof(char));

	if (!executableName)
		exit(0);

	strcpy(executableName, currentTestName);

	getTestName(executableName);


	if (!testExecutable)
	{
		std::string exeFile = binaryDir + folderName + "/Debug/" + executableName + ".exe";
		system(exeFile.c_str());
	}
	else
	{
		std::string exeFile = binaryDir + folderName + "/Debug/" + executableName + ".exe -c ";
		system(exeFile.c_str());
	}

	free(executableName);
}


void setCommonDataStructureValTests(struct Common_Project_Values* projectVal)
{

	int randVal = rand() % RAND_MAX + 1;

	srand(randVal);

	projectVal->seedRand = randVal;

	randVal = rand() % RAND_MAX + 100;
	projectVal->elementCount = randVal;

}


void showCommonProjectValue(struct Common_Project_Values projectVal, enum logLevel globalLogLevel)
{
	printf("Your Random Seed Value: %i\n", projectVal.seedRand);
	printf("******************************************\n");
	printf("Number of Element in your Data Structure: %i\n", projectVal.elementCount);
	printf("******************************************\n");

	printf("Your log level : %s\n\n", globalLogLevel == logLevel::debug ? "Debug" : globalLogLevel == logLevel::error ? "Error" : globalLogLevel == logLevel::fatal ? "Fatal" : "Info");
}


void setLogLevel(std::string userLogChoice, enum logLevel& globalLogLevel)
{
	if (userLogChoice == "debug")
		globalLogLevel = logLevel::debug;

	else if (userLogChoice == "error")
		globalLogLevel = logLevel::error;

	else if (userLogChoice == "fatal")
		globalLogLevel = logLevel::fatal;

}


void showComputeElapsedTime(uint64_t computeVal, size_t& timeElapsed, char* algorithmName, bool globalCompute, enum logLevel globalLogLevel)
{
	if (globalCompute)
		printf("Compute For  %s: %zu\n", algorithmName, computeVal);

	if (globalLogLevel <= logLevel::debug)
	{
		printf("Elapsed time (us) for  %s: %zu\n\n", algorithmName, timeElapsed);
	}
}


void showCommonProjectValueForTest(enum logLevel globalLogLevel, int countElement, int randSeedValue)
{
	printf("Your Random Seed Value: %i\n", randSeedValue);
	printf("******************************************\n");
	printf("Number of Element in your Data Structure: %i\n", countElement);
	printf("******************************************\n");

	printf("Your log level : %s\n\n", globalLogLevel == logLevel::debug ? "Debug" : globalLogLevel == logLevel::error ? "Error" : globalLogLevel == logLevel::fatal ? "Fatal" : "Info");
}