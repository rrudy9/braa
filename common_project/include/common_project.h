#ifndef COMMON_PROJECT_H
#define COMMON_PROJECT_H

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

#ifdef TEST_FOUND 
#include "CppUTest/MemoryLeakDetectorMallocMacros.h"
#endif

enum class logLevel {info, debug, error, fatal };

extern bool global_show_compute;
extern logLevel global_log_level;

// show validation result if u dont have cpputest
extern bool global_result_validation;

//set global variable to use in each test , if the user wants to set it with arguments
extern int global_test_count_element; 
extern int global_test_rand_seed; 


struct Common_Project_Values
{
	int	elementCount;
	int seedRand;
};

char* getInput();

void markEndOfInput(char* str);

void getTestName(char* str);

void getIntegerInputVal(char* hint, int& getNumber);

void launchExecutable(const char* currentTestName, std::string& binaryDir, const char* folderName, bool testExecutable);

void setCommonDataStructureValTests(struct Common_Project_Values* projectVal);

void showCommonProjectValue(struct Common_Project_Values projectVal, enum logLevel globalLogLevel);

void setLogLevel(std::string userLogChoice, enum logLevel& globalLogLevel);

void showComputeElapsedTime(uint64_t computeVal, size_t& timeElapsed, char* algorithmName, bool globalCompute, enum logLevel globalLogLevel);

void showCommonProjectValueForTest(enum logLevel globalLogLevel, int countElement, int randSeedValue);



#endif


