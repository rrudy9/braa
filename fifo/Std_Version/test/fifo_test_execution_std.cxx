#include "CppUTest/TestHarness.h"
#include "CppUTest/MemoryLeakDetectorMallocMacros.h"

#include <common_project.h>
#include <queue>

TEST_GROUP(QUEUE_STD)
{
	struct Common_Project_Values fifoCommonValue = { global_test_count_element, global_test_rand_seed };
	std::queue<int> q = {};

	void setup()
	{
		showCommonProjectValue(fifoCommonValue, global_log_level);
		for (int i = 0; i < fifoCommonValue.elementCount; ++i)
		{
			int valToPushBack = rand() % RAND_MAX + 1;
			q.push(valToPushBack);
		}
	}

	void teardown()
	{
	}
};

TEST(QUEUE_STD, getELementCount)
{
	if (global_log_level <= logLevel::debug)
		printf("Your Total number of element in Queue is : %zu\n", q.size());

	CHECK(q.size() >= 0);

}


TEST(QUEUE_STD, getLastValue)
{

	if (global_log_level <= logLevel::debug)
		printf("Your Last Value in Queue is : %i\n", q.back());

	//CHECK(returnLastValue(q) >= 0);

}

TEST(QUEUE_STD, getFirstValue)
{
	if (global_log_level <= logLevel::debug)
		printf("Your First Value in Queue is : %i\n", q.front());

	//CHECK(returnFirstValue(q) >= 0);
}


TEST(QUEUE_STD, isEmpty)
{
	if (global_log_level <= logLevel::debug)
		printf("Is your Queue Empty : %s\n", q.empty() == 0 ? "no" : "yes");


}


TEST(QUEUE_STD, poppinFront)
{
	size_t timeElapsed = 0;
	int computeVal = 0;

	int firstValueBeforePoppinFront = q.front();

	q.pop();
	

	int newFirstValue = q.front();

	CHECK(firstValueBeforePoppinFront != newFirstValue);

}

TEST(QUEUE_STD, pushinBack)
{
	size_t timeElapsed = 0;
	int computeVal = 0;

	int lastValueBeforePushingBack = q.back();

	int valToAdd = rand() % 1001 + 1;
	q.push(valToAdd);

	int newLastVal = q.back();

	CHECK(lastValueBeforePushingBack != newLastVal);
}


