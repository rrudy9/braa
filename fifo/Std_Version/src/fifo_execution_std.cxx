#include <stdlib.h>
#include <stdio.h>
#include <common_project.h>
#include <iostream>
#include <queue>
#include "argtable3.h"


#ifdef TEST_FOUND 
#include "CppUTest/MemoryLeakDetectorMallocMacros.h"
#endif

bool global_show_compute = false;
logLevel global_log_level = logLevel::info;



int main(int argc, char* argv[])
{

	struct arg_lit
		* help = NULL,
		* showCompute = NULL,
		* poppingFront = NULL,
		* getElementCount = NULL,
		* getLastValue = NULL,
		* getFirstValue = NULL,
		* isEmpty = NULL,
		* showQueue = NULL;

	struct arg_str
		* logLevelValue = NULL;

	struct arg_int
		* setElementCount = NULL,
		* setRandSeedValue = NULL,
		* pushingBack = NULL;

	struct arg_end* end = NULL;

	struct Common_Project_Values fifoCommonValue = { 100000 , 30 };
	std::queue<int> q = {};

	void* argtable[] = {
		help = arg_litn(NULL, "help", 0, 1, "display this help for QUEUE FIFO <Std Version> and exit"),
		showCompute = arg_litn("c","show-compute",0, 1, "Show Compute"),
		logLevelValue = arg_strn("v", "verbose","<info,debug,error,fatal>", 0, 1, "Set Log Level (info, debug, error, fatal) (Default : info)"),
		setElementCount = arg_intn("q", "queue-count","<n>" , 0, 1, "Set Queue Number of element (Default Value: 100k) "),
		setRandSeedValue = arg_intn("r", "rand-seed","<n>", 0, 1, "Set value for Seed Random (Default Value: 30)"),

		getElementCount = arg_litn("n", "get-element-count", 0, 1, "Get Number of element in your QUEUE"),
		getLastValue = arg_litn("l", "get-last-value", 0, 1, "Get Last Value of your Queue"),
		getFirstValue = arg_litn("f", "get-first-value", 0, 1, "Get First Value of your Queue"),
		isEmpty = arg_litn("e", "is-empty", 0, 1, "Check if your Queue is empty"),
		poppingFront = arg_litn("p", "pop-front", 0, 1, "remove first value from your queue"),
		pushingBack = arg_intn("b", "push-back","<value>", 0, 1, "Add a value at the end of your Queue "),
		showQueue = arg_litn("z", "show-queue", 0, 1, "Show Queue"),

		end = arg_end(20)
	};

	int exitcode = 0;
	char progname[] = "fifoStd.exe";

	int nerrors;
	nerrors = arg_parse(argc, argv, argtable);

	if (argc == 1) {
		printf("Please choose one of the available commands\n\n");
		arg_print_glossary(stdout, argtable, "  %-25s %s\n");
		exitcode = 0;
		goto exit;
	}

	//If the parser returned any errors then display them and exit v
	if (nerrors > 0)
	{
		// Display the error details contained in the arg_end struct.
		arg_print_errors(stderr, end, progname);
		fprintf(stderr,"Try '%s --help' for more information.\n", progname);
		exitcode = 1;
		goto exit;
	}


	if (help->count > 0)
	{
		printf("Usage: %s", progname);
		arg_print_syntax(stdout, argtable, "\n\n");
		arg_print_glossary(stdout, argtable, "  %-25s %s\n");
		exitcode = 0;
		goto exit;
	}

	if (showCompute->count > 0)
	{
		global_show_compute = !global_show_compute;
	}

	if (logLevelValue->count > 0)
	{
		setLogLevel(*(logLevelValue)->sval, global_log_level);
	}

	//set project common value (random seed , and number of element in array
	if (setElementCount->count > 0)
	{
		fifoCommonValue.elementCount = *(setElementCount)->ival;
	}

	if (setRandSeedValue->count > 0)
	{
		fifoCommonValue.seedRand = *(setRandSeedValue)->ival;
	}

	srand(fifoCommonValue.seedRand);
	showCommonProjectValue(fifoCommonValue, global_log_level);

	for (int i = 0; i < fifoCommonValue.elementCount; ++i)
	{
		int randValue = rand() % 10001;
		q.push(randValue);
	}


	if (poppingFront->count > 0)
	{

		if (!q.empty())
		{
			q.pop();
		}
		else
			printf("List is Empty, you can't delete values , try something else\n");
	}

	if (pushingBack->count > 0)
	{
		q.push(*(pushingBack)->ival);
	}

	if (getElementCount->count > 0)
	{
		printf("Your Total number of element in Queue is : %zu\n", q.size());
	}

	if (getLastValue->count > 0)
	{

		if (!q.empty())
		{
			printf("Last Value in Queue %i\n", q.back());
		}
		else
		{
			printf("Queue is Empty\n");
		}
	}

	if (getFirstValue->count > 0)
	{

		if (!q.empty())
		{
			printf("Front Value in Queue %i\n", q.front());
		}
		else
		{
			printf("Queue is Empty\n");
		}
	}

	if (isEmpty->count > 0)
	{
		printf("Is your Queue Empty : %s\n", q.empty() ? "yes" : "no");
	}

	if (showQueue->count > 0)
	{ 
		//showLinkedList(q->front, "Your Queue");
		std::queue<int> copyQ = q; 
		while (!copyQ.empty())
		{
			printf("%i,", copyQ.front());
			copyQ.pop();
		}
	}

	arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
	return exitcode;

exit:
/* deallocate each non-null entry in argtable[] */
	arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
	return exitcode;
}

