#ifndef FIFO_BASE_ARRAY_C
#define FIFO_BASE_ARRAY_C

#include <stdio.h>
#include <stdlib.h>
#include <common_project.h>

struct queue_base_array
{
	int count; 
	int* queue;
	int front; 
	int back;
	int capacity; 
};

void createQueueArray(struct queue_base_array &arrayQueue, struct Common_Project_Values projectVal);

void destroyQueueArray(struct queue_base_array& arrayQueue);

void pushBack(struct queue_base_array &arrayQueue, int value);

void deleteChosenValue(struct queue_base_array& arrayQueue, int value);

int isQueueEmpty(struct queue_base_array arrayQueue);

int searchValue(struct queue_base_array arrayQueue, int value, int& indexValueInArray);

void popFront(struct queue_base_array &arrayQueue);

void pushFront(struct queue_base_array& arrayQueue, int value);

void displayQueue(struct queue_base_array arrayQueue);

void getFirstValue(struct queue_base_array arrayQueue, int& saveFrontValueFoLaterUse);

void getLastValue(struct queue_base_array arrayQueue, int& saveFrontValueFoLaterUse);

void moveValueToFront(struct queue_base_array& arrayQueue, int indexToMove); 

void insertValueInIndex(struct queue_base_array& arrayQueue, int valueToAdd, int index);

int getTotalElementNumberInQueue(struct queue_base_array arrayQueue);

#endif 