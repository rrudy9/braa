#ifndef FIFO_COMMON_C
#define FIFO_COMMON_C

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <ctime>
#include <algorithm>
#include <math.h>
#include <chrono>

#include <common_project.h>
#include <linked_list_common_c.h>

#ifdef TEST_FOUND 
#include "CppUTest/MemoryLeakDetectorMallocMacros.h"
#endif

static int CAPACITY;

struct queue
{
	int count;
	struct linked_list* front;
	struct linked_list* back;
};

void initialize(struct queue** q, struct Common_Project_Values projectVal);

void createFifoQueue(struct queue** q, struct Common_Project_Values projectVal);

void destroyQueue(struct queue* q);

int isQueueEmpty(struct queue* q);

void pushBack(struct queue* q, int value, uint64_t& computeVal);

void pushFront(struct queue* q, int value);

void popFront(struct queue* q);

void popBack(struct queue* q);

int returnFirstValue(struct queue* q);

int returnLastValue(struct queue* q);

int returnElementNumberInQueue(struct queue* q);

void getTimeLapsComputeVariables(size_t& elapsedTime, void (*f)(struct queue*, int, uint64_t&), struct queue* queue, int value, uint64_t& computeVal);
void getTimeLapsComputeVariables(size_t& elapsedTime, void (*f)(struct queue*), struct queue* queue);



#endif
