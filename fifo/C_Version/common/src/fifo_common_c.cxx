#include "../include/fifo_common_c.h"

void initialize(struct queue** q, struct Common_Project_Values projectVal)
{
	*q = (struct queue*)malloc(sizeof(struct queue));

	CAPACITY = projectVal.elementCount * 2; 
	
	if (*q == NULL)
	{
		fprintf(stderr, "Can't allocate your queue\n");
		exit(0);
	}

	(*q)->count = 0;
	(*q)->front = NULL;
	(*q)->back = NULL;
}

void createFifoQueue(struct queue** q, struct Common_Project_Values projectVal)
{
	initialize(q, projectVal);
	uint64_t computeVal = 0;

	for (int i = 0; i < projectVal.elementCount; ++i)
	{
		int randValue = rand() % 10001;
		pushBack(*q, randValue, computeVal);
	}

}

void destroyQueue(struct queue* q)
{
	destroyList(q->front);
	free(q);
}

int isQueueEmpty(struct queue* q)
{
	return (q->back == NULL);
}

int returnFirstValue(struct queue* q)
{
	if (isQueueEmpty(q))
	{
		printf("Queue is Empty\n");
		return 0;
	}
	

	return q->front->data;
}


int returnLastValue(struct queue* q)
{
	if (isQueueEmpty(q))
	{
		printf("Queue is Empty\n");
		return 0;
	}

	return q->back->data;
}

int returnElementNumberInQueue(struct queue* q)
{
	return q->count;
}

void pushFront(struct queue* q, int value)
{

	if (isQueueEmpty(q))
	{
		printf("Your Queue is Empty\n");
		return;
	}

	if (q->count >= CAPACITY)
	{
		q->front->data = value; 
	}

	 struct linked_list* tmp = (struct linked_list*)malloc(sizeof(struct linked_list));

	if (!tmp)
	{
		fprintf(stderr, "Can't allocate your node for push back\n");
		exit(0);
	}

	tmp->data = value; 
	tmp->next = q->front;
	q->front = tmp; 
	++q->count;

}

void popBack(struct queue* q)
{

	if (isQueueEmpty(q))
	{
		printf("Your Queue is Empty\n");
		return;
	}
	struct linked_list* listChain = q->front;
	struct linked_list* tmp = NULL; 
	while (listChain != q->back)
	{
		tmp = listChain;
		listChain = listChain->next;
	}
	
	free(listChain); 

	tmp->next = NULL; 
	q->back = tmp; 

	--q->count; 

}

void pushBack(struct queue* q, int value, uint64_t& computeVal)
{
	if (q->count >= CAPACITY)
	{
		popFront(q);
		--q->count;
	}

	struct linked_list* tmp;
	tmp = (struct linked_list*)malloc(sizeof(struct linked_list));
	
	if (!tmp)
	{
		fprintf(stderr, "Can't allocate your node for push back\n");
		exit(0);
	}

	tmp->data = value;
	tmp->next = NULL;

	++computeVal;

	if (!isQueueEmpty(q))
	{
		q->back->next = tmp;
		q->back = tmp;
	}
	else
	{
		q->front = q->back = tmp;
	}

	++q->count;

}

void popFront(struct queue* q)
{
	if (isQueueEmpty(q))
	{
		printf("Your Queue is Empty\n");
		return;
	}


	struct linked_list* tmp = NULL;
	int n = q->front->data;
	tmp = q->front;
	q->front = q->front->next;
	--q->count;
	free(tmp);

	if (global_log_level <= logLevel::debug)
		printf("\nfirst value : %i was pop up\n", n);

}



void getTimeLapsComputeVariables(size_t& elapsedTime, void (*f)(struct queue*, int, uint64_t&), struct queue* queue, int value, uint64_t& computeVal)
{
	std::chrono::steady_clock::time_point t1 = std::chrono::high_resolution_clock::now();
	f(queue, value, computeVal);
	std::chrono::steady_clock::time_point t2 = std::chrono::high_resolution_clock::now();

	elapsedTime = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();
}


void getTimeLapsComputeVariables(size_t& elapsedTime, void (*f)(struct queue*), struct queue* queue)
{
	std::chrono::steady_clock::time_point t1 = std::chrono::high_resolution_clock::now();
	f(queue);
	std::chrono::steady_clock::time_point t2 = std::chrono::high_resolution_clock::now();

	elapsedTime = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();

}

