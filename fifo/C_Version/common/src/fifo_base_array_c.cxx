#include "../include/fifo_base_array_c.h"


void createQueueArray(struct queue_base_array &arrayQueue, struct Common_Project_Values projectVal)
{
	if (projectVal.elementCount)
		arrayQueue.queue = (int*)malloc(arrayQueue.capacity * sizeof(int));

	for (int i = 0; i < projectVal.elementCount; ++i)
	{
		int randVal = rand() % 10001; 
		pushBack(arrayQueue, randVal);
	}
}


void destroyQueueArray(struct queue_base_array& arrayQueue)
{
	if (arrayQueue.queue != NULL)
	{
		free(arrayQueue.queue);
		arrayQueue.queue = NULL; 
	}
	arrayQueue.count = 0; 
	arrayQueue.front = arrayQueue.back = -1; 
}

void popFront(struct queue_base_array& arrayQueue)
{
	if (isQueueEmpty(arrayQueue))
	{
		printf("fill your queue\n");
		return;
	}

	if (arrayQueue.back == arrayQueue.front)
	{
		free(arrayQueue.queue);
		arrayQueue.queue = NULL;
		arrayQueue.count = 0;
		arrayQueue.front = arrayQueue.back = -1;
		return;
	}
	
	memmove(arrayQueue.queue, arrayQueue.queue + 1, arrayQueue.count * sizeof(int));
	--arrayQueue.count;
	arrayQueue.queue = (int*)realloc(arrayQueue.queue, (arrayQueue.count  * sizeof(int*)));
	--arrayQueue.back;
}



void pushBack(struct queue_base_array& arrayQueue, int value)
{

	int indexOfValInQueue = -1;
	int findValueInQueue = searchValue(arrayQueue, value,indexOfValInQueue);

	if (indexOfValInQueue != -1)
	{
		//move value to front
		moveValueToFront(arrayQueue, indexOfValInQueue);
		return; 
	}

	if (arrayQueue.count >=  arrayQueue.capacity)
	{
		popFront(arrayQueue);
		arrayQueue.queue = (int*)realloc(arrayQueue.queue, ( ( arrayQueue.count + 1) * sizeof(int*)));

	}

	if (isQueueEmpty(arrayQueue))
	{
		arrayQueue.front = arrayQueue.back = 0; 
		arrayQueue.queue[arrayQueue.back] = value;
		++arrayQueue.count;
		return;
	}

	++arrayQueue.back;
	arrayQueue.queue[arrayQueue.back] = value;
	++arrayQueue.count;

} 

void moveValueToFront(struct queue_base_array& arrayQueue, int indexToMove)
{
	int valueToMoveToFront = arrayQueue.queue[indexToMove];
	memmove(arrayQueue.queue + indexToMove, arrayQueue.queue + indexToMove + 1, arrayQueue.count * sizeof(int));
	memmove(arrayQueue.queue + 1 , arrayQueue.queue , arrayQueue.count * sizeof(int));

	arrayQueue.queue[0] = valueToMoveToFront ;

}

void deleteChosenValue(struct queue_base_array& arrayQueue, int value)
{

	if (isQueueEmpty(arrayQueue))
	{
		printf("queue is empty\n");
		return; 
	}


	int indexValueInArray = 0;
	for (int i = arrayQueue.front; i <= arrayQueue.back; ++i)
	{
		if (arrayQueue.queue[i] == value)
		{
			indexValueInArray = i; 
		}
	}
	
	memmove(arrayQueue.queue + indexValueInArray , arrayQueue.queue + indexValueInArray + 1 , arrayQueue.count * sizeof(int));
	
	--arrayQueue.count;
	arrayQueue.queue = (int*)realloc(arrayQueue.queue, (arrayQueue.count * sizeof(int*)));

	--arrayQueue.back;

}

int searchValue(struct queue_base_array arrayQueue, int value , int & indexValueInArray)
{
	for (int i = arrayQueue.front; i <= arrayQueue.back; ++i)
	{
		if (arrayQueue.queue[i] == value) indexValueInArray = i ;
	}

	if (indexValueInArray >= 0)
		return arrayQueue.queue[indexValueInArray];
	else
		return indexValueInArray;
}

void pushFront(struct queue_base_array& arrayQueue, int value)
{

	int indexOfValInQueue = -1;
	int findValueInQueue = searchValue(arrayQueue, value, indexOfValInQueue);
	if (indexOfValInQueue != -1)
	{
		//move value to front
		moveValueToFront(arrayQueue, indexOfValInQueue);
		return;
	}

	if (arrayQueue.count >= arrayQueue.capacity)
	{
		popFront(arrayQueue);
	}

	if (isQueueEmpty(arrayQueue))
	{
		arrayQueue.front = arrayQueue.back = 0;
		arrayQueue.queue[arrayQueue.back] = value;
		++arrayQueue.count;
		return;
	}

	++arrayQueue.count;
	arrayQueue.queue = (int*)realloc(arrayQueue.queue, (arrayQueue.count * sizeof(int*)));


	memmove(arrayQueue.queue + 1, arrayQueue.queue, arrayQueue.count * sizeof(int));
	arrayQueue.queue[0] = value; 
	++arrayQueue.back;


}

void insertValueInIndex(struct queue_base_array& arrayQueue, int valueToAdd, int index)
{

	int indexOfValInQueue = -1;
	int findValueInQueue = searchValue(arrayQueue, valueToAdd, indexOfValInQueue);
	// si on croise la meme valeur, on change juste son index
	if (indexOfValInQueue != -1)
	{
		if (index <= arrayQueue.count)
		{
			int valThatWillBeReplaced = arrayQueue.queue[index];
			memmove(arrayQueue.queue + indexOfValInQueue, arrayQueue.queue + indexOfValInQueue + 1, arrayQueue.count * sizeof(int));

			memmove(arrayQueue.queue + index, arrayQueue.queue + (index - 1), arrayQueue.count * sizeof(int));

			arrayQueue.queue[index - 1] = valueToAdd;
			
			return;
		}
		else
		{
			printf("Choose a valid index or put it at the end\n");
			return; 
		}
	}

	if (index <= arrayQueue.count + 1)
	{
		++arrayQueue.count; 
		arrayQueue.queue = (int*)realloc(arrayQueue.queue, (arrayQueue.count * sizeof(int*)));

		memmove(arrayQueue.queue + index, arrayQueue.queue + (index - 1), arrayQueue.count * sizeof(int));
		arrayQueue.queue[index - 1] = valueToAdd;
		++arrayQueue.back;

	}
	else
	{
		printf("Choose a valid index (count element + 1)\n");
	}
}


void getFirstValue(struct queue_base_array arrayQueue, int& saveFrontValueFoLaterUse)
{
	if (!isQueueEmpty(arrayQueue))
		printf("First Value in Queue: %i\n", arrayQueue.queue[arrayQueue.front]);
}


void getLastValue(struct queue_base_array arrayQueue, int& saveFrontValueFoLaterUse)
{
	if (!isQueueEmpty(arrayQueue))
		printf("Last Value in Queue: %i\n", arrayQueue.queue[arrayQueue.back]);
}


int getTotalElementNumberInQueue(struct queue_base_array arrayQueue)
{
	return arrayQueue.count; 
}

int isQueueEmpty(struct queue_base_array arrayQueue)
{
	return (arrayQueue.count == 0);
}

void displayQueue(struct queue_base_array arrayQueue)
{
	if (!isQueueEmpty(arrayQueue))
	{
		for (int i = arrayQueue.front; i <= arrayQueue.back; ++i)
		{
			printf("%i,", arrayQueue.queue[i]);
		}
	}
}