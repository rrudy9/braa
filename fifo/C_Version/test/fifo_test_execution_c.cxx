#include "CppUTest/TestHarness.h"
#include "CppUTest/MemoryLeakDetectorMallocMacros.h"

#include <common_project.h>
#include <fifo_common_c.h>
#include <linked_list_common_c.h>



TEST_GROUP(QUEUE_C)
{
	struct Common_Project_Values fifoCommonValue = { global_test_count_element, global_test_rand_seed };
	struct queue* q = NULL;

	void setup()
	{
		showCommonProjectValue(fifoCommonValue, global_log_level);
		createFifoQueue(&q, fifoCommonValue);
	}

	void teardown()
	{
		destroyQueue(q);
	}
};

TEST(QUEUE_C, getELementCount)
{
	if (global_log_level <= logLevel::debug)
		printf("Your Total number of element in Queue is : %i\n", returnElementNumberInQueue(q));

	CHECK(returnElementNumberInQueue(q) >= 0);

}


TEST(QUEUE_C, getLastValue)
{

	if (global_log_level <= logLevel::debug)
		printf("Your Last Value in Queue is : %i\n", returnLastValue(q));

	CHECK(returnLastValue(q) >= 0);

}

TEST(QUEUE_C, getFirstValue)
{
	if (global_log_level <= logLevel::debug)
		printf("Your First Value in Queue is : %i\n", returnFirstValue(q));

	CHECK(returnFirstValue(q) >= 0);
}


TEST(QUEUE_C, isEmpty)
{
	if (global_log_level <= logLevel::debug)
		printf("Is your Queue Empty : %s\n", isQueueEmpty(q) == 0 ? "no" : "yes");

	CHECK(isQueueEmpty(q) >= 0);

}


TEST(QUEUE_C, poppingFront)
{
	size_t timeElapsed = 0;
	uint64_t computeVal = 0;

	int firstValueBeforePoppinFront = returnFirstValue(q);

	if (global_log_level == logLevel::debug && !isQueueEmpty(q))
		showLinkedList(q->front, "\nQueue Values");

	getTimeLapsComputeVariables(timeElapsed, popFront, q);
	showComputeElapsedTime(computeVal, timeElapsed, "Popping front in Queue", global_show_compute, global_log_level);

	if (global_log_level == logLevel::debug && !isQueueEmpty(q))
		showLinkedList(q->front, "\nQueue Values after popping front");

	int newFirstValue = returnFirstValue(q);
	
	CHECK(firstValueBeforePoppinFront != newFirstValue);

}

TEST(QUEUE_C, pushingBack)
{
	size_t timeElapsed = 0;
	uint64_t computeVal = 0;

	int lastValueBeforePushingBack = returnLastValue(q);

	int valToAdd = rand() % 1001 +1 ;

	if (global_log_level == logLevel::debug && !isQueueEmpty(q))
		showLinkedList(q->front, "\nQueue Values");

	getTimeLapsComputeVariables(timeElapsed, pushBack, q, valToAdd, computeVal);
	showComputeElapsedTime(computeVal, timeElapsed, "Pushing Back in Queue", global_show_compute, global_log_level);

	if (global_log_level == logLevel::debug && !isQueueEmpty(q))
		showLinkedList(q->front, "\nQueue Values after pushing back");

	int newLastVal = returnLastValue(q);

	CHECK(lastValueBeforePushingBack != newLastVal);
}


