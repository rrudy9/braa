#include "CppUTest/TestHarness.h"
#include "CppUTest/MemoryLeakDetectorMallocMacros.h"

#include <common_project.h>
#include <fifo_base_array_c.h>


TEST_GROUP(QUEUE_BASE_ARRAY)
{
	struct Common_Project_Values fifoCommonValue = { global_test_count_element, global_test_rand_seed };
	struct queue_base_array arrayQueue = { 0, NULL, -1, -1, fifoCommonValue.elementCount };

	void setup()
	{
		showCommonProjectValue(fifoCommonValue, global_log_level);
		createQueueArray(arrayQueue, fifoCommonValue);

	}

	void teardown()
	{
		destroyQueueArray(arrayQueue);
	}
};

TEST(QUEUE_BASE_ARRAY, getELementCount)
{
	printf("Total Number in Queue: %i\n", getTotalElementNumberInQueue(arrayQueue));

	//CHECK(returnElementNumberInQueue(q) >= 0);

}


TEST(QUEUE_BASE_ARRAY, getLastValue)
{
	int lastValue = 0;
	getLastValue(arrayQueue, lastValue);
//	CHECK(returnLastValue(q) >= 0);

}

TEST(QUEUE_BASE_ARRAY, getFirstValue)
{
	int firstValue = 0;
	getFirstValue(arrayQueue, firstValue);

//	CHECK(returnFirstValue(q) >= 0);
}


TEST(QUEUE_BASE_ARRAY, isEmpty)
{
	printf("Is Your List is Empty? %s\n", isQueueEmpty(arrayQueue) ? "yes" : "no");

}


TEST(QUEUE_BASE_ARRAY, poppingFront)
{
	popFront(arrayQueue);
	displayQueue(arrayQueue);
}


TEST(QUEUE_BASE_ARRAY, pushingFront)
{
	int randVal = rand() % 1001; 
	pushFront(arrayQueue, randVal);
	displayQueue(arrayQueue);
}


TEST(QUEUE_BASE_ARRAY, pushingBack)
{
	
}


