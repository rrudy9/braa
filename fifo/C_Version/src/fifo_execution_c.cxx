#include <stdlib.h>
#include <stdio.h>
#include <common_project.h>
#include <fifo_common_c.h>
#include "argtable3.h"


#ifdef TEST_FOUND 
#include "CppUTest/MemoryLeakDetectorMallocMacros.h"
#endif

bool global_show_compute = false;
logLevel global_log_level = logLevel::info;

int main(int argc, char* argv[])
{
	struct arg_lit
		* help = NULL,
		* poppingFront = NULL,
		* getElementCount = NULL,
		* getLastValue = NULL,
		* getFirstValue = NULL,
		* showCompute = NULL,
		* checkResultValidation = NULL,
		* showQueue = NULL,
		* isEmpty = NULL;

	struct arg_str
		* logLevelValue = NULL;

	struct arg_int
		* setElementCount = NULL,
		* setRandSeedValue = NULL,
		* pushingBack = NULL;

	struct arg_end* end = NULL;

	struct Common_Project_Values fifoCommonValue = { 100000 , 30 };
	struct queue* q = NULL; 

	void* argtable[] = {
		help = arg_litn(NULL, "help", 0, 1, "display this help for QUEUE FIFO <C Version> and exit"),
		showCompute = arg_litn("c","show-compute",0, 1, "Show Compute"),
		checkResultValidation = arg_litn("t","run-test" ,0, 1, "Run Test for implemented algorithms"),
		logLevelValue = arg_strn("v", "verbose","<info,debug,error,fatal>", 0, 1, "Set Log Level (info, debug, error, fatal) (Default : info)"),
		setElementCount = arg_intn("q", "queue-count","<n>" , 0, 1, "Set Queue Number of element (Default Value: 100k) "),
		setRandSeedValue = arg_intn("r", "rand-seed","<n>", 0, 1, "Set value for Seed Random (Default Value: 30)"),

		getElementCount = arg_litn("n", "get-element-count", 0, 1, "Get Number of element in your QUEUE"),
		getLastValue = arg_litn("l", "get-last-value", 0, 1, "Get Last Value of your Queue"),
		getFirstValue = arg_litn("f", "get-first-value", 0, 1, "Get First Value of your Queue"),
		isEmpty = arg_litn("e", "is-empty", 0, 1, "Check if your Queue is empty"),
		poppingFront = arg_litn("p", "pop-front", 0, 1, "remove first value from your queue"),
		pushingBack = arg_intn("b", "push-back","<value>", 0, 1, "Add a value at the end of your Queue "),
		showQueue = arg_litn("z", "show-queue", 0, 1, "Show Queue"),

		end = arg_end(20)
	};
	
	int exitcode = 0;
	char progname[] = "fifoC.exe";

	int nerrors;
	nerrors = arg_parse(argc, argv, argtable);

	if (argc == 1) {
		printf("Please choose one of the available commands\n\n");
		arg_print_glossary(stdout, argtable, "  %-25s %s\n");
		exitcode = 0;
		goto exit;
	}

	//If the parser returned any errors then display them and exit 
	if (nerrors > 0)
	{
		// Display the error details contained in the arg_end struct.
		arg_print_errors(stderr, end, progname);
		fprintf(stderr,"Try '%s --help' for more information.\n", progname);
		exitcode = 1;
		goto exit;
	}


	if (help->count > 0)
	{
		printf("Usage: %s", progname);
		arg_print_syntax(stdout, argtable, "\n\n");
		arg_print_glossary(stdout, argtable, "  %-25s %s\n");
		exitcode = 0;
		goto exit;
	}


	if (showCompute->count > 0)
	{
		global_show_compute = !global_show_compute;
	}
	
	if (logLevelValue->count > 0)
	{
		setLogLevel(*(logLevelValue)->sval, global_log_level);
	}

	//set project common value (random seed , and number of element in array
	if (setElementCount->count > 0)
	{
		fifoCommonValue.elementCount = *(setElementCount)->ival;
	}

	if (setRandSeedValue->count > 0)
	{
		fifoCommonValue.seedRand = *(setRandSeedValue)->ival;
	}


	srand(fifoCommonValue.seedRand);
	showCommonProjectValue(fifoCommonValue, global_log_level);
	createFifoQueue(&q, fifoCommonValue);

	if (poppingFront->count > 0)
	{

		size_t timeElapsed = 0;
		int computeVal = 0;

		getTimeLapsComputeVariables(timeElapsed, popFront, q);

		showComputeElapsedTime(computeVal, timeElapsed, "Popping front in Queue", global_show_compute, global_log_level);

	}

	if (pushingBack->count > 0)
	{
		
		size_t timeElapsed = 0;
		uint64_t computeVal = 0;

		getTimeLapsComputeVariables(timeElapsed, pushBack, q, *(pushingBack)->ival, computeVal);
		showComputeElapsedTime(computeVal, timeElapsed, "Pushing Back in Queue", global_show_compute, global_log_level);
	}

	if (getElementCount->count > 0)
	{
		printf("Your Total number of element in Queue is : %i\n", returnElementNumberInQueue(q));

	}

	if (getLastValue->count > 0)
	{
		printf("Your Last Value in Queue is : %i\n", returnLastValue(q));
	}

	if (getFirstValue->count > 0)
	{
		printf("Your First Value in Queue is : %i\n", returnFirstValue(q));
	}

	if (isEmpty->count > 0)
	{
		printf("Is your Queue Empty : %s\n", isQueueEmpty(q) ? "yes" : "no");

	}
	
	if (checkResultValidation->count > 0)
	{
		if (isQueueEmpty(q))
		{
			printf("List is empty\n");
			goto exit;
		}

		struct linked_list* listChain = q->front; 
		int countQueue = q->count;
		int* arrayForTesting = (int*)malloc(q->count * sizeof(int));
		int i = 0; 
		while (listChain)
		{
			arrayForTesting[i] = listChain->data;
			++i;
			listChain = listChain->next; 
		}

		popFront(q);
		i = 1; 
		listChain = q->front; 
		while (listChain)
		{
			if (arrayForTesting[i] != listChain->data)
			{
				fprintf(stderr,"Value is not the same, Incorrect Pop Front \n");
				free(arrayForTesting);
				goto exit; 
			}
			listChain = listChain->next; 
			++i; 
		}

		printf("Pop Front Successully done\n");

		pushFront(q, arrayForTesting[0]);

		i = 0;
		listChain = q->front;
		while (listChain)
		{
			if (arrayForTesting[i] != listChain->data)
			{
				fprintf(stderr,"Value is not the same, Incorrect Push Front\n");
				free(arrayForTesting);
				goto exit;
			}
			listChain = listChain->next;
			++i;
		}

		printf("Push Front Successully done\n");

		int randVal = rand() % 1001;
		uint64_t cc = 0; 
		pushBack(q, randVal,cc);
 
		i = 0;
		listChain = q->front;
		while (listChain)
		{
			if (arrayForTesting[i] != listChain->data)
			{
				fprintf(stderr,"Value is not the same, Incorrect Push back\n");
				free(arrayForTesting);
				goto exit;
			}
			listChain = listChain->next;
			++i;
			if (i == q->count - 1) break;
		}

		if (q->back->data != randVal)
		{
			fprintf(stderr, "Value In back was inserted correctly\n");
			free(arrayForTesting);
			goto exit;
		}

		printf("Push back works perfectly\n");


		popBack(q); 

		
		i = 0;
		listChain = q->front;
		while (listChain->next)
		{
			if (arrayForTesting[i] != listChain->data)
			{
				fprintf(stderr,"Value is not the same, Incorrect Pop Back\n");
				free(arrayForTesting);
				goto exit;
			}
			listChain = listChain->next;
			++i;
		}
	
		printf("Pop Back successully implemented\n"); 
	}

	if (showQueue->count > 0)
	{
		showLinkedList(q->front, "Your Queue");
	}

	arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
	destroyQueue(q);
	
	return exitcode;


exit:
	/* deallocate each non-null entry in argtable[] */
	arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
	return exitcode;
}
