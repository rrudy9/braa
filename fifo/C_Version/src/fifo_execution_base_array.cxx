#include <stdlib.h>
#include <stdio.h>
#include <common_project.h>
#include "argtable3.h"
#include <fifo_base_array_c.h>

#ifdef TEST_FOUND 
#include "CppUTest/MemoryLeakDetectorMallocMacros.h"
#endif

bool global_show_compute = false;
logLevel global_log_level = logLevel::info;

int main(int argc, char* argv[])
{
	struct arg_lit
		* help = NULL,
		* poppingFront = NULL,
		* getElementCount = NULL,
		* showLastValue = NULL,
		* showFirstValue = NULL,
		* showCompute = NULL,
		* checkTest = NULL,
		* showQueue = NULL,
		* showCapacity  = NULL,
		* isEmpty = NULL;

	struct arg_str
		* logLevelValue = NULL;

	struct arg_int
		* setElementCount = NULL,
		* deleteValFromQueue = NULL,
		* setRandSeedValue = NULL,
		* pushingFront = NULL,
		* insertRandomValueInChoosenIndex = NULL,
		* pushingBack = NULL;

	struct arg_end* end = NULL;

	struct Common_Project_Values fifoCommonValue = { 100000 , 30 };
	struct queue_base_array arrayQueue = { 0, NULL, -1, -1, fifoCommonValue.elementCount };

	void* argtable[] = {
		help = arg_litn(NULL, "help", 0, 1, "display this help for QUEUE FIFO <C Version (Array Base)> and exit"),
		showCompute = arg_litn("c","show-compute",0, 1, "Show Compute"),
		checkTest = arg_litn("t","show-test" ,0, 1, "Show Test Validation for implemented algorithms"),
		logLevelValue = arg_strn("v", "verbose","<info,debug,error,fatal>", 0, 1, "Set Log Level (info, debug, error, fatal) (Default : info)"),
		setElementCount = arg_intn("q", "queue-count","<n>" , 0, 1, "Set Queue Number of element (Default Value: 100k) "),
		setRandSeedValue = arg_intn("r", "rand-seed","<n>", 0, 1, "Set value for Seed Random (Default Value: 30)"),

		getElementCount = arg_litn("n", "get-element-count", 0, 1, "Get Number of element in your QUEUE"),
		showLastValue = arg_litn("l", "get-last-value", 0, 1, "Get Last Value of your Queue"),
		showFirstValue = arg_litn("f", "get-first-value", 0, 1, "Get First Value of your Queue"),
		isEmpty = arg_litn("e", "is-empty", 0, 1, "Check if your Queue is empty"),
		poppingFront = arg_litn("p", "pop-front", 0, 1, "remove first value from your queue"),
		pushingBack = arg_intn("b", "push-back","<value>", 0, 1, "Add a value at the end of your Queue "),
		insertRandomValueInChoosenIndex = arg_intn("i", "insert-in-index","<value>", 0, 1, "Add a random value at your giving index"),
		pushingFront = arg_intn("o", "push-front","<value>", 0, 1, "Add a value at the beginning of your Queue "),
		deleteValFromQueue = arg_intn("k", "delete-val","<value>", 0, 1, "Delete a value from your Queue "),
		showCapacity = arg_litn("h", "show-capacity", 0, 1, "Show Capacity"),
		showQueue = arg_litn("z", "show-queue", 0, 1, "Show Queue"),


		
		
		
		end = arg_end(20)
	};

	int exitcode = 0;
	char progname[] = "fifoC.exe";

	int nerrors;
	nerrors = arg_parse(argc, argv, argtable);

	if (argc == 1) {
		printf("Please choose one of the available commands\n\n");
		arg_print_glossary(stdout, argtable, "  %-25s %s\n");
		exitcode = 0;
		goto exit;
	}

	//If the parser returned any errors then display them and exit 
	if (nerrors > 0)
	{
		// Display the error details contained in the arg_end struct.
		arg_print_errors(stderr, end, progname);
		fprintf(stderr, "Try '%s --help' for more information.\n", progname);
		exitcode = 1;
		goto exit;
	}


	if (help->count > 0)
	{
		printf("Usage: %s", progname);
		arg_print_syntax(stdout, argtable, "\n\n");
		arg_print_glossary(stdout, argtable, "  %-25s %s\n");
		exitcode = 0;
		goto exit;
	}


	if (showCompute->count > 0)
	{
		global_show_compute = !global_show_compute;
	}

	

	if (logLevelValue->count > 0)
	{
		setLogLevel(*(logLevelValue)->sval, global_log_level);
	}

	//set project common value (random seed , and number of element in array
	if (setElementCount->count > 0)
	{
		fifoCommonValue.elementCount = *(setElementCount)->ival;
	}

	if (setRandSeedValue->count > 0)
	{
		fifoCommonValue.seedRand = *(setRandSeedValue)->ival;
	}


	srand(fifoCommonValue.seedRand);
	showCommonProjectValue(fifoCommonValue, global_log_level);
	arrayQueue.capacity = fifoCommonValue.elementCount * 2; 
	createQueueArray(arrayQueue, fifoCommonValue);


	if (poppingFront->count > 0)
	{
		popFront(arrayQueue);	
	}

	if (pushingBack->count > 0)
	{
		pushBack(arrayQueue,*(pushingBack)->ival);

	}

	if (pushingFront->count > 0)
	{
		pushFront(arrayQueue, *(pushingFront)->ival);
	}

	if (deleteValFromQueue->count > 0)
	{
		deleteChosenValue(arrayQueue, *(deleteValFromQueue)->ival);
	}

	if (getElementCount->count > 0)
	{
		printf("Total Number in Queue: %i\n", getTotalElementNumberInQueue(arrayQueue));
	}

	if (showLastValue->count > 0)
	{
		int lastValue = 0; 
		getLastValue(arrayQueue, lastValue);
	}

	if (showFirstValue->count > 0)
	{
		int firstValue = 0; 
		getFirstValue(arrayQueue, firstValue);
	}

	if (isEmpty->count > 0)
	{
		printf("Is Your List is Empty? %s\n", isQueueEmpty(arrayQueue) ? "yes" : "no");
	}
	
	if (insertRandomValueInChoosenIndex->count > 0)
	{
		int randValToAdd = rand() % arrayQueue.count; 
		if (*(insertRandomValueInChoosenIndex)->ival > 0)
			insertValueInIndex(arrayQueue, randValToAdd, *(insertRandomValueInChoosenIndex)->ival);
		else
		{
			printf("Choose a value from 1 to count (%i)\n", arrayQueue.count);
			goto exit; 
		}
	}
	
	if (checkTest->count > 0)
	{
		int countArray = arrayQueue.count;
		int* arrayForTesting = (int*)malloc(arrayQueue.count * sizeof(int));

		for (int i = arrayQueue.front; i <= arrayQueue.back; ++i)
		{
			arrayForTesting[i] = arrayQueue.queue[i];
		}

		//search
		//delete
		//insert

		int pickRandValueInArray = arrayForTesting[ (arrayQueue.count - 1 ) % rand()];

		printf("searching for value\n");
		int indexOfValInQueue = -1; 
		int getSearchedValue =  searchValue(arrayQueue, pickRandValueInArray, indexOfValInQueue);

		if (getSearchedValue != pickRandValueInArray)
		{
			fprintf(stderr,"value not found, Error in code\n");
			free(arrayForTesting);
			goto exit;
		}

		printf("Value was found , successfull search\n");

		deleteChosenValue(arrayQueue, pickRandValueInArray);
		int indexOfVal = -1;
		getSearchedValue = searchValue(arrayQueue, pickRandValueInArray, indexOfVal);
		if (indexOfVal != -1)
		{
			fprintf(stderr,"value was found\n, delete was not correctly implemented\n");
			free(arrayForTesting);
			goto exit;
		}

		insertValueInIndex(arrayQueue, pickRandValueInArray, indexOfValInQueue + 1);

		indexOfVal = -1; 
		getSearchedValue = searchValue(arrayQueue, pickRandValueInArray, indexOfVal);

		if (indexOfVal != indexOfValInQueue)
		{
			fprintf(stderr,"value not found in the right index, Error in code\n");
			free(arrayForTesting);
			goto exit;
		}

		printf("Value was inserted in the right index\n, insertion implemented Correctly\n"); 

		for (int i = arrayQueue.front; i <= arrayQueue.back; ++i)
		{
			if (arrayForTesting[i] != arrayQueue.queue[i])
			{
				fprintf(stderr,"Values are not the same, Incorrect Code\n");
				free(arrayForTesting);
				goto exit;
			}
		}
		
		printf("Checked Values success\n");
		free(arrayForTesting);

	}

	if (showCapacity->count > 0)
	{
		printf("Queue Capacity: %i\n", arrayQueue.capacity);
	}

	if (showQueue->count > 0)
	{
		displayQueue(arrayQueue);
	}

	arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
	destroyQueueArray(arrayQueue);
	return exitcode;


exit:
	/* deallocate each non-null entry in argtable[] */
	arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
	return exitcode;
}
