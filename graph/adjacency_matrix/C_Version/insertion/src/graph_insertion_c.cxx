#include "../include/graph_insertion_c.h"
#include <graph_common_c.h>


void insertDistanceValueBetweenEdges(int*** matrix, int*** edges, int numberOfEdges, bool testRun)
{

	// traverse the array of edges
	for (int i = 0; i < numberOfEdges; ++i)
	{
		//find x , y of edges
		int x = (*edges)[i][0];
		int y = (*edges)[i][1];
		
		int edgePairDistance = 0;

		if (testRun)
			edgePairDistance = rand() % 10001;
		else
			edgePairDistance = 1; 

		(*matrix)[x][y] = edgePairDistance;
		(*matrix)[y][x] = edgePairDistance;
	}
}


void insertEdgesValue(int*** edges, int numberOfVertices, int numberOfEdges, uint64_t& computeVal, bool testRun, int source, int destination)
{

	for (int i = 0; i < numberOfEdges ; ++i)
	{
		for (int j = 0; j < 2; ++j)
		{
			++computeVal;

			if (j == 0)
			{

				if (!testRun)
				{
					(*edges)[i][j] = source;
				}
				else
				{

					int randSource = rand() % numberOfVertices;
					(*edges)[i][j] = randSource;
				}
			}

			if (j == 1)
			{
				if (!testRun)
				{
					(*edges)[i][j] = destination;
				}
				else
				{
					int randDestination = rand() % numberOfVertices;
					(*edges)[i][j] = randDestination;
				}
			}
		}
	}
}



void implementedInsertion(int *** matrix, int*** edges, int numberOfEdges, bool testRun)
{
	size_t timeElasped = 0;
	int computeVal = 0;

	getTimeLapsComputeVariables(timeElasped, insertDistanceValueBetweenEdges, matrix, edges, numberOfEdges, testRun);

	showComputeElapsedTime(computeVal, timeElasped, "Graph Edge Insertion", global_show_compute, global_log_level);

}