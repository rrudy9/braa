#pragma once

#include <common_project.h>

#ifdef TEST_FOUND 
#include "CppUTest/MemoryLeakDetectorMallocMacros.h"
#endif



void insertDistanceValueBetweenEdges(int*** matrix, int*** edges, int numberOfEdges, bool testRun);

void implementedInsertion(int*** matrix, int*** edges, int numberOfEdges, bool testRun);

void insertEdgesValue(int*** edges,int numberOfVertices, int numberOfEdges, uint64_t& computeVal, bool testRun, int source, int destination);
