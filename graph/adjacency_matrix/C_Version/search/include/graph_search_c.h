#pragma once

#include <common_project.h>
#include <graph_common_c.h>

#ifdef TEST_FOUND 
#include "CppUTest/MemoryLeakDetectorMallocMacros.h"
#endif

#include <limits.h>

void depthFirstSearch(int** matrix, int vertice, int numberVertices, int numberOfEdges, uint64_t& computeVal);

void dfsUtil(int** matrix, int vertice, bool* visited, int numberOfEdges, uint64_t& computeVal);

void dijkstra(int** matrix, int source, int numberVertices, int** dist, uint64_t& computeVal);

int minDistance(int* dist, bool* visited, int numberVertices);

void printSolution(int* dist, int numberVertices);

void aStarAlgorithm(int*** matrix, int** dist, int numberVertices, int source, int destination);

void unRealHeuristicValue(int** shortedPathSourceTarget, int** dist, int source, int target);

void implementedDepthFirstSearch(int** matrix, int shortedPathToVertice, int numberVertices, int numberOfEdges);

void implementedDijkstra(int** matrix, int source, int numberVertices, int** shortedDistanceFromSource);
