#include "../include/graph_search_c.h"

void depthFirstSearch(int** matrix, int vertice, int numberVertices, int numberOfEdges, uint64_t& computeVal)
{

	bool* visited = (bool*)malloc(numberVertices * sizeof(bool));

	if (!visited)
	{
		fprintf(stderr, "Can't allocate your array for depth first search\n");
		exit(0);
	}

	for (int vertices = 0; vertices < numberVertices; ++vertices)
	{
		visited[vertices] = false;
	}

	dfsUtil(matrix, vertice, visited, numberOfEdges, computeVal);
	free(visited);

}


void dfsUtil(int** matrix, int vertice, bool* visited, int numberOfEdges, uint64_t& computeVal)
{

	visited[vertice] = true;
	if(global_log_level <= logLevel::debug)
		printf("%d-> ", vertice);


	for (int j = 0; j < numberOfEdges ; ++j)
	{
		++computeVal;

		if (((matrix)[vertice][j] > 0) && (!visited[j]))
		{

			dfsUtil(matrix, j, visited, numberOfEdges, computeVal);
		}
	}
}


int minDistance(int* dist, bool* visited, int numberVertices)
{
	int min = INT_MAX, minIndex;
	for (int v = 0; v < numberVertices; ++v)
		if (visited[v] == false && dist[v] <= min)
			min = dist[v], minIndex = v;

	return minIndex;
}

void printSolution(int* dist, int numberVertices)
{
	//printf("un real heuristic from Source to target (last value)\n");
	for (int i = 0; i < numberVertices; ++i)
	{
		if (dist[i])
		{
			printf("Vertex : %d - Shorted Distance: %d\n", i, dist[i]);
		}
	}
}



void dijkstra(int** matrix, int source, int numberVertices, int** dist, uint64_t& computeVal)
{

	(*dist) = (int*)malloc(numberVertices * sizeof(int));

	bool* visited = (bool*)malloc(numberVertices * sizeof(bool));

	if (!visited)
	{
		fprintf(stderr, "Can't allocate your visited array for dijkstra\n");
		exit(0);
	}

	//initialize all distance to infinite
	for (int i = 0; i < numberVertices; ++i)
		(*dist)[i] = INT_MAX, visited[i] = false;

	//distance of source vertex from itself 
	(*dist)[source] = 0;

	//find shortest path for all vertices
	++computeVal;

	for (int count = 0; count < numberVertices - 1; ++count)
	{
		//minimum distance vertex from the set of
		//vertices not yet processed, 
		int u = minDistance((*dist), visited, numberVertices);
		visited[u] = true;

		//update dist value of the adjacent vertices of the picked vertex
		for (int v = 0; v < numberVertices; ++v)
		{
			++computeVal;

			//update dist[v] only if it is not in visited
			//there is an edge from u to v 
			if (!visited[v] && (matrix)[u][v] && (*dist)[u] != INT_MAX && (*dist)[u] + (matrix)[u][v] < (*dist)[v])
			{
				
				(*dist)[v] = (*dist)[u] + (matrix)[u][v];
			}
		}
	}


	//	free(dist);
	free(visited);

}

void unRealHeuristicValue(int** shortedPathSourceTarget, int** dist, int source, int target)
{
	(*shortedPathSourceTarget)[source] = (*dist)[target];

}

void aStarAlgorithm(int*** matrix, int** dist, int numberVertices, int source, int destination)
{
	/*
	bool* visited = (bool*)malloc(numberVertices * sizeof(bool));

	struct queue* ll = (struct queue*)malloc(sizeof(struct queue));
	initialize(ll);

	struct queue* finalList = (struct queue*)malloc(sizeof(struct queue));
	initialize(finalList);

	//int f[numberVertices], g , h; 


	for (int count = 0; count < numberVertices - 1; ++count)
	{

		//finalList.pushBack(finalList,count);

		for (int v = returnLastValue(finalList); v < numberVertices; ++v)
		{
			//update dist[v] only if it is not in visited
			if ((*matrix)[count][v])
			{
				//f[i++] = (*matrix)[count][v] + (*dist)[v];
			//	ll.pushBack(ll, 	
			}

		}

	}

	*/

}

void implementedDepthFirstSearch(int** matrix, int shortedPathToVertice, int numberVertices, int numberOfEdges)
{

	size_t timeElasped = 0;
	uint64_t computeVal = 0;

	getTimeLapsComputeVariables(timeElasped, depthFirstSearch, matrix, shortedPathToVertice, numberVertices, numberOfEdges, computeVal);
	
	printf("\n");
	
	showComputeElapsedTime(computeVal, timeElasped, "Depth First Search", global_show_compute, global_log_level);
}


void implementedDijkstra(int** matrix, int source, int numberVertices, int** shortedDistanceFromSource)
{

	size_t timeElasped = 0;
	uint64_t computeVal = 0;

	getTimeLapsComputeVariables(timeElasped, dijkstra, matrix, source, numberVertices,shortedDistanceFromSource, computeVal);
	
	printf("\n");
	
	showComputeElapsedTime(computeVal, timeElasped, "Dijkstra Algorithm", global_show_compute, global_log_level);
}




