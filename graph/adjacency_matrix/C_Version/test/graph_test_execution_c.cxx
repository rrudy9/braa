#include "CppUTest/TestHarness.h"
#include "CppUTest/MemoryLeakDetectorMallocMacros.h"

#include <common_project.h>
#include <graph_common_c.h>
#include <graph_search_c.h>
#include <graph_insertion_c.h>



TEST_GROUP(TEST_GRAPH_ADJACENCY_MATRIX_C)
{
	struct Common_Project_Values graphCommonValue = { global_test_count_element, global_test_rand_seed };
	int** adjMatrix = NULL;
	int** edges = NULL;
	
	void setup()
	{
		showCommonProjectValue(graphCommonValue, global_log_level);
		createMatrix(&adjMatrix, &edges, graphCommonValue, graphCommonValue.elementCount);
	}

	void teardown()
	{
		destroyGraph(adjMatrix, edges, graphCommonValue.elementCount, graphCommonValue.elementCount);
	}
};

TEST(TEST_GRAPH_ADJACENCY_MATRIX_C, dijkstra)
{
	int valToSearch = rand() % graphCommonValue.elementCount;

	if (global_log_level <= logLevel::debug)
		printf("Shorted Distance for %i\n", valToSearch);

	int* shortedDistanceToEveryVertice = NULL;

	implementedDijkstra(adjMatrix, valToSearch, graphCommonValue.elementCount, &shortedDistanceToEveryVertice);

	if (global_log_level <= logLevel::debug)
	{
		printf("Shorted Distance to every vertex from %i\n", valToSearch);
		printSolution(shortedDistanceToEveryVertice, graphCommonValue.elementCount);
	}

	free(shortedDistanceToEveryVertice);

}

TEST(TEST_GRAPH_ADJACENCY_MATRIX_C, search)
{

	int valToSearch = rand() % graphCommonValue.elementCount;

	if (global_log_level <= logLevel::debug)
		printf("shorted Distance for %i\n", valToSearch);

	// check fo values 
	implementedDepthFirstSearch(adjMatrix, valToSearch, graphCommonValue.elementCount, graphCommonValue.elementCount);

	FAIL("force failing for search on purpose");

}

TEST(TEST_GRAPH_ADJACENCY_MATRIX_C, insertion)
{


	int source = rand() % graphCommonValue.elementCount;
	int destination = rand() % graphCommonValue.elementCount;

	if (global_log_level <= logLevel::debug)
		printf("source, destination to add ( %i , %i )\n", source, destination);

	uint64_t cc = 0; 
	insertEdgesValue(&edges,graphCommonValue.elementCount, 1, cc, true, source, destination);

	implementedInsertion(&adjMatrix, &edges, graphCommonValue.elementCount, false);

	// faire un test
}


