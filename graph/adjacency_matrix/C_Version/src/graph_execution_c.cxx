#include <stdlib.h>
#include <stdio.h>
#include <common_project.h>
#include <graph_common_c.h>
#include <graph_search_c.h>
#include <graph_insertion_c.h>
#include <argtable3.h>


#ifdef TEST_FOUND 
#include "CppUTest/MemoryLeakDetectorMallocMacros.h"
#endif

bool global_show_compute = false;
logLevel global_log_level = logLevel::info;



int main(int argc, char* argv[])
{

	struct arg_lit
		* help = NULL,
		* showCompute = NULL,
		* showGraph = NULL;

	struct arg_str
		* logLevelValue = NULL,
		* insertion = NULL;


	struct arg_int
		* setElementCount = NULL,
		* setRandSeedValue = NULL,
		* setNumberOfEdges = NULL, 
		* depthFirstSearch = NULL,
		* dijkstraAlgo = NULL;

	struct arg_end* end = NULL;

	struct Common_Project_Values graphCommonValue = { 100000 , 30 };
	int** adjMatrix = NULL;
	int** edges = NULL;
	int numberOfEdges = 100000;


	void* argtable[] = {
		help = arg_litn(NULL, "help", 0, 1, "display this help for Graph - Adjacency Matrix <C Version> and exit"),
		showCompute = arg_litn("c","show-compute",0, 1, "Show Compute"),
		logLevelValue = arg_strn("v", "verbose","<info,debug,error,fatal>", 0, 1, "Set Log Level (info, debug, error, fatal) (Default : info)"),
		setElementCount = arg_intn("g", "graph-count","<n>" , 0, 1, "Set Graph Number of element (Default Value: 100k)"),
		setRandSeedValue = arg_intn("r", "rand-seed","<n>", 0, 1, "Set value for Seed Random (Default Value: 30)"),
		setNumberOfEdges = arg_intn("e", "edges","<n>", 0, 1, "Set Count for Edges (Default Value: 100k) / better not to exceed it too much compare to number of vertices"),

		depthFirstSearch = arg_intn("s", "search","<your dest value> (integer)", 0, 1, "Access Graph - Adjacency Matrix C Search Algorithm implementation (Depth First Search)"),
		dijkstraAlgo = arg_intn("d", "dijkstra","<your dest value> (integer)", 0, 1, "Access Graph - Adjacency Matrix C Search Algorithm implementation (Dijkstra Algorithm)"),
		insertion = arg_strn("i", "insertion", "<source,destination> (integer)" ,0, 1, "Access Graph - Adjacency Matrix C Insertion Algorithm implementation"),
		showGraph = arg_litn("p", "show-graph", 0, 1, "Display Graph "),

		end = arg_end(20)
	};

	int exitcode = 0;
	char progname[] = "graphAdjMat.exe";

	int nerrors;
	nerrors = arg_parse(argc, argv, argtable);

	if (argc == 1) {
		printf("Please choose one of the available commands\n\n");
		arg_print_glossary(stdout, argtable, "  %-25s %s\n");
		exitcode = 0;
		goto exit;
	}

	//If the parser returned any errors then display them and exit 
	if (nerrors > 0)
	{
		// Display the error details contained in the arg_end struct.
		arg_print_errors(stderr, end, progname);
		fprintf(stderr,"Try '%s --help' for more information.\n", progname);
		exitcode = 1;
		goto exit;
	}


	if (help->count > 0)
	{
		printf("Usage: %s", progname);
		arg_print_syntax(stdout, argtable, "\n\n");
		arg_print_glossary(stdout, argtable, "  %-25s %s\n");
		exitcode = 0;
		goto exit;
	}


	if (showCompute->count > 0)
	{
		global_show_compute = !global_show_compute;
	}

	if (logLevelValue->count > 0)
	{
		setLogLevel(*(logLevelValue)->sval, global_log_level);
	}

	if (setElementCount->count > 0)
	{
		graphCommonValue.elementCount = *(setElementCount)->ival;
	}

	if (setRandSeedValue->count > 0)
	{
		graphCommonValue.seedRand = *(setRandSeedValue)->ival;
	}

	if (setNumberOfEdges->count > 0)
	{
		numberOfEdges= *(setNumberOfEdges)->ival;
	}

	srand(graphCommonValue.seedRand);
	showCommonProjectValue(graphCommonValue, global_log_level);
	printf("Number of Edges : %i\n", numberOfEdges);

	createMatrix(&adjMatrix, &edges, graphCommonValue, numberOfEdges);

	if (insertion->count > 0)
	{

		std::string src = "";
		std::string dest = "";
		int checkCommaInString = 0;
		char* valToInsert = (char*)malloc(strlen(*(insertion)->sval) + 1 * sizeof(char));
		if (valToInsert) strcpy(valToInsert, *(insertion)->sval);
		else
		{
			fprintf(stderr, "Can't allocate your value from arguments\n");
			exit(0);
		}

		std::string str = "";
		for (int i = 0; i < strlen(valToInsert); ++i)
		{
			str += valToInsert[i];
			if (valToInsert[i] == ',')
			{
				++checkCommaInString;
				if (checkCommaInString == 1)
				{
					src = str;
					src[src.size() - 1] = '\0';
					str = "";
				}
				if (checkCommaInString == 2)
				{
					dest = str;
					dest[dest.size() - 1] = '\0';
					break;
				}
			}
		}

		if (checkCommaInString >= 1)
		{
			if (checkCommaInString == 1) dest = str;

			int source = atoi(src.c_str());
			int destination = atoi(dest.c_str());

			if ((source == destination) || ((source > graphCommonValue.elementCount) || (destination > graphCommonValue.elementCount)))
			{
				fprintf(stderr, "{x,y} should be an element of V (number of vertices) & x different than y ");
				exitcode = 2;
				goto exit;
			}

			uint64_t cc = 0; 
			insertEdgesValue(&edges,graphCommonValue.elementCount, 1,cc,false,source, destination);

			implementedInsertion(&adjMatrix, &edges, 1, false);

		}
	}

	if (depthFirstSearch->count > 0)
	{
		implementedDepthFirstSearch(adjMatrix, *(depthFirstSearch)->ival, graphCommonValue.elementCount, numberOfEdges);
	}

	if (dijkstraAlgo->count > 0)
	{
		int* shortedDistanceToEveryVertice = NULL;

		implementedDijkstra(adjMatrix, *(dijkstraAlgo)->ival, numberOfEdges, &shortedDistanceToEveryVertice);
		
		if (global_log_level <= logLevel::debug)
		{
			printf("Shorted Distance to every vertex from %i\n", *(dijkstraAlgo)->ival);
			printSolution(shortedDistanceToEveryVertice, graphCommonValue.elementCount);
		}
		
		free(shortedDistanceToEveryVertice);

	}

	if (showGraph->count > 0)
	{
		printf("Display Graph\n");
		printAdjMatrix(&adjMatrix, graphCommonValue.elementCount);
	}

	arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));


	destroyGraph(adjMatrix, edges, graphCommonValue.elementCount, numberOfEdges);

	return exitcode;


exit:
	/* deallocate each non-null entry in argtable[] */
	arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
	return exitcode;

}


