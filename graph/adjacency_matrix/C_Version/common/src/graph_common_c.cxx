 #include "../include/graph_common_c.h"
#include <graph_insertion_c.h>

// je ne passe pas la structure pr garder le nom plus clair pr le graphe...
void initializeMatrix(int*** matrix, int numberVertices)
{
	// initialize all value to zero	
	*matrix = (int**)malloc((numberVertices * numberVertices) * sizeof(int*));

	if (!(*matrix))
	{
		fprintf(stderr,"can't allocate your Matrix\n");
		exit(0);
	}

	for (int i = 0; i < numberVertices; ++i)
	{
		(*matrix)[i] = (int*)malloc(numberVertices * sizeof(int));

		for (int j = 0; j < numberVertices; ++j)
		{
			(*matrix)[i][j] = 0;
		}

		//(*matrix)[i][i] = 0; 
	}

}

void initializeEdges(int*** edges, int numberOfEdges)
{
	*edges = (int**)malloc((numberOfEdges * 2 ) * sizeof(int*));

	if (!(*edges))
	{
		fprintf(stderr, "Can't allocate your edges");
		exit(0);
	}

	for (int i = 0; i < numberOfEdges ; ++i)
	{
		(*edges)[i] = (int*)malloc( 2 * sizeof(int));

		for (int j = 0; j < 2; ++j)
		{
			(*edges)[i][j] = 0;
		}
	}
}

void createMatrix(int*** matrix, int***edges, struct Common_Project_Values projectVal, int numberOfEdges)
{
	initializeEdges(edges, numberOfEdges);

	initializeMatrix(matrix, projectVal.elementCount);

	uint64_t cc = 0;
	int source = 0; 
	int destination = 0; 
	insertEdgesValue(edges,projectVal.elementCount, numberOfEdges, cc, true, source, destination);

	implementedInsertion(matrix, edges, numberOfEdges, true);

}


void printAdjMatrix(int*** adj, int numberVertices)
{
	for (int i = 0; i < numberVertices; ++i)
	{
		for (int j = 0; j < numberVertices; ++j)
		{
			printf("%d ", (*adj)[i][j]);
		}
		printf("\n");
	}
}

void printEdgeMatrix(int*** adj, int numberEdges)
{
	for (int i = 0; i < numberEdges ; ++i)
	{
		for (int j = 0; j < 2; ++j)
		{
			printf("%d ", (*adj)[i][j]);
		}
		printf("\n");
	}
}


void freeMatrix(int*** matrix, int numberVertices)
{
	for (int i = 0; i < numberVertices; ++i)
	{
		free((*matrix)[i]);
	}

	free(*matrix);
}


void destroyGraph(int** matrix, int** edges, int elementCount, int numberOfEdges)
{
	freeMatrix(&matrix, elementCount);
	freeMatrix(&edges, numberOfEdges);
}



void getTimeLapsComputeVariables(size_t& elapsedTime, void (*f)(int***, int, uint64_t&, bool), int*** matrix, int numberOfEdges, uint64_t& computeVal, bool testRun)
{
	std::chrono::steady_clock::time_point t1 = std::chrono::high_resolution_clock::now();
	f(matrix, numberOfEdges, computeVal, testRun);
	std::chrono::steady_clock::time_point t2 = std::chrono::high_resolution_clock::now();

	elapsedTime = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();
}

void getTimeLapsComputeVariables(size_t& elapsedTime, void (*f)(int***, int***, int, bool), int*** matrix, int*** edges, int numberOfEdges, bool testRun)
{
	std::chrono::steady_clock::time_point t1 = std::chrono::high_resolution_clock::now();
	f(matrix, edges, numberOfEdges, testRun);
	std::chrono::steady_clock::time_point t2 = std::chrono::high_resolution_clock::now();

	elapsedTime = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();
} 

void getTimeLapsComputeVariables(size_t& elapsedTime, void (*f)(int**, int, int, int, uint64_t&), int** matrix, int vertice, int numberVertices, int numberOfEdges, uint64_t& computeVal)
{
	std::chrono::steady_clock::time_point t1 = std::chrono::high_resolution_clock::now();
	f(matrix, vertice, numberVertices, numberOfEdges, computeVal);
	std::chrono::steady_clock::time_point t2 = std::chrono::high_resolution_clock::now();

	elapsedTime = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();
}

void getTimeLapsComputeVariables(size_t& elapsedTime, void (*f)(int**, int, int, int**, uint64_t&), int** matrix, int source, int numberVertices, int** dist, uint64_t& computeVal)
{
	std::chrono::steady_clock::time_point t1 = std::chrono::high_resolution_clock::now();
	f(matrix, source, numberVertices, dist, computeVal);
	std::chrono::steady_clock::time_point t2 = std::chrono::high_resolution_clock::now();

	elapsedTime = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();
}


