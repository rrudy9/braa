file(GLOB graph_adjacency_matrix_common_c
	.cxx .hxx 
	src/*.cxx 
	include/*.h)

add_library(graph_adjacency_matrix_common_c_lib ${graph_adjacency_matrix_common_c})


set(linkingLibrary 
	${CPPUTEST_LDFLAGS}  
	common_project_lib
	graph_adjacency_matrix_insertion_c_lib
	)

set(linkingDirectories 
	"${CPPUTEST_INCLUDE_DIRS}"
	"${PROJECT_SOURCE_DIR}/graph/Adjacency_Matrix/C_Version/insertion/include" 
	"${PROJECT_SOURCE_DIR}/common_project/include" 
	)

	
if (CPPUTEST_LIBRARIES)
	add_compile_definitions(TEST_FOUND="test")
endif(CPPUTEST_LIBRARIES)



target_link_libraries(graph_adjacency_matrix_common_c_lib PRIVATE ${linkingLibrary})

target_include_directories(graph_adjacency_matrix_common_c_lib PRIVATE ${linkingDirectories})

set_property(TARGET graph_adjacency_matrix_common_c_lib PROPERTY FOLDER "graph/Adjacency_Matrix/C_Version/common")
