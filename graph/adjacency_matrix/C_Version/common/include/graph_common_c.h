#pragma once

#include <stdlib.h>
#include <stdio.h>
#include <common_project.h>
#include <chrono>
#include <string>
#include <iostream>
#include <random>

#ifdef TEST_FOUND 
#include "CppUTest/MemoryLeakDetectorMallocMacros.h"
#endif

#define IN 99


void initializeMatrix(int*** matrix, int numberVertices);

void printAdjMatrix(int*** adj, int numberVertices);

void printEdgeMatrix(int*** adj, int numberEdges);

void freeMatrix(int*** matrix, int numberVertices);

void destroyGraph(int** matrix, int** edges, int elementCount, int numberOfEdges);

void initializeEdges(int*** edges, int numberOfEdges);

void createMatrix(int***,int***, struct Common_Project_Values, int);

void getTimeLapsComputeVariables(size_t& elapsedTime, void (*f)(int*** , int , uint64_t&, bool), int*** matrix, int numberOfEdges, uint64_t& computeVal, bool testRun);
void getTimeLapsComputeVariables(size_t& elapsedTime, void (*f)(int***, int***, int, bool), int*** matrix, int*** edges, int numberOfEdges, bool testRun);
void getTimeLapsComputeVariables(size_t& elapsedTime, void (*f)(int**, int, int, int, uint64_t&), int** matrix, int vertice, int numberVertices, int numberOfEdges, uint64_t& computeVal);
void getTimeLapsComputeVariables(size_t& elapsedTime, void (*f)(int**, int, int, int**, uint64_t& ), int** matrix, int source, int numberVertices, int** dist, uint64_t& computeVal);
