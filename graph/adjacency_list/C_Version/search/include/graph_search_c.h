#pragma once

#include <common_project.h>
#include <graph_common_c.h>

#ifdef TEST_FOUND 
#include "CppUTest/MemoryLeakDetectorMallocMacros.h"
#endif


void depthFirstSearch(struct Graph* graph, int destFromSource, uint64_t& computeVal);

void dfsUtil(struct Graph* graph, int destFromSource, bool* visited, uint64_t& computeVal);

void breadthFirstSearch(struct Graph* graph, int startVertice);

void implementedSearch(struct Graph*graph, int destFromSource);