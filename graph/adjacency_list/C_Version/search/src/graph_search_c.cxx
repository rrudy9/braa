#include "../include/graph_search_c.h"

void depthFirstSearch(struct Graph* graph, int destFromSource, uint64_t& computeVal)
{

	bool* visited = (bool*)malloc(graph->vertices + 1 * sizeof(bool));

	//initialize
	for (int vertices = 0; vertices < graph->vertices; ++vertices)
	{
		++computeVal;
		visited[vertices] = false;
	}

	dfsUtil(graph, destFromSource, visited, computeVal);
	free(visited);

}


void dfsUtil(struct Graph* graph, int destFromSource, bool* visited, uint64_t& computeVal)
{
	visited[destFromSource] = true;
	++computeVal;
	if(global_log_level <= logLevel::debug)
		printf("%i ,", destFromSource);

	struct adj_list_node* contentGraph = graph->array[destFromSource].head;
	
	while (contentGraph)
	{
		++computeVal;

		if (!visited[contentGraph->dest])
		{
			dfsUtil(graph, contentGraph->dest, visited, computeVal);
		}
		contentGraph = contentGraph->next;
	}
}

void breadthFirstSearch(struct Graph* graph, int startVertice)
{

	/*struct queue* q = (struct queue*)malloc(sizeof(struct queue));
	initialize(q);

	bool* visited = (bool*)malloc(graph->vertices * sizeof(bool));

	for (int vertices = 0; vertices < graph->vertices; ++vertices)
	{
		visited[vertices] = false;
	}


	visited[startVertice] = true;
	pushBack(q, startVertice);
	*/

	// need to find why it is not working (will check it later
	/*
	while(!isEmpty(q))
	{

		int currentVertice = returnFrontValue(q);
		printf("%d, ", currentVertice);

		popFront(q);
		struct adj_list_node * contentGraph = graph->array[currentVertice].head;
		while (contentGraph)
		{
			if (!visited[contentGraph->dest])
			{
				visited[contentGraph->dest] = true;
				pushBack(q, contentGraph->dest);
			}
			contentGraph = contentGraph->next;
		}
	}

	free(visited);
	//freeList(q->front);
	free(q);

	*/
}

void implementedSearch(struct Graph* graph, int destFromSource)
{
	size_t timeElapsed = 0;
	uint64_t computeVal = 0;

	printf("\nDepth First Traversal Search for %i\n", destFromSource);

	getTimeLapsComputeVariables(timeElapsed, depthFirstSearch, graph, destFromSource, computeVal);
	printf("\n");
	showComputeElapsedTime(computeVal, timeElapsed, "Graph Depth First Traversal Search (Adjacency List)", global_show_compute, global_log_level);

}