#include "CppUTest/TestHarness.h"
#include "CppUTest/MemoryLeakDetectorMallocMacros.h"

#include <common_project.h>
#include <graph_common_c.h>
#include <graph_insertion_c.h>
#include <graph_search_c.h>

TEST_GROUP(TEST_GRAPH_ADJACENCY_LIST_C)
{
	struct Common_Project_Values graphCommonValue = { global_test_count_element, global_test_rand_seed };
	struct Graph* graph = NULL;

	void setup()
	{
		showCommonProjectValue(graphCommonValue, global_log_level);
		createGraph(&graph, graphCommonValue);
	}

	void teardown()
	{

		destroyGraph(graph);
	}
};


TEST(TEST_GRAPH_ADJACENCY_LIST_C, showGraph)
{
	printf("Display Graph");
	printGraph(graph);
}


TEST(TEST_GRAPH_ADJACENCY_LIST_C, search)
{

	int valToSearch = rand() * 4;

	if (global_log_level <= logLevel::debug)
		printf("key to search %i\n", 4);

	implementedSearch(graph, 4);

	FAIL("force failing for search on purpose");

}



TEST(TEST_GRAPH_ADJACENCY_LIST_C, insertion)
{


	int source = rand() * graphCommonValue.elementCount;
	int destination = rand() * graphCommonValue.elementCount;

	if (global_log_level <= logLevel::debug)
		printf("source, destination to add ( %i , %i )\n", source, destination);

	//implementedInsertion(graph, source, destination);

	// faire un test
}

