#pragma once

#include <common_project.h>

#ifdef TEST_FOUND 
#include "CppUTest/MemoryLeakDetectorMallocMacros.h"
#endif

struct Graph;

void addEdge(struct Graph* graph, int src, int dest);

void implementedInsertion(struct Graph* graph, int src, int dest);