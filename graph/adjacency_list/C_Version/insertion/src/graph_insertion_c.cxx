#include "../include/graph_insertion_c.h"
#include <graph_common_c.h>



void addEdge(struct Graph* graph, int src, int dest)
{
	struct adj_list_node* node = NULL;
	createNode(&node, dest);

	node->next = graph->array[src].head;
	graph->array[src].head = node;
	
	//graph is undirected , add an edge from dest to src
	createNode(&node,src);
	node->next = graph->array[dest].head;
	graph->array[dest].head = node;

}


void implementedInsertion(struct Graph* graph, int src, int dest)
{
	size_t timeElapsed = 0;
	int computeVal = 0;

	getTimeLapsComputeVariables(timeElapsed, addEdge, graph, src, dest);
	showComputeElapsedTime(computeVal, timeElapsed, "Graph Insertion", global_show_compute, global_log_level);

}