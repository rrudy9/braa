
#ifndef GRAPH_COMMON_C
#define GRAPH_COMMON_C

#include <stdlib.h>
#include <stdio.h>
#include <common_project.h>
#include <chrono>
#include <string>
#include <iostream>
#include <random>
#include <math.h>

#ifdef TEST_FOUND 
#include "CppUTest/MemoryLeakDetectorMallocMacros.h"
#endif

struct adj_list_node
{
	int dest;
	struct adj_list_node* next;
};

struct adj_list
{
	struct adj_list_node* head;
};

struct Graph
{
	int vertices;
	struct adj_list* array;
};


void createNode(struct adj_list_node** node,int dest);

void initializeGraph(struct Graph**, struct Common_Project_Values);

void createGraph(struct Graph**, struct Common_Project_Values);

void printGraph(struct Graph* graph);

void destroyGraph(struct Graph* graph);

void getTimeLapsComputeVariables(size_t& elapsedTime, void (*f)(struct Graph*, int, int), struct Graph* graph, int src, int dest);
void getTimeLapsComputeVariables(size_t& elapsedTime, void (*f)(struct Graph*, int, uint64_t&), struct Graph* graph, int vertice, uint64_t& computeVal);

#endif
