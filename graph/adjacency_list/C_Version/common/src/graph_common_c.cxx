#include "../include/graph_common_c.h"
#include <graph_insertion_c.h>


void createNode(struct adj_list_node**node, int dest)
{
	*node = (struct adj_list_node*)malloc(sizeof(struct adj_list_node));
	
	if (!(*node))
	{
		fprintf(stderr, "Can't alloacte your node \n");
		exit(0);
	}

	(*node)->dest = dest;
	(*node)->next = NULL;

}

void initializeGraph(struct Graph** graph , struct Common_Project_Values projectVal)
{
	*graph = (struct Graph*)malloc( sizeof(struct Graph));

	if (!*graph)
	{
		fprintf(stderr, "Can't alloacte your graph \n");
		exit(0);
	}

	(*graph)->vertices = projectVal.elementCount;

	(*graph)->array = (struct adj_list*)malloc(projectVal.elementCount * sizeof(struct adj_list));

	if (!(*graph)->array)
	{
		fprintf(stderr, "Can't alloacte your list in graph \n");
		exit(0);
	}
	// change how it is done here
	for (int i = 1; i <= projectVal.elementCount ; ++i)
	{
		(*graph)->array[i].head = NULL;
	}

}

void createGraph(struct Graph** graph, struct Common_Project_Values projectVal)
{
	initializeGraph(graph, projectVal);

	for (int i = 1; i <= projectVal.elementCount; ++i)
	{
		int source = rand() % projectVal.elementCount  + 1 ; 
		int destination = rand() % projectVal.elementCount + 1; 
		addEdge(*graph, source, destination);
	}


	

}

void printGraph(struct Graph* graph)
{

	for (int vertices = 1; vertices <= graph->vertices; ++vertices)
	{
		printf("\n Ajacency list of vertex %i\n, head", vertices);
		struct adj_list_node* items = graph->array[vertices].head;
		while (items)
		{
			printf("-> %i", items->dest);
			items = items->next;
		}
		printf("\n");
	}

}

void destroyGraph(struct Graph* graph)
{
	for (int vertices = 0; vertices < graph->vertices; ++vertices)
	{
		struct adj_list_node* contentGraph = graph->array[vertices].head;
		while (contentGraph)
		{
			struct adj_list_node* tmp = contentGraph;
			contentGraph = contentGraph->next;
			free(tmp);
		}


	}
	free(graph->array);
	free(graph);
}

void getTimeLapsComputeVariables(size_t& elapsedTime, void (*f)(struct Graph*, int, int), struct Graph* graph, int src, int dest)
{
	std::chrono::steady_clock::time_point t1 = std::chrono::high_resolution_clock::now();
	f(graph, src, dest);
	std::chrono::steady_clock::time_point t2 = std::chrono::high_resolution_clock::now();

	elapsedTime = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();

}

void getTimeLapsComputeVariables(size_t& elapsedTime, void (*f)(struct Graph*, int, uint64_t&), struct Graph* graph, int vertice, uint64_t& computeVal)
{

	std::chrono::steady_clock::time_point t1 = std::chrono::high_resolution_clock::now();
	f(graph,vertice,computeVal);
	std::chrono::steady_clock::time_point t2 = std::chrono::high_resolution_clock::now();

	elapsedTime = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();
}
