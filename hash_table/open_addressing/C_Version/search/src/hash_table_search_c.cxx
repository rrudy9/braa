#include "../include/hash_table_search_c.h"

bool searchKey(struct hash_table* table, int key, uint64_t& computeVal)
{

	for (int i = 0; i < table->count; ++i)
	{
		++computeVal;

		struct hash_item* current = (struct hash_item*)table[i].data;
		if (current != NULL)
		{
			if (current->key == key)
			{
				if (global_log_level <= logLevel::debug)
					printf("\n Key: %i found with the following value: %i\n", current->key, current->value);
				
				return true; 
			}
		}

	}

	return false; 
}


void implementedSearch(struct hash_table* table, int valToSearch)
{
	if (table->count != 0)
	{
		size_t timeElapsed = 0;
		uint64_t computeVal = 0;

		if (!getTimeLapsComputeVariables(timeElapsed, searchKey, table, valToSearch, computeVal))
		{
			printf("key : %i not found\n", valToSearch);
		}
		
		showComputeElapsedTime(computeVal, timeElapsed, "Hash Table Selection", global_show_compute, global_log_level);
	}
	else
	{
		printf("Try To insert a value first, your table is empty!\n");
	}
}
