#pragma once

#include <common_project.h>
#include <hash_table_common_c.h>

#ifdef TEST_FOUND 
#include "CppUTest/MemoryLeakDetectorMallocMacros.h"
#endif


bool searchKey(struct hash_table* table, int key, uint64_t& computeVal);

void implementedSearch(struct hash_table* table, int valToSearch);
