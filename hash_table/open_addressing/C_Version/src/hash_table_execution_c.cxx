#include <stdlib.h>
#include <stdio.h>
#include <common_project.h>
#include <hash_table_deletion_c.h>
#include <hash_table_common_c.h>
#include <hash_table_insertion_c.h>
#include <hash_table_search_c.h>
#include "argtable3.h"


#ifdef TEST_FOUND 
#include "CppUTest/MemoryLeakDetectorMallocMacros.h"
#endif

bool global_show_compute = false;
logLevel global_log_level = logLevel::info;

int main(int argc, char* argv[])
{

	struct arg_lit
		* help = NULL,
		* showCompute = NULL,
		* checkResultValidation = NULL,
		* showTable = NULL;

	struct arg_str
		* logLevelValue = NULL,
		* insertion = NULL;


	struct arg_int
		* setElementCount = NULL,
		* setRandSeedValue = NULL,
		* search = NULL,
		* deletion = NULL;

	struct arg_end* end = NULL;

	struct Common_Project_Values tableCommonValue = { 100000 , 30 };
	struct hash_table* table = NULL;

	void* argtable[] = {
	help = arg_litn(NULL, "help", 0, 1, "display this help for Hash Table - Open Addressing <C Version> and exit"),
	showCompute = arg_litn("c","show-compute",0, 1, "Show Compute"),
	logLevelValue = arg_strn("v", "verbose","<info,debug,error,fatal>", 0, 1, "Set Log Level (info, debug, error, fatal) (Default : info)"),
	setElementCount = arg_intn("h", "hash-table-count","<n>" , 0, 1, "Set Hash Table Number of element (Default Value: 100k)"),
	setRandSeedValue = arg_intn("r", "rand-seed","<n>", 0, 1, "Set value for Seed Random (Default Value: 30)"),

	checkResultValidation = arg_litn("t","run-test" ,0, 1, "Run Test for implemented algorithms"),
	search = arg_intn("s", "search","<your key value> (integer)", 0, 1, "Access Hash Table - Open Addressing C Search Algorithm implementation"),
	insertion = arg_strn("i", "insertion", "<key,value> (integer)" ,0, 1, "Access Hash Table - Open Addressing C Insertion Algorithm implementation"),
	deletion = arg_intn("d", "deletion","<your key value> (integer)", 0, 1, "Access Hash Table - Open Addressing C Deletion Algorithm implementation"),
	showTable = arg_litn("p", "show-table", 0, 1, "Display Hash table"),

	end = arg_end(20)
	};

	int exitcode = 0;
	char progname[] = "hashTableOpenAddressing.exe";

	int nerrors;
	nerrors = arg_parse(argc, argv, argtable);

	if (argc == 1) {
		printf("Please choose one of the available commands\n\n");
		arg_print_glossary(stdout, argtable, "  %-25s %s\n");
		exitcode = 0;
		goto exit;
	}

	//If the parser returned any errors then display them and exit 
	if (nerrors > 0)
	{
		// Display the error details contained in the arg_end struct.
		arg_print_errors(stderr, end, progname);
		fprintf(stderr,"Try '%s --help' for more information.\n", progname);
		exitcode = 1;
		goto exit;
	}


	if (help->count > 0)
	{
		printf("Usage: %s", progname);
		arg_print_syntax(stdout, argtable, "\n\n");
		arg_print_glossary(stdout, argtable, "  %-25s %s\n");
		exitcode = 0;
		goto exit;
	}

	if (showCompute->count > 0)
	{
		global_show_compute = !global_show_compute;
	}


	if (logLevelValue->count > 0)
	{
		setLogLevel(*(logLevelValue)->sval, global_log_level);
	}

	//set project common value (random seed , and number of element in array
	if (setElementCount->count > 0)
	{
		tableCommonValue.elementCount = *(setElementCount)->ival;
	}

	if (setRandSeedValue->count > 0)
	{
		tableCommonValue.seedRand = *(setRandSeedValue)->ival;
	}

	srand(tableCommonValue.seedRand);
	showCommonProjectValue(tableCommonValue, global_log_level);
	createTable(&table, tableCommonValue);

	if (search->count > 0)
	{
		implementedSearch(table, *(search)->ival);
	}

	if (insertion->count > 0)
	{
		std::string key = "";
		std::string value = "";
		int checkCommaInString = 0;
		char* valToInsert = (char*)malloc(strlen(*(insertion)->sval) + 1 * sizeof(char));
		if (valToInsert) strcpy(valToInsert, *(insertion)->sval);
		else
		{
			fprintf(stderr, "Can't allocate your array argument for insertion\n");
			exit(0);
		}

		std::string str = "";
		for (int i = 0; i < strlen(valToInsert); ++i)
		{
			str += valToInsert[i];
			if (valToInsert[i] == ',')
			{
				++checkCommaInString;
				if (checkCommaInString == 1)
				{
					key = str;
					key[key.size() - 1] = '\0';
					str = "";
				}
				if (checkCommaInString == 2)
				{
					value = str;
					value[value.size() - 1] = '\0';
					break;
				}
			}
		}

		if (checkCommaInString >= 1)
		{
			if (checkCommaInString == 1) value = str;

			int keyInteger = atoi(key.c_str());
			int valueInteger = atoi(value.c_str());

			implementedInsertion(&table, keyInteger, valueInteger, tableCommonValue.elementCount + 1);
		}
		else
			printf("insert a valid form --> key,value\n");

		free(valToInsert);
	}

	if (deletion->count > 0)
	{
		implementedDeletion(&table, *(deletion)->ival, tableCommonValue.elementCount);
	}

	if (checkResultValidation->count > 0)
	{
		// test on keys
		int* arrayFoTesting = (int*)malloc(table->count * sizeof(int));
		for (int i = 0; i < table->count; ++i)
		{
			struct hash_item* current = (struct hash_item*)table[i].data;
			if (current != NULL)
				arrayFoTesting[i] = current->key;
		}
		
		int index = rand() % (table->count - 1 );

		int pickUpRandomKey = arrayFoTesting[index];

		uint64_t cc = 0; 
		if (!searchKey(table, pickUpRandomKey, cc))
		{
			fprintf(stderr, "can't find your key, error in search algorithm\n");
			free(arrayFoTesting);
			destroyTable(table);
			goto exit; 
		}

		cc = 0; 
		deleteKey(&table, pickUpRandomKey,cc, tableCommonValue.elementCount);

		if (searchKey(table, pickUpRandomKey, cc))
		{
			fprintf(stderr, "found your key, error in delete algorithm\n");
			free(arrayFoTesting);
			destroyTable(table);
			goto exit;

		}

		int* arrayForCheckingElementAfterRunningAlgorithm = (int*)malloc(table->count * sizeof(int));
		
		for (int i = 0; i < table->count; ++i)
		{
			struct hash_item* current = (struct hash_item*)table[i].data;
			if (current != NULL)
				arrayForCheckingElementAfterRunningAlgorithm[i] = current->key;
		}

		for (int i = 0; i < table->count; ++i)
		{
			if ( i < index )
			{
				if (arrayForCheckingElementAfterRunningAlgorithm[i] != arrayFoTesting[i])
				{
					fprintf(stderr, "wrong in key values after deletion, error in delete algorithm\n");
					free(arrayFoTesting);
					free(arrayForCheckingElementAfterRunningAlgorithm);
					destroyTable(table);
					goto exit;
				}
			}
			else
			{
				if (arrayForCheckingElementAfterRunningAlgorithm[i] != arrayFoTesting[i + 1])
				{
					fprintf(stderr, "wrong in key values after deletion, error in delete algorithm\n");
					free(arrayFoTesting);
					free(arrayForCheckingElementAfterRunningAlgorithm);
					destroyTable(table);
					goto exit;
				}
			}
		}

		cc = 0; 
		
		//we insert a random value for the key ( reminder we're checking the keys here
		insertKey(&table, pickUpRandomKey,1,cc, tableCommonValue.elementCount);
		
		if (!searchKey(table, pickUpRandomKey, cc))
		{
			fprintf(stderr, "can't find your key after insertion, error in insertion algorithm\n");
			free(arrayFoTesting);				
			free(arrayForCheckingElementAfterRunningAlgorithm);
			destroyTable(table);
			goto exit;
		}

		if(pickUpRandomKey != table[--table->count].data->key)
		{
			fprintf(stderr, "key are not in the same in hash table, error in insertion algorithm\n");
			free(arrayFoTesting);
			free(arrayForCheckingElementAfterRunningAlgorithm);
			destroyTable(table);
			goto exit;
		}
		
		printf("Testing Functions Success\n");
		free(arrayFoTesting);
		free(arrayForCheckingElementAfterRunningAlgorithm);

	}

	if (showTable->count > 0)
	{
		printTable(table);
	}

	arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
	destroyTable(table);
	
	return exitcode;


exit:
	/* deallocate each non-null entry in argtable[] */
	arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
	return exitcode;

}

