#include "../include/hash_table_insertion_c.h"
#include <hash_table_common_c.h>

void insertKey(struct hash_table** table, int key, int value, uint64_t& computeVal, int capacity)
{
	int index = hashFunction(key, capacity);
	int i = index;

	struct hash_item* newItem = (struct hash_item*)malloc(sizeof(struct hash_item));

	if (!newItem)
	{
		fprintf(stderr, "Can't allocate your hash item for insertion\n");
		exit(0);
	}

	newItem->key = key;
	newItem->value = value;


	//double hashing
	while ((*table)[i].flag == 1)
	{
		++computeVal;

		if ((*table)[i].data->key == key)
		{
			if (global_log_level <= logLevel::debug)
				printf("key already exist , we update the value\n");
			
			(*table)[i].data->value = value;
			free(newItem);
			return;
		}
		
		i = (i + 1) % capacity;
		if (i == index)
		{
			if (global_log_level <= logLevel::debug)
				printf("hash table is full cannot insert any more\n");
			
			free(newItem);
			return;
		}
	}

	if ((*table)[i].flag != 2)
	{
		++(*table)->count;
		*table = (struct hash_table*)realloc(*table ,((*table)->count * sizeof(struct hash_table)));
		(*table)[i].flag = 1;
		(*table)[i].data = newItem;

		//printf("Insertion Success\n");
		return; 
	}
	(*table)[i].flag = 1;
	(*table)[i].data = newItem;

	++(*table)->count;
	//printf("Insertion Success\n");

}

void implementedInsertion(struct hash_table** table, int key, int value,int capacity)
{
	size_t timeElapsed = 0;
	uint64_t computeVal = 0;

	getTimeLapsComputeVariables(timeElapsed, insertKey, table, key, value, computeVal, capacity);
	showComputeElapsedTime(computeVal, timeElapsed, "Hash Table Insertion", global_show_compute, global_log_level);

}