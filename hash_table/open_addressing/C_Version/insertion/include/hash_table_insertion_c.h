#pragma once

#include <common_project.h>

#ifdef TEST_FOUND 
#include "CppUTest/MemoryLeakDetectorMallocMacros.h"
#endif

struct hash_table;

void insertKey(struct hash_table** table, int key, int value, uint64_t& computeVal, int capacity);

void implementedInsertion(struct hash_table** table, int key, int value, int capacity);