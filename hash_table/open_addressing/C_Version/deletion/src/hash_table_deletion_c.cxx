#include "../include/hash_table_deletion_c.h"


void deleteKey(struct hash_table** table, int key, uint64_t& computeVal,int capacity)
{
	int index = hashFunction(key, capacity);
	int i = index;
	++computeVal;

	while ((*table)[i].flag != 0)
	{
		++computeVal;

		if ((*table)[i].flag == 1 && (*table)[i].data->key == key)
		{

			int fromIndexOfKeyToTheEndOfTable = i; 
			while (fromIndexOfKeyToTheEndOfTable < (*table)->count - 1)
			{
				memmove((*table)[fromIndexOfKeyToTheEndOfTable].data, (*table)[fromIndexOfKeyToTheEndOfTable + 1].data, (*table)->count);
				++fromIndexOfKeyToTheEndOfTable;
			}

			free((*table)[(*table)->count - 1].data);
			(*table)[(*table)->count - 1].data = NULL; 
			
			--(*table)->count;
			*table = (struct hash_table*)realloc(*table ,((*table)->count * sizeof(struct hash_table)) ) ;

			if(global_log_level <= logLevel::debug)
				printf("\n Key: %i was successfully removed\n", key);
		
			return;
		}

		i = (i + 1) % capacity;

		if (i == index)
		{
			break;
		}
	}

	if (global_log_level <= logLevel::debug)
		printf("Key: %i not found\n", key);

}

void implementedDeletion(struct hash_table** table, int key, int capacity)
{
	size_t timeElapsed = 0;
	uint64_t computeVal = 0;

	getTimeLapsComputeVariables(timeElapsed, deleteKey, table, key, computeVal, capacity);
	showComputeElapsedTime(computeVal, timeElapsed, "Hash Table Open Addressing Deletion", global_show_compute, global_log_level);

	if (global_log_level == logLevel::debug)
	{
		printf("remaining key/value after deletion\n");
		printTable(*table);
	}
}
