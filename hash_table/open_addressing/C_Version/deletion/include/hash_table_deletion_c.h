#pragma once

#include <common_project.h>
#include <hash_table_common_c.h>

#ifdef TEST_FOUND 
#include "CppUTest/MemoryLeakDetectorMallocMacros.h"
#endif

void deleteKey(struct hash_table** table, int key, uint64_t&computeVal,int capacity);

void implementedDeletion(struct hash_table** table, int key, int capacity);
