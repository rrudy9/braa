file(GLOB hash_table_open_addressing_common_c
	.cxx .hxx 
	src/*.cxx 
	include/*.h)

add_library(hash_table_open_addressing_common_c_lib ${hash_table_open_addressing_common_c})


set(linkingLibrary 
	${CPPUTEST_LDFLAGS}  
	common_project_lib
	hash_table_open_addressing_insertion_c_lib
	)

set(linkingDirectories 
	"${CPPUTEST_INCLUDE_DIRS}"
	"${PROJECT_SOURCE_DIR}/common_project/include"
	"${PROJECT_SOURCE_DIR}/hash_table/Open_Addressing/C_Version/insertion/include" 
	)

	
if (CPPUTEST_LIBRARIES)
	add_compile_definitions(TEST_FOUND="test")
endif(CPPUTEST_LIBRARIES)




target_link_libraries(hash_table_open_addressing_common_c_lib PRIVATE ${linkingLibrary})


target_include_directories(hash_table_open_addressing_common_c_lib PRIVATE ${linkingDirectories})

set_property(TARGET hash_table_open_addressing_common_c_lib PROPERTY FOLDER "hash_table/Open_Addressing/C_Version/common")
