#include "../include/hash_table_common_c.h"
#include <hash_table_insertion_c.h>


void initializeTable(struct hash_table** table, struct Common_Project_Values projectVal)
{
	*table = (struct hash_table*)malloc(projectVal.elementCount * sizeof(struct hash_table));

	if (*table == NULL)
	{
		fprintf(stderr, "Can't allocate your hash table");
		exit(0);
	}

	(*table)->count = 0;

	for (int i = 0; i < projectVal.elementCount; ++i)
	{
		(*table)[i].flag = 2;
		(*table)[i].data = NULL;
	}
}

void createTable(struct hash_table** table, struct Common_Project_Values projectVal)
{
	initializeTable(table, projectVal);

	for (int i = 0; i < projectVal.elementCount; ++i)
	{
		uint64_t cc = 0; 
		int randKey = rand() * 10001; 
		int randValue = rand() * 10001; 

		insertKey(table,randKey, randValue, cc,projectVal.elementCount);
	}
}

unsigned long hashFunction(int key, int elementCount)
{
	return (key % elementCount);
}


void destroyTable(struct hash_table* table)
{
	for (int i = 0; i < table->count; ++i)
	{
		struct hash_item* current = (struct hash_item*)table[i].data;
		if (current != NULL)
			free(current);

	}
}


void printTable(struct hash_table* table)
{
	for (int i = 0; i < table->count; ++i)
	{
		struct hash_item* current = (struct hash_item*)table[i].data;
		int index = i; 
		if (current != NULL)
			printf("Key: %i, Value: %i, at index : %i\n", current->key, current->value, i);
	}
}


bool getTimeLapsComputeVariables(size_t& elapsedTime, bool (*f)(struct hash_table*, int, uint64_t&), struct hash_table* table, int key, uint64_t& computeVal)
{
	bool returnVal = false; 
	std::chrono::steady_clock::time_point t1 = std::chrono::high_resolution_clock::now();
	returnVal = f(table, key, computeVal);
	std::chrono::steady_clock::time_point t2 = std::chrono::high_resolution_clock::now();

	elapsedTime = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();
	return returnVal; 
}

void getTimeLapsComputeVariables(size_t& elapsedTime, void (*f)(struct hash_table**, int, uint64_t&, int), struct hash_table** table, int key, uint64_t& computeVal, int capacity)
{
	std::chrono::steady_clock::time_point t1 = std::chrono::high_resolution_clock::now();
	f(table, key, computeVal, capacity);
	std::chrono::steady_clock::time_point t2 = std::chrono::high_resolution_clock::now();

	elapsedTime = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();

}

void getTimeLapsComputeVariables(size_t& elapsedTime, void (*f)(struct hash_table**, int, int, uint64_t&,int), struct hash_table** table, int key, int value, uint64_t& computeVal,int capacity)
{
	std::chrono::steady_clock::time_point t1 = std::chrono::high_resolution_clock::now();
	f(table, key,value, computeVal,capacity);
	std::chrono::steady_clock::time_point t2 = std::chrono::high_resolution_clock::now();

	elapsedTime = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();

}
