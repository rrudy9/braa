
#ifndef HASH_TABLE_COMMON_C
#define HASH_TABLE_COMMON_C

#include <stdlib.h>
#include <stdio.h>
#include <common_project.h>
#include <chrono>
#include <string>
#include <iostream>
#include <random>

#ifdef TEST_FOUND 
#include "CppUTest/MemoryLeakDetectorMallocMacros.h"
#endif

struct hash_item
{
	int key;
	int value;
};

struct hash_table
{
	int flag;
	int count;
	struct hash_item* data;
};



void initializeTable(struct hash_table** table, struct Common_Project_Values projectVal);

void createTable(struct hash_table** table, struct Common_Project_Values projectVal);

unsigned long hashFunction(int key, int elementCount);

void destroyTable(struct hash_table* table);

void printTable(struct hash_table* table);

bool getTimeLapsComputeVariables(size_t& elapsedTime, bool (*f)(struct hash_table*, int, uint64_t&), struct hash_table* table, int key, uint64_t& computeVal);
void getTimeLapsComputeVariables(size_t& elapsedTime, void (*f)(struct hash_table**, int, uint64_t&,int), struct hash_table** table, int key, uint64_t& computeVal, int capacity);
void getTimeLapsComputeVariables(size_t& elapsedTime, void (*f)(struct hash_table**, int,int, uint64_t&,int), struct hash_table** table, int key, int value, uint64_t& computeVal,int capacity);
#endif
