#include "CppUTest/TestHarness.h"
#include "CppUTest/MemoryLeakDetectorMallocMacros.h"

#include <common_project.h>
#include <hash_table_deletion_c.h>
#include <hash_table_common_c.h>
#include <hash_table_insertion_c.h>
#include <hash_table_search_c.h>



TEST_GROUP(TEST_HASH_TABLE_OPEN_ADDRESSING_C)
{
	struct Common_Project_Values tableCommonValue = { global_test_count_element, global_test_rand_seed };
	struct hash_table* table = NULL;

	void setup()
	{
		showCommonProjectValue(tableCommonValue, global_log_level);
		createTable(&table, tableCommonValue);
	}

	void teardown()
	{

		destroyTable(table);
	}
};




TEST(TEST_HASH_TABLE_OPEN_ADDRESSING_C, deletion)
{
	int valToDelete = rand() * 1001; 

	if (global_log_level <= logLevel::debug)
		printf("key to delete %i\n", valToDelete);

	implementedDeletion(&table, valToDelete, tableCommonValue.elementCount);

	// faire un test 

}

TEST(TEST_HASH_TABLE_OPEN_ADDRESSING_C, search)
{

	int valToSearch = rand() * 1001; 

	if (global_log_level <= logLevel::debug)
		printf("key to search %i\n", valToSearch);

	implementedSearch(table, valToSearch);

	FAIL("force failing for search on purpose");

}

TEST(TEST_HASH_TABLE_OPEN_ADDRESSING_C, insertion)
{


	int keyToAdd = rand() * 10012; 
	int valueToAdd = rand() * 1012;

	if (global_log_level <= logLevel::debug)
		printf("key, value to add ( %i , %i )\n", keyToAdd, valueToAdd);

	implementedInsertion(&table, keyToAdd, valueToAdd,tableCommonValue.elementCount);

	// faire un test
}

