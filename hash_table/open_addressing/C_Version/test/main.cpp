#include "CppUTest/CommandLineTestRunner.h"

#include <ctime>
#include <common_project.h>	
#include "argtable3.h"

bool global_show_compute = false;
logLevel global_log_level = logLevel::info;

int global_test_count_element = 100000;
int global_test_rand_seed = 30;

int main(int argc, char* argv[])
{
	struct arg_lit
		* help = NULL,
		* showCompute = NULL,
		* runAll = NULL;

	struct arg_str
		* chooseTest = NULL,
		* logLevelValue = NULL;

	struct arg_int
		* setElementCount = NULL,
		* setRandSeedValue = NULL;


	struct arg_end* end = NULL;

	void* argtable[] = {
		help = arg_litn(NULL, "help", 0, 1, "display this help for Hash Table - Open Addressing <C Test Version> and exit"),
		showCompute = arg_litn("c","show-compute",0, 1, "Show Compute"),
		logLevelValue = arg_strn("v", "verbose","<info,debug,error,fatal>", 0, 1, "Set Log Level (info, debug, error, fatal) (Default : info)"),
		setElementCount = arg_intn("h", "hash-table-count","<n>" , 0, 1, "Set Test Hash Table - Open Addressing Number of element (Default Value: 100k), Doesn't work when n = 0 "),
		setRandSeedValue = arg_intn("s", "rand-seed","<n>", 0, 1, "Set Test value for Seed Random (Default Value: 30)"),

		chooseTest = arg_strn("n", NULL,"<insertion,search, deletion>", 0, 1, "Choose a test to run (-n >> -r)"),
		runAll = arg_litn("r", "run-all", 0, 1, "Run All Tests (-n >> -r) "),

		end = arg_end(20)
	};

	int exitcode = 0;
	char progname[] = "testHashTableOpenAddressing.exe";

	int nerrors;
	nerrors = arg_parse(argc, argv, argtable);


	if (argc == 1) {
		printf("Please choose one of the available commands\n\n");
		arg_print_glossary(stdout, argtable, "  %-25s %s\n");
		exitcode = 0;
		goto exit;
	}

	if (help->count > 0)
	{
		printf("Usage: %s", progname);
		arg_print_syntax(stdout, argtable, "\n\n");
		arg_print_glossary(stdout, argtable, "  %-25s %s\n");
		exitcode = 0;
		goto exit;
	}

	if (nerrors > 0)
	{
		// Display the error details contained in the arg_end struct.
		arg_print_errors(stderr, end, progname);
		fprintf(stderr,"Try '%s --help' for more information.\n", progname);
		exitcode = 1;
		goto exit;
	}

	if (showCompute->count > 0)
	{
		global_show_compute = !global_show_compute;
	}

	if (logLevelValue->count > 0)
	{
		setLogLevel(*(logLevelValue)->sval, global_log_level);
	}

	if (setElementCount->count > 0)
	{
	//	if (*(setElementCount)->ival == 0) goto exit;

		global_test_count_element = *(setElementCount)->ival;
	}

	if (setRandSeedValue->count > 0)
	{
		global_test_rand_seed = *(setRandSeedValue)->ival;
	}

	if (chooseTest->count > 0)
	{
		const char* argVal[4] = { argv[0] , "-n", *(chooseTest)->sval, "-c" };

		char* newArgs[4] = { 0 };
		for (int i = 0; i < 4; i++)
		{
			newArgs[i] = (char*)malloc(sizeof(char) * strlen(argVal[i]));
			if (newArgs[i] == NULL)
			{
				fprintf(stderr, "can't allocate your arguments array");
				exitcode = 0;
				goto exit;
			}
			strcpy(newArgs[i], argVal[i]);
		}

		srand(global_test_rand_seed);
		showCommonProjectValueForTest(global_log_level, global_test_count_element, global_test_rand_seed);

		return CommandLineTestRunner::RunAllTests(4, newArgs);
	}

	if (runAll->count > 0)
	{
		const char* argVal[4] = { argv[0] , "-g", "TEST_HASH_TABLE_OPEN_ADDRESSING_C", "-c" };

		char* newArgs[4] = { 0 };
		for (int i = 0; i < 4; i++)
		{
			newArgs[i] = (char*)malloc(sizeof(char) * strlen(argVal[i]));
			if (newArgs[i] == NULL)
			{
				fprintf(stderr,"can't allocate your arguments array");
				exitcode = 0;
				goto exit;
			}
			strcpy(newArgs[i], argVal[i]);
		}

		srand(global_test_rand_seed);
		showCommonProjectValueForTest(global_log_level, global_test_count_element, global_test_rand_seed);

		return CommandLineTestRunner::RunAllTests(4, newArgs);
	}

	arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
	return exitcode;

exit:
	/* deallocate each non-null entry in argtable[] */
	arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
	return exitcode;
}
