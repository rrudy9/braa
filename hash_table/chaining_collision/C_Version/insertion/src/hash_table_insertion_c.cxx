#include "../include/hash_table_insertion_c.h"
#include <hash_table_common_c.h>



void insertKeyValue(struct hash_table* table, char* key, char* value, uint64_t& computeVal)
{

	int index = hashFunction(key, table->bunkerSize);

	struct hash_item* item = NULL; 
	createItem(&item, key, value);

	struct hash_item* currentItem = table->items[index];

	++computeVal;

	//printf("table count: %i / key: %s / index HashFunction: %i \n",table->count, key, index);


	if (currentItem == NULL)
	{
		if (table->count == table->bunkerSize)
		{
			if (global_log_level <= logLevel::error)
				printf("Insert Error: Hash Table is full\n");

			return;
		}


		table->items[index] = item;
		++table->count;
	
	}
	else
	{
		if (global_log_level == logLevel::debug )
			printf("already existent\n");

		//case 1: we need to udpate the value
		if (strcmp(currentItem->key, key) == 0)
		{
			strcpy(table->items[index]->value, value);

			if (global_log_level <= logLevel::debug)
				printf("Value for key %s was updated\n", key);
			return;
		}
		else
		{
			//case 2 : collision
			handleCollision(table, index, item, computeVal);
			return;
		}

	}

}


struct linked_list* insertLinkedList(struct linked_list* list, struct hash_item* item, uint64_t& computeVal)
{
	++computeVal;
	if (!list)
	{
		struct linked_list* head = allocateList();
		head->item = item;
		head->next = NULL;
		list = head;
		return list;
	}
	else if (list->next == NULL)
	{
		struct linked_list* node = allocateList();
		node->item = item;
		node->next = NULL;
		list->next = node;
		return list;
	}

	struct linked_list* tmp = list;
	while (tmp->next->next)
	{
		++computeVal;
		tmp = tmp->next;
	}

	struct linked_list* node = allocateList();
	node->item = item;
	node->next = NULL;
	tmp->next = node;

	return list;

}




void handleCollision(struct hash_table* table, unsigned long index, struct hash_item* item, uint64_t& computeVal)
{

	struct linked_list* head = table->overflowBuckets[index];
	++computeVal;
	
	if (head == NULL)
	{
		head = allocateList();
		head->item = item;
		head->next = NULL; 
		table->overflowBuckets[index] = head;
		
		return;
	}
	else
	{

		struct linked_list* newNode = allocateList();
		newNode->item = item; 
		newNode->next = NULL;

		while (head->next != NULL)
		{
			if (strcmp(head->item->key, item->key) == 0)
			{
				strcpy(head->item->value, item->value);

				if (global_log_level <= logLevel::debug)
					printf("Value for key %s was updated\n", item->key);
			
				return;
			}

			head = head->next;
		}

		head->next = newNode;

		//table->overflowBuckets[index] = insertLinkedList(head, item, computeVal);
		return;
	}
}


void implementedInsertion (struct hash_table* table, char* key, char* value)
{
	size_t timeElapsed = 0;
	uint64_t computeVal = 0;

	getTimeLapsComputeVariables(timeElapsed, insertKeyValue, table, key, value, computeVal);
	showComputeElapsedTime(computeVal, timeElapsed, "Hash Table Insertion",global_show_compute, global_log_level);

	if (global_log_level == logLevel::debug)
	{
		printf("table after insertion\n");
		printTable(table);
	}

}