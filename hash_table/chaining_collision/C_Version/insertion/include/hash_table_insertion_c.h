#pragma once

#include <common_project.h>

#ifdef TEST_FOUND 
#include "CppUTest/MemoryLeakDetectorMallocMacros.h"
#endif


struct hash_table;
struct linked_list;
struct hash_item;

void insertKeyValue(struct hash_table* table, char* key, char* value, uint64_t& computeVal);

struct linked_list* insertLinkedList(struct linked_list* list, struct hash_item* item, uint64_t& computeVal);

void handleCollision(struct hash_table* table, unsigned long index, struct hash_item* item, uint64_t& computeVal);

void implementedInsertion(struct hash_table* table, char* key, char* value);