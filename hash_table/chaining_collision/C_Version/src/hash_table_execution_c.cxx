#include <stdlib.h>
#include <stdio.h>
#include <common_project.h>
#include <hash_table_deletion_c.h>
#include <hash_table_common_c.h>
#include <hash_table_insertion_c.h>
#include <hash_table_search_c.h>
#include <argtable3.h>


#ifdef TEST_FOUND 
#include "CppUTest/MemoryLeakDetectorMallocMacros.h"
#endif

bool global_show_compute = false;
logLevel global_log_level = logLevel::info;

void copyKeyElementInVectorFromHashMapForTesting(struct hash_table* table, std::vector<char*> &vectorFoTesting);
void freeAllocatedMemoryInVector(std::vector<char*>& vectorFoTesting);

int main(int argc, char* argv[])
{
	struct arg_lit
		* help = NULL,
		* showCompute = NULL,
		* checkResultValidation = NULL,
		* showTable = NULL;

	struct arg_str
		* logLevelValue = NULL,
		* search = NULL, 
		* insertion = NULL,
		* deletion = NULL;


	struct arg_int
		* setElementCount = NULL,
		* setRandSeedValue = NULL;

	struct arg_end* end = NULL;

	struct Common_Project_Values tableCommonValue = { 100000 , 30 };
	struct hash_table* table = NULL;


	void* argtable[] = {
		help = arg_litn(NULL, "help", 0, 1, "display this help for Hash Table - Chaining Collision <C Version> and exit"),
		showCompute = arg_litn("c","show-compute",0, 1, "Show Compute"),
		logLevelValue = arg_strn("v", "verbose","<info,debug,error,fatal>", 0, 1, "Set Log Level (info, debug, error, fatal) (Default : info)"),
		setElementCount = arg_intn("h", "hash-table-count","<n>" , 0, 1, "Set Hash Table Number of element (Default Value: 100k)"),
		setRandSeedValue = arg_intn("r", "rand-seed","<n>", 0, 1, "Set value for Seed Random (Default Value: 30)"),

		checkResultValidation = arg_litn("t","run-test" ,0, 1, "Run Test for implemented algorithms"),
		search = arg_strn("s", "search","<your key value> (string)", 0, 1, "Access Hash Table - Chaining Collision C Search Algorithm implementation"),
		insertion = arg_strn("i", "insertion", "<key,value> (string)" ,0, 1, "Access Hash Table - Chaining Collision C Insertion Algorithm implementation"),
		deletion = arg_strn("d", "deletion","<your key value> (string)", 0, 1, "Access Hash Table - Chaining Collision C Deletion Algorithm implementation"),
		showTable = arg_litn("p", "show-table", 0, 1, "Display Hash table"),

		end = arg_end(20)
	};

	int exitcode = 0;
	char progname[] = "hashTableChainingCollision.exe";

	int nerrors;
	nerrors = arg_parse(argc, argv, argtable);

	if (argc == 1) {
		printf("Please choose one of the available commands\n\n");
		arg_print_glossary(stdout, argtable, "  %-25s %s\n");
		exitcode = 0;
		goto exit;
	}

	//If the parser returned any errors then display them and exit 
	if (nerrors > 0)
	{
		// Display the error details contained in the arg_end struct.
		arg_print_errors(stderr, end, progname);
		fprintf(stderr,"Try '%s --help' for more information.\n", progname);
		exitcode = 1;
		goto exit;
	}


	if (help->count > 0)
	{
		printf("Usage: %s", progname);
		arg_print_syntax(stdout, argtable, "\n\n");
		arg_print_glossary(stdout, argtable, "  %-25s %s\n");
		exitcode = 0;
		goto exit;
	}


	if (showCompute->count > 0)
	{
		global_show_compute = !global_show_compute;
	}


	if (logLevelValue->count > 0)
	{
		setLogLevel(*(logLevelValue)->sval, global_log_level);
	}

	//set project common value (random seed , and number of element in array
	if (setElementCount->count > 0)
	{
		tableCommonValue.elementCount = *(setElementCount)->ival;
	}

	if (setRandSeedValue->count > 0)
	{
		tableCommonValue.seedRand = *(setRandSeedValue)->ival;
	}

	srand(tableCommonValue.seedRand);
	showCommonProjectValue(tableCommonValue, global_log_level);
	createTable(&table, tableCommonValue);

	if (search->count > 0)
	{

		char* valToSearch = (char*)malloc(strlen(*(search)->sval) + 1 * sizeof(char));
		if (valToSearch) strcpy(valToSearch, *(search)->sval);
		else
		{
			fprintf(stderr, "Can't allocate your value from arguments for search\n");
			exit(0);
		}

		implementedSearch(table, valToSearch);
		free(valToSearch);
	}

	if (insertion->count > 0)
	{
		std::string key = "";
		std::string value = "";
		int checkCommaInString = 0; 
		// we can use a string if we dont want to allocate memory
		char* valToInsert = (char*)malloc(strlen(*(insertion)->sval) + 1 * sizeof(char));
		if (valToInsert) strcpy(valToInsert, *(insertion)->sval);
		else
		{
			fprintf(stderr, "Can't allocate your value from arguments for insertion\n");
			exit(0);
		}

		std::string str = "";
		for (int i = 0; i < strlen(valToInsert); ++i)
		{
			str += valToInsert[i];
			if (valToInsert[i] == ',')
			{
				++checkCommaInString; 
				if (checkCommaInString == 1)
				{
					key = str;
					key[key.size() - 1] = '\0';
					str = "";
				}
				if (checkCommaInString == 2)
				{
					value = str;
					value[value.size() - 1] = '\0';
					break;
				}
			}
		}

		if (checkCommaInString >= 1)
		{
			if (checkCommaInString == 1) value = str;

			char* keyInChar = (char*)malloc(strlen(key.c_str()) + 1 *sizeof(char));
			if (keyInChar) strcpy(keyInChar, key.c_str());
			else
			{
				fprintf(stderr, "Can't allocate your value to copy key\n");
				exit(0);
			}

			char* valueInChar = (char*)malloc(strlen(value.c_str()) + 1 * sizeof(char));
			if (valueInChar) strcpy(valueInChar, value.c_str());
			else
			{
				fprintf(stderr, "Can't allocate your value to copy value\n");
				exit(0);
			}
			implementedInsertion(table, keyInChar, valueInChar);
			free(keyInChar);
			free(valueInChar);
		}
		else
			printf("insert a valid form --> key,value\n");

		free(valToInsert);
	}

	if (deletion->count > 0)
	{
		char* valToDelete = (char*)malloc(strlen(*(deletion)->sval) + 1 * sizeof(char));
		if (valToDelete) strcpy(valToDelete, *(deletion)->sval);

		implementedDeletion(table, valToDelete);
		free(valToDelete);
	}

	if (checkResultValidation->count > 0)
	{
		std::vector<char*> keyMapForTesting = {};

		//we can check if the values are the same too , but lets stick to the keys (more interesting for the collision...)

		copyKeyElementInVectorFromHashMapForTesting(table, keyMapForTesting);

		char* pickUpRandomKey = keyMapForTesting[rand() % keyMapForTesting.size()];

		size_t indexOfRandomVal = std::distance(keyMapForTesting.begin(), std::find(keyMapForTesting.begin(), keyMapForTesting.end(), pickUpRandomKey));

		uint64_t cc = 0; 
	
		deleteKey(table, pickUpRandomKey, cc);

		// check if key still exist in the table and that no other value was delete
		std::vector<char*> elementAfterDelete = {  };
		copyKeyElementInVectorFromHashMapForTesting(table, elementAfterDelete);

		if (elementAfterDelete.size() == keyMapForTesting.size())
		{
			fprintf(stderr, "Nothing was deleted, Error in code \n");
			freeAllocatedMemoryInVector(keyMapForTesting);
			freeAllocatedMemoryInVector(elementAfterDelete);
			destroyTable(table);
			goto exit;
		}
			
		//i
		for (int i = 0; i < elementAfterDelete.size() ; ++i)
		{
			if (i < indexOfRandomVal)
			{
				if (strcmp(keyMapForTesting.at(i), elementAfterDelete.at(i)) !=0)
				{
					fprintf(stderr, "Values are not same , Incorrect Deletion");
					freeAllocatedMemoryInVector(keyMapForTesting);
					freeAllocatedMemoryInVector(elementAfterDelete);
					destroyTable(table);
					goto exit;
				}
			}
			else
			{
				if (strcmp(keyMapForTesting.at(i + 1), elementAfterDelete.at(i)) != 0)
				{
					fprintf(stderr, "Values are not same , Incorrect Deletion");
					freeAllocatedMemoryInVector(keyMapForTesting);
					freeAllocatedMemoryInVector(elementAfterDelete);
					destroyTable(table);
					goto exit;
				}
			}
		}
		
		// same as search , if it was deleted successfully than search gonna be NULL also (no need to check it)
		printf("Deletion was successfully implemented\n");
		
		cc = 0; 
		insertKeyValue(table, pickUpRandomKey, "random value", cc);

		std::vector<char*> elementAfterInsertion = {  };
		copyKeyElementInVectorFromHashMapForTesting(table, elementAfterInsertion);

		int checkIfValueExist = 0;
		// some time it will fail but still true (if we replace an item in the array of item by
		// the head of the overflow bucket (value in table will be shifted by one or two value ..)

		// or find another way to implement this .. 
		for (int i = 0; i < elementAfterInsertion.size(); ++i)
		{
		
			if ( strcmp(keyMapForTesting.at(i), elementAfterInsertion.at(i)) != 0)
			{
				fprintf(stderr, "Values are not same , Incorrect Insertion");
				freeAllocatedMemoryInVector(keyMapForTesting);
				freeAllocatedMemoryInVector(elementAfterDelete);
				freeAllocatedMemoryInVector(elementAfterInsertion);
				destroyTable(table);
				goto exit;
			}
		}
		printf("Insertion was successfully implemented\n");

		freeAllocatedMemoryInVector(keyMapForTesting);
		freeAllocatedMemoryInVector(elementAfterDelete);
		freeAllocatedMemoryInVector(elementAfterInsertion);

	}


	if (showTable->count > 0)
	{
		printTable(table);
	}

	arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
	destroyTable(table);
	
	return exitcode;


exit:
	/* deallocate each non-null entry in argtable[] */
	arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
	return exitcode;

}


void copyKeyElementInVectorFromHashMapForTesting(struct hash_table* table, std::vector<char*>& vectorFoTesting)
{

	for (int i = 0; i < table->bunkerSize; ++i)
	{
		if (table->items[i])
		{
			char* s = (char*)malloc(strlen(table->items[i]->key) + 1 * sizeof(char));
			strcpy(s, table->items[i]->key);

			vectorFoTesting.push_back(s);
		}

		if (table->overflowBuckets[i])
		{
			struct linked_list* listChain = table->overflowBuckets[i];
			while (listChain)
			{
				char* s = (char*)malloc(strlen(listChain->item->key) + 1 * sizeof(char));
				strcpy(s, listChain->item->key);

				vectorFoTesting.push_back(s);
				listChain = listChain->next;
			}
		}
	}
}

void freeAllocatedMemoryInVector(std::vector<char*>& vectorFoTesting)
{
	for (std::vector<char*>::iterator it = vectorFoTesting.begin(); it != vectorFoTesting.end(); ++it)
	{
		free(*it);
	}
}