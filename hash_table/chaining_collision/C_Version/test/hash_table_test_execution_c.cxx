#include "CppUTest/TestHarness.h"
#include "CppUTest/MemoryLeakDetectorMallocMacros.h"

#include <common_project.h>
#include <hash_table_deletion_c.h>
#include <hash_table_common_c.h>
#include <hash_table_insertion_c.h>
#include <hash_table_search_c.h>




TEST_GROUP(TEST_HASH_TABLE_CHAIN_COLLISION_C)
{
	struct Common_Project_Values tableCommonValue = { global_test_count_element, global_test_rand_seed };
	struct hash_table* table = NULL;

	void setup()
	{
		showCommonProjectValue(tableCommonValue, global_log_level);
		createTable(&table, tableCommonValue);
	}

	void teardown()
	{
		
		destroyTable(table);
	}
};




TEST(TEST_HASH_TABLE_CHAIN_COLLISION_C, deletion)
{
	int randVal = rand() % 100 + 1;

	std::string valToSearch = random_string(randVal);
	char* writableKey = (char*)malloc(valToSearch.size() + 1 * sizeof(char));
	std::copy(valToSearch.begin(), valToSearch.end(), writableKey);
	writableKey[valToSearch.size()] = '\0';

	if (global_log_level <= logLevel::debug)
		printf("key to delete %s\n", writableKey);

	implementedDeletion(table, writableKey);

	free(writableKey);


	// faire un test 

}

TEST(TEST_HASH_TABLE_CHAIN_COLLISION_C, search)
{
	int randVal = rand() % 100 + 1;

	std::string valToSearch = random_string(randVal);
	char* writableKey = (char*)malloc(valToSearch.size() + 1 * sizeof(char));
	std::copy(valToSearch.begin(), valToSearch.end(), writableKey);
	writableKey[valToSearch.size()] = '\0';

	if (global_log_level <= logLevel::debug)
		printf("key to search %s\n", writableKey);

	implementedSearch(table, writableKey);

	free(writableKey);

	FAIL("force failing for search on purpose");
}

TEST(TEST_HASH_TABLE_CHAIN_COLLISION_C, insertion)
{
	
	int randVal = rand() % 100 + 1;

	std::string key = random_string(randVal);
	std::string value = random_string(randVal);
	char* writableKey = (char*)malloc(key.size() + 1 * sizeof(char));
	char* writableValue = (char*)malloc(key.size() + 1 * sizeof(char));

	std::copy(key.begin(), key.end(), writableKey);
	writableKey[key.size()] = '\0';

	std::copy(value.begin(), value.end(), writableValue);
	writableValue[value.size()] = '\0';

	implementedInsertion(table, writableKey, writableValue);

	// faire un test
}

