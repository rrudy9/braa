#ifndef HASH_TABLE_COMMON_H
#define HASH_TABLE_COMMON_H

#include <stdlib.h>
#include <stdio.h>
#include <common_project.h>
#include <chrono>
#include <string>
#include <iostream>
#include <random>

#ifdef TEST_FOUND 
#include "CppUTest/MemoryLeakDetectorMallocMacros.h"
#endif

struct hash_item
{
	char* key;
	char* value;

};

struct hash_table
{
	struct hash_item** items;
	struct linked_list** overflowBuckets;
	int bunkerSize; // size
	int count; // number of elements
};

struct linked_list
{
	struct hash_item* item;
	struct linked_list* next;
};


unsigned long hashFunction(char* str, int elementCount);

void createItem(struct hash_item** item ,char* key, char* value);

void initializeTable(struct hash_table** table, struct Common_Project_Values projectVal);

void createTable(struct hash_table** table, struct Common_Project_Values projectVal);

void freeItem(struct hash_item* item);

void destroyTable(struct hash_table* table);

void printTable(struct hash_table* table);

void createOverflowBuckets(struct hash_table* table, struct linked_list*** buckets);

void freeLinkedList(struct linked_list* list);

void freeList(struct linked_list* list);

void freeOverflowBuckets(struct hash_table* table);

struct linked_list* allocateList();


void getTimeLapsComputeVariables(size_t& elapsedTime, void (*f)(struct hash_table*, char*, uint64_t&), struct hash_table* table, char* key, uint64_t& computeVal);
void getTimeLapsComputeVariables(size_t& elapsedTime, void (*f)(struct hash_table*, char*,char*, uint64_t&), struct hash_table* table, char* key,char*value, uint64_t& computeVal);

std::string random_string(int length);

#endif
