#include "../include/hash_table_common_c.h"
#include <hash_table_insertion_c.h>

//a simple hash function for test
//will use another complicated hash in the futur

unsigned long hashFunction(char* str, int elementCount)
{
	unsigned long i = 0;
	for (int j = 0; str[j]; ++j)
		i += str[j];
	return i % elementCount ;
}


void createItem(struct hash_item** item,  char* key, char* value)
{
	*item = (struct hash_item*)malloc(2 * sizeof(struct hash_item));

	if (*item == NULL)
	{
		fprintf(stderr,"can't allocate item in hash table");
		exit(0);
	}

	(*item)->key = (char*)malloc(strlen(key) + 1);
	(*item)->value = (char*)malloc(strlen(value) + 1);

	strcpy((*item)->key, key);
	strcpy((*item)->value, value);

}


void initializeTable(struct hash_table** table, struct Common_Project_Values projectVal)
{
	*table = (struct hash_table*)malloc(sizeof(struct hash_table));

	if (*table == NULL)
	{
		fprintf(stderr,"can't allocate table");
		exit(0);
	}

	(*table)->bunkerSize = projectVal.elementCount;
	(*table)->count = 0;
	(*table)->items = (struct hash_item**)malloc( (*table)->bunkerSize * sizeof(struct hash_item*));

	for (int i = 0; i < (*table)->bunkerSize; ++i)
		(*table)->items[i] = NULL;

	 createOverflowBuckets(*table,&(*table)->overflowBuckets);


}

void createTable(struct hash_table** table, struct Common_Project_Values projectVal)
{
	initializeTable(table, projectVal);

	for (int i = 0; i < projectVal.elementCount; ++i)
	{	
		uint64_t c = 0; 

		int randVal = rand() % 100 + 1;
		std::string key = random_string(randVal);
		std::string value = random_string(randVal);
		char* writableKey = (char*)malloc(key.size() + 1 * sizeof(char));
		if (!writableKey)
		{
			fprintf(stderr, "Can't allocate writable key\n");
			exit(0);
		}

		char* writableValue = (char*)malloc(key.size() + 1 * sizeof(char));

		if (!writableValue)
		{
			fprintf(stderr, "Can't allocate writable value \n");
			exit(0);
		}

		std::copy(key.begin(), key.end(), writableKey);
		writableKey[key.size()] = '\0';

		std::copy(value.begin(), value.end(), writableValue);
		writableValue[value.size()] = '\0';
	
		insertKeyValue(*table,writableKey, writableValue,c);
		free(writableKey);
		free(writableValue);
	}
		



}

void freeItem(struct hash_item* item)
{
	free(item->key);
	free(item->value);
	free(item);

}

void destroyTable(struct hash_table* table)
{
	for (int i = 0; i < table->bunkerSize; ++i)
	{
		struct hash_item* item = table->items[i];
		if (item != NULL)
			freeItem(item);
	}

	freeOverflowBuckets(table);
	free(table->items);
	free(table);

}


void printTable(struct hash_table* table)
{
	for (int i = 0; i < table->bunkerSize; ++i)
	{
		if (table->items[i])
			printf("Key: %s, Value: %s at index : %i\n", table->items[i]->key, table->items[i]->value, i);

		if (table->overflowBuckets[i])
		{
			struct linked_list* head = table->overflowBuckets[i];
			while (head)
			{
				printf("Key: %s, Value: %s in overflow bucket \n", head->item->key, head->item->value);
				head = head->next;
			}
		}
	}
}


void createOverflowBuckets(struct hash_table* table, struct linked_list*** buckets)
{
	*buckets = (struct linked_list**)malloc( (table->bunkerSize ) * sizeof(struct linked_list*));

	if (*buckets == NULL)
	{
		fprintf(stderr, "Can't allocate your list for overflow bucket");
		exit(0);
	}

	for (int i = 0; i < table->bunkerSize; ++i)
		(*buckets)[i] = NULL;

}

void freeOverflowBuckets(struct hash_table* table)
{
	struct linked_list** buckets = table->overflowBuckets;
	for (int i = 0; i < table->bunkerSize; ++i)
		freeLinkedList(buckets[i]);

	free(buckets);

}


struct linked_list* allocateList()
{
	struct linked_list* list = (struct linked_list*)malloc(sizeof(struct linked_list));
	if (list == NULL)
	{
		fprintf(stderr, "Can't allocate your memory list chaining");
	}

	return list;
}



void freeLinkedList(struct linked_list* list)
{
	struct linked_list* tmp = list;
	while (list)
	{
		tmp = list;
		list = list->next;
		free(tmp->item->key);
		free(tmp->item->value);
		free(tmp->item);
		free(tmp);

	}

}

void freeList(struct linked_list* list)
{
	if (list)
	{
		free(list->item->key);
		free(list->item->value);
		free(list->item);
		free(list);

	}
}



void getTimeLapsComputeVariables(size_t& elapsedTime, void (*f)(struct hash_table*, char*, uint64_t&), struct hash_table* table, char* key, uint64_t& computeVal)
{

	std::chrono::steady_clock::time_point t1 = std::chrono::high_resolution_clock::now();
	f(table, key, computeVal);
	std::chrono::steady_clock::time_point t2 = std::chrono::high_resolution_clock::now();

	elapsedTime = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();

}

void getTimeLapsComputeVariables(size_t& elapsedTime, void (*f)(struct hash_table*, char*, char*, uint64_t&), struct hash_table* table, char* key, char* value, uint64_t& computeVal)
{
	std::chrono::steady_clock::time_point t1 = std::chrono::high_resolution_clock::now();
	f(table, key,value, computeVal);
	std::chrono::steady_clock::time_point t2 = std::chrono::high_resolution_clock::now();

	elapsedTime = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();

}


std::string random_string(int length)
{
	const std::string CHARACTERS = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
	std::string random_string;

	for (std::size_t i = 0; i < length; ++i)
	{
		random_string += CHARACTERS[rand() % (sizeof(CHARACTERS) - 1)];
	}

	return random_string;
}