#include "../include/hash_table_search_c.h"

char* searchHash(struct hash_table* table, char* key, uint64_t& computeVal)
{
	int index = hashFunction(key,table->bunkerSize);
	struct hash_item* item = table->items[index];
	struct linked_list* head = table->overflowBuckets[index];

	++computeVal;

	while (item != NULL)
	{
		++computeVal;

		if (strcmp(item->key, key) == 0)
		{
			return item->value;
		}
		
		if (head == NULL)
		{
			return NULL;
		}
		item = head->item;
		head = head->next;
	}

	return NULL;

}

void searchKey(struct hash_table* table, char* key, uint64_t& computeVal)
{
	char* val = NULL;
	++computeVal;

	if ((val = searchHash(table, key, computeVal)) == NULL)
	{
		if (global_log_level <= logLevel::debug)
			printf("key: %s does not exist\n", key);
		return;
	}
	else
	{
		if (global_log_level <= logLevel::debug)
			printf("Your searched key:%s, Value:%s\n", key, val);
	}
}


void implementedSearch(struct hash_table* table, char* valToSearch)
{

	if (table->count != 0)
	{
		size_t timeElapsed = 0;
		uint64_t computeVal = 0;

		if (global_log_level == logLevel::debug )
		{
			printf("display hash table");
			printTable(table);
			printf("\n");

		}
		getTimeLapsComputeVariables(timeElapsed, searchKey, table, valToSearch, computeVal);
		showComputeElapsedTime(computeVal, timeElapsed, "Hash Table Selection", global_show_compute, global_log_level);
	}
	else
	{
		printf("Try To insert a value first, your table is empty!\n");
	}
}