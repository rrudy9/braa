#include "../include/hash_table_deletion_c.h"

void deleteKey(struct hash_table* table, char* key, uint64_t& computeVal)
{
	
	int index = hashFunction(key, table->bunkerSize);

	struct hash_item* item = table->items[index];
	struct linked_list* head = table->overflowBuckets[index];
	
	++computeVal;

	if (item == NULL)
	{
		if (global_log_level <= logLevel::debug)
			printf("Item does not exists\n");
	}
	else
	{

		if (head == NULL && strcmp(item->key, key) == 0)
		{
			// no collision chain, remove the item
			table->items[index] = NULL;
			freeItem(item);
			--table->count;

			if (global_log_level <= logLevel::debug)
				printf("Item removed from array of items\n");

			return;
		}
		else if (head != NULL)
		{

			if (strcmp(item->key, key) == 0)
			{

				// remove the item and set the head of the list as the new item
				freeItem(item);
				struct linked_list* node = head;
				head = head->next;
				node->next = NULL;
				createItem(&table->items[index],node->item->key, node->item->value);
				//freeLinkedList(node);
				freeList(node);
				table->overflowBuckets[index] = head;

				if (global_log_level <= logLevel::debug)
					printf("Item removed from array while setting it to the value of head in the list overflow bucket\n");
				return;
			}

			struct linked_list* nodeToBeDeleted = head;
			struct linked_list* prev = NULL;

			if (strcmp(head->item->key, key) == 0)
			{
				freeList(head);
				head = NULL; 
				table->overflowBuckets[index] = head;
				return;
			}

			while (nodeToBeDeleted != NULL && strcmp(nodeToBeDeleted->item->key, key) != 0)
			{
				prev = nodeToBeDeleted;
				nodeToBeDeleted = nodeToBeDeleted->next;
			}

			prev->next = nodeToBeDeleted->next;

			free(nodeToBeDeleted);
		}
	}
}


struct hash_item* linkedListRemove(struct linked_list* list)
{
	if (!list)
		return NULL;
	if (!list->next)
		return NULL;

	struct linked_list* node = list->next;
	struct linked_list* tmp = list;

	tmp->next = NULL;
	list = node;

	struct hash_item* it = NULL;

	if (it)
		memcpy(tmp->item, it, sizeof(struct hash_item));
	else
	{
		fprintf(stderr, "Can't allocate item for deletion\n");
		exit(0);
	}
	
	free(tmp->item->key);
	free(tmp->item->value);
	free(tmp->item);
	free(tmp);

	return it;
}



void implementedDeletion(struct hash_table* table, char* key)
{
	if (table->count != 0)
	{
		size_t timeElapsed = 0;
		uint64_t computeVal = 0;

		getTimeLapsComputeVariables(timeElapsed, deleteKey, table, key, computeVal);
		showComputeElapsedTime(computeVal, timeElapsed, "Hash Table Deletion", global_show_compute, global_log_level);

		if (global_log_level == logLevel::debug)
		{
			printf("remaining key/value after deletion\n");
			printTable(table);
		}
	}
	else
	{
		printf("Try To insert a value first, your table is empty!\n");
	}
}