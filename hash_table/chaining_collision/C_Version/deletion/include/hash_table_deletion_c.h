#pragma once

#include <common_project.h>
#include <hash_table_common_c.h>

#ifdef TEST_FOUND 
#include "CppUTest/MemoryLeakDetectorMallocMacros.h"
#endif

void deleteKey(struct hash_table* table, char* key, uint64_t& computeVal);

struct hash_item* linkedListRemove(struct linked_list* list);

void implementedDeletion(struct hash_table* table, char* key);
