#include "../include/array_search_c.h"

void implementedSearch(int* cArray, struct Common_Project_Values projectVal, int valToSearch)
{

	naiveAlgorithmValue(cArray, projectVal, valToSearch);

	if (global_log_level <= logLevel::debug)
		printf("binary Search\n\n");

	launchBinarySearch(cArray, projectVal, valToSearch);


}


void naiveSearch(int* cArray, struct Common_Project_Values projectVal, int valToSearch, int& checkSearchedValue, uint64_t& computeVal)
{

	for (int index = 0; index < projectVal.elementCount; ++index)
	{

		if (cArray[index] == valToSearch)
		{
			if (checkSearchedValue == 0) ++checkSearchedValue;

			if (global_log_level <= logLevel::debug)
				printf("value : %i found at index %i\n", valToSearch, index);
		}
	
		++computeVal;	
	}
}

void naiveAlgorithmValue(int * cArray, struct Common_Project_Values projectVal, int valToSearch)
{
	uint64_t computeVal = 0;
	int checkSearchedValue = 0;
	size_t timeElapsed = 0; 
	
	if (global_log_level <= logLevel::debug)
		printf("searching for value :  %i \n", valToSearch);

	getTimeLapsComputeVariables(timeElapsed, naiveSearch, cArray, projectVal, valToSearch, checkSearchedValue,computeVal);

	if (global_log_level <= logLevel::debug)
		printf("value %i not found\n", valToSearch);

	showComputeElapsedTime(computeVal, timeElapsed, "Naive Search", global_show_compute,global_log_level);

}


void binarySearch(int* cArray, int begin, int ending, int valFoSearch, int& checkSearchedValue, uint64_t& computeVal)
{

	if (begin <= ending)
	{
		int mid = begin + (ending - begin) / 2;

		if (cArray[mid] == valFoSearch)
		{
			if (checkSearchedValue == 0) ++checkSearchedValue;

			if (global_log_level <= logLevel::debug)
				printf("value : %i found at index %i\n", valFoSearch, mid);
		}

		if (cArray[mid] > valFoSearch)
		{
			binarySearch(cArray, begin, mid - 1, valFoSearch, checkSearchedValue, computeVal);
		}
		else
		{	
			binarySearch(cArray, mid + 1, ending, valFoSearch, checkSearchedValue, computeVal );
		}

	}
	
	++computeVal;

}

void launchBinarySearch(int* cArray, struct Common_Project_Values projectVal, int valToSearch)
{
	int checkSearchedValue = 0;
	uint64_t computeVal = 0;
	size_t timeElapsed = 0;

	if (global_log_level <= logLevel::debug)
	{
		printf("searching for value :  %i \n", valToSearch);
		printf("let's do a binary search, but first we need to sort the array\n");
	}
	
	arrayQuickSort(cArray,0,projectVal.elementCount -1, computeVal);

	computeVal = 0; 

	getTimeLapsComputeVariables(timeElapsed, binarySearch, cArray, 0, projectVal.elementCount - 1, valToSearch,checkSearchedValue, computeVal);

	if ( global_log_level <= logLevel::debug && !checkSearchedValue)
		printf("value %i not found\n", valToSearch);

	showComputeElapsedTime(computeVal, timeElapsed, "Binary Search", global_show_compute, global_log_level);

}