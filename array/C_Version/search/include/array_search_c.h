#pragma once

#include <array_quick_sort_c.h>
#include <common_project.h>
#include <array_common_c.h>

void implementedSearch(int * cArray, struct Common_Project_Values projectVal, int valToSearch);

void binarySearch(int* cArray, int begin, int ending, int valFoSearch, int& checkSearchedValue, uint64_t& computeVal);

void naiveSearch(int* cArray, struct Common_Project_Values projectVal, int valToSearch, int& checkSearchedValue, uint64_t& computeVal);

void naiveAlgorithmValue(int* cArray, struct Common_Project_Values projectVal, int valToSearch);

void launchBinarySearch(int* cArray, struct Common_Project_Values projectVal, int valToSearch);
