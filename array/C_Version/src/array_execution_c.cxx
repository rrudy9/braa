#include <stdlib.h>
#include <stdio.h>
#include <common_project.h>
#include <array_search_c.h>
#include <array_quick_sort_c.h>
#include <array_insertion_sort_c.h>
#include <array_bubble_sort_c.h>
#include <array_merge_sort_c.h>
#include <argtable3.h>


#ifdef TEST_FOUND 
#include "CppUTest/MemoryLeakDetectorMallocMacros.h"
#endif


bool global_show_compute = false;
logLevel global_log_level = logLevel::info;
bool global_result_validation = false;


int main(int argc, char* argv[]) 
{
	struct arg_lit
		* help = NULL,
		* bubbleSort = NULL,
		* insertionSort = NULL,
		* mergeSort = NULL,
		* quickSort = NULL,
		* checkResultValidation = NULL,
		* printArray = NULL,
		* showCompute = NULL;

	struct arg_str
		* logLevelValue = NULL;

	struct arg_int
		* setElementCount = NULL,
		* setRandSeedValue = NULL,
		* searchAlgorithm = NULL;


	struct arg_end* end = NULL;

	struct Common_Project_Values arrCommonValue = { 100000 , 30 };
	int* cArray = NULL; 
	int* copyArray = NULL;

	
	void* argtable[] = {
		help = arg_litn(NULL, "help", 0, 1, "display this help for Array <C Version> and exit"),
		showCompute = arg_litn("c","show-compute" ,0, 1, "Show Compute"),
		checkResultValidation = arg_litn("t","result-validation" ,0, 1, "Show Result Validation for sorting algorithms"),
		logLevelValue = arg_strn("v", "verbose","<info,debug,error,fatal>", 0, 1, "Set Log Level (info, debug, error, fatal) (Default : info)"),
		setElementCount = arg_intn("a", "array-count","<n>" , 0, 1, "Set Array Number of element (Default Value: 100k) Doesn't work when n = 0 "),
		setRandSeedValue = arg_intn("r", "rand-seed","<n>", 0, 1, "Set value for Seed Random (Default Value: 30)"),

		searchAlgorithm = arg_intn("s", "search","<value>", 0, 1, "Access Array C Search Algorithm implementation"),
		bubbleSort = arg_litn("b", "bubble-sort", 0, 1, "Access Array C Bubble Sort Algorithm implementation"),
		insertionSort = arg_litn("i", "insertion-sort", 0, 1, "Access Array C Insertion Sort Algorithm implementation"),
		mergeSort = arg_litn("m", "merge-sort", 0, 1, "Access Array C Merge Sort Algorithm implementation"),
		quickSort = arg_litn("q", "quick-sort", 0, 1, "Access Array C Quick Sort Algorithm implementation"),
		printArray = arg_litn("p", "print-array", 0, 1, "Show Array"),
		
		end = arg_end(20)
	};

	int exitcode = 0;
	char progname[] = "arrayC.exe";

	int nerrors;
	nerrors = arg_parse(argc, argv, argtable);

	if (argc == 1) {
		printf("Please choose one of the available commands\n\n");
		arg_print_glossary(stdout, argtable, "  %-25s %s\n");
		exitcode = 0;
		goto exit;
	}

	if (help->count > 0)
	{
		printf("Usage: %s", progname);
		arg_print_syntax(stdout, argtable, "\n\n");
		arg_print_glossary(stdout, argtable, "  %-25s %s\n");
		exitcode = 0;
		goto exit;
	}

	
	//If the parser returned any errors then display them and exit 
	if (nerrors > 0)
	{
		// Display the error details contained in the arg_end struct.
		arg_print_errors(stderr, end, progname);
		fprintf(stderr,"Try '%s --help' for more information.\n", progname);
		exitcode = 1;
		goto exit;
	}

	

	if (showCompute->count > 0)
	{
		global_show_compute = !global_show_compute;
	}
	
	if (checkResultValidation->count > 0)
	{
		global_result_validation = !global_result_validation;
	}

	if (logLevelValue->count > 0)
	{
		setLogLevel(*(logLevelValue)->sval, global_log_level);
	}
	
	//set project common value (random seed , and number of element in array
	if (setElementCount->count > 0)
	{
		if (*(setElementCount)->ival == 0) {
			printf("Your array is empty\n");
			goto exit;
		} 
		
		arrCommonValue.elementCount = *(setElementCount)->ival;
	}

	if (setRandSeedValue->count > 0)
	{
		arrCommonValue.seedRand = *(setRandSeedValue)->ival;
	}
	
	srand(arrCommonValue.seedRand);
	showCommonProjectValue(arrCommonValue, global_log_level);
	
	createCArray(&cArray, arrCommonValue);

	if (searchAlgorithm->count > 0)
	{
		implementedSearch(cArray, arrCommonValue, *(searchAlgorithm)->ival);
	}

	if (bubbleSort->count > 0)
	{

		// we copy the array so we can compare sorting algorithms for the same array
		copyArray = (int*)malloc(arrCommonValue.elementCount * sizeof(int));
		if (copyArray != NULL)
			memcpy(copyArray, cArray, sizeof(int) * arrCommonValue.elementCount);
		else {
			fprintf(stderr, "Can't allocate your copy array\n");
			exit(0);
		}

		implementedBubbleSort(copyArray, arrCommonValue);

		if (global_result_validation)
			checkIfArrayIsSorted(copyArray, arrCommonValue, "BubbleSort");


		free(copyArray);
	}

	if (insertionSort->count > 0)
	{
		copyArray = (int*)malloc(arrCommonValue.elementCount * sizeof(int));
		if (copyArray != NULL)
			memcpy(copyArray, cArray, sizeof(int) * arrCommonValue.elementCount);
		else {
			fprintf(stderr, "Can't allocate your copy array\n");
			exit(0);
		}
		implementedInsertionSort(copyArray, arrCommonValue);

		if (global_result_validation)
			checkIfArrayIsSorted(copyArray, arrCommonValue, "InsertionSort");

		free(copyArray);

	}

	if (mergeSort->count > 0)
	{
		copyArray = (int*)malloc(arrCommonValue.elementCount * sizeof(int));
		if (copyArray != NULL)
			memcpy(copyArray, cArray, sizeof(int) * arrCommonValue.elementCount);
		else {
			fprintf(stderr, "Can't allocate your copy array\n");
			exit(0);
		}
		implementedMergeSort(copyArray, arrCommonValue);

		if (global_result_validation)
			checkIfArrayIsSorted(copyArray, arrCommonValue, "MergeSort");

		free(copyArray);

	}

	if (quickSort->count > 0)
	{
		copyArray = (int*)malloc(arrCommonValue.elementCount * sizeof(int));
		if (copyArray != NULL)
			memcpy(copyArray, cArray, sizeof(int) * arrCommonValue.elementCount);
		else {
			fprintf(stderr, "Can't allocate your copy array\n");
			exit(0);
		}
		implementedQuickSort(copyArray, arrCommonValue);

		if (global_result_validation)
			checkIfArrayIsSorted(copyArray, arrCommonValue, "Implemented QuickSort");

		free(copyArray);
	}

	if (printArray->count > 0)
	{
		showArray(cArray, "Your Array " ,arrCommonValue);
	}
	

	
	arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
	destroyCArray(cArray);

	return exitcode;


exit:
	/* deallocate each non-null entry in argtable[] */
	arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
	return exitcode;
}
