#include "CppUTest/TestHarness.h"
#include "CppUTest/MemoryLeakDetectorMallocMacros.h"

#include <common_project.h>
#include <array_search_c.h>
#include <array_quick_sort_c.h>
#include <array_insertion_sort_c.h>
#include <array_bubble_sort_c.h>
#include <array_merge_sort_c.h>

void checkArrayTest(int* cArray, struct Common_Project_Values projectVal)
{
	bool checkIfArrayIsSorted = false;

	for (int i = 0; i < projectVal.elementCount - 1; ++i)
	{
		int c1 = cArray[i];
		int c2 = cArray[++i];
		if (c1 > c2)
		{
			checkIfArrayIsSorted = !checkIfArrayIsSorted;
			CHECK(!checkIfArrayIsSorted);
			return;
		}
	}
}


TEST_GROUP(TEST_ARRAY_C)
{
	struct Common_Project_Values arrCommonValue = {global_test_count_element, global_test_rand_seed};
	int* cArray = NULL;

	void setup()
	{
		showCommonProjectValue(arrCommonValue, global_log_level);

		createCArray(&cArray, arrCommonValue);
	}

	void teardown()
	{
		destroyCArray(cArray);
	}
};

TEST(TEST_ARRAY_C, search)
{
	int valToSearch = rand() % 10011 + 1;

	implementedSearch(cArray, arrCommonValue, valToSearch);

	FAIL("force failing for search on purpose");

}

TEST(TEST_ARRAY_C, quickSort)
{
	implementedQuickSort(cArray, arrCommonValue);

	checkArrayTest(cArray, arrCommonValue);

}


TEST(TEST_ARRAY_C, mergeSort)
{
	implementedMergeSort(cArray, arrCommonValue);

	checkArrayTest(cArray, arrCommonValue);


}


TEST(TEST_ARRAY_C, insertionSort)
{
	implementedInsertionSort(cArray, arrCommonValue);

	checkArrayTest(cArray, arrCommonValue);

}


TEST(TEST_ARRAY_C, bubbleSort)
{
	implementedBubbleSort(cArray, arrCommonValue);

	checkArrayTest(cArray, arrCommonValue);

}



