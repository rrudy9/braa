#include "../include/array_common_c.h"


void createCArray(int** cArray, struct Common_Project_Values projectVal)
{
	(*cArray) = (int*) malloc (projectVal.elementCount * sizeof(cArray));

	if (!cArray)
	{
		fprintf(stderr,"can't allocate array in C");
		exit(0);
	}

	for (int indexRandVal = 0; indexRandVal < projectVal.elementCount; ++indexRandVal)
	{
		int randVal = rand() % 10001;
		(*cArray)[indexRandVal] = randVal;
	}

}

void destroyCArray(int* cArray)
{
	free(cArray);
}

void checkIfArrayIsSorted(int* cArray, struct Common_Project_Values projectVal, char* sortingAlgorithm)
{
	bool checkSortedArray = false;
	for (int i = 0; i < projectVal.elementCount - 1; ++i)
	{
		int c1 = cArray[i];
		int c2 = cArray[++i];
		if (c1 > c2)
		{
			checkSortedArray = !checkSortedArray;
			if (global_log_level <= logLevel::error)
				fprintf(stderr,"Testing if Array is sorted for %s algorithm: Array is not sorted\n", sortingAlgorithm);
			return;
		}
	}

	printf("Testing if Array is sorted for %s algorithm: Validated - Array was sorted Successfully\n\n", sortingAlgorithm);
}



void showArray(int* cArray, char* hint, struct Common_Project_Values projectVal)
{
	printf("%s\n", hint);

	for (int index = 0; index < projectVal.elementCount; ++index)
	{
		if (index == 0) printf("[ ");

		if (index == projectVal.elementCount - 1)
		{
			printf("%i", cArray[index]);
			printf(" ]\n");
		}
		else
		{
			printf("%i,", cArray[index]);
		}
	}
	printf("\n");

}


void getTimeLapsComputeVariables(size_t& elapsedTime, void (*f)(int*, struct Common_Project_Values, int, int&, uint64_t&), int* cArray, struct Common_Project_Values projectVal, int valToSearch, int& checkSearchedValue, uint64_t& computeVal)
{
	std::chrono::steady_clock::time_point t1 = std::chrono::high_resolution_clock::now();
	f(cArray, projectVal, valToSearch, checkSearchedValue, computeVal);
	std::chrono::steady_clock::time_point t2 = std::chrono::high_resolution_clock::now();

	elapsedTime = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();

}

void getTimeLapsComputeVariables(size_t& elapsedTime, void (*f)(int*, int, int, int, int&, uint64_t&), int* cArray, int begin, int ending, int valToSearch, int& checkSearchedValue, uint64_t& computeVal)
{
	std::chrono::steady_clock::time_point t1 = std::chrono::high_resolution_clock::now();
	f(cArray, begin,ending, valToSearch, checkSearchedValue, computeVal);
	std::chrono::steady_clock::time_point t2 = std::chrono::high_resolution_clock::now();

	elapsedTime = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();

}

void getTimeLapsComputeVariables(size_t& elapsedTime, void (*f)(int*, int, int, uint64_t&), int* cArray, int startingPivot, int endingPivot, uint64_t& computeVal)
{
	std::chrono::steady_clock::time_point t1 = std::chrono::high_resolution_clock::now();
	f(cArray, startingPivot, endingPivot, computeVal);
	std::chrono::steady_clock::time_point t2 = std::chrono::high_resolution_clock::now();

	elapsedTime = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();
}

void getTimeLapsComputeVariables(size_t& elapsedTime, void (*f)(int*, struct Common_Project_Values, uint64_t&), int* cArray, struct Common_Project_Values projectVal, uint64_t& computeVal)
{
	std::chrono::steady_clock::time_point t1 = std::chrono::high_resolution_clock::now();
	f(cArray,projectVal, computeVal);
	std::chrono::steady_clock::time_point t2 = std::chrono::high_resolution_clock::now();

	elapsedTime = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();
}


