#ifndef ARRAY_COMMON_C
#define ARRAY_COMMON_C

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <ctime>
#include <iterator>
#include <vector>
#include <algorithm>
#include <math.h>
#include <chrono>

#include <common_project.h>

#ifdef TEST_FOUND 
#include "CppUTest/MemoryLeakDetectorMallocMacros.h"
#endif

void createCArray(int** cArray, struct Common_Project_Values projectVal);
void showArray(int* cArray, char* hint, struct Common_Project_Values projectVal);
void destroyCArray(int* cArray);
void checkIfArrayIsSorted(int* cArray, struct Common_Project_Values projectVal, char* sortingAlgorithm);



void getTimeLapsComputeVariables(size_t& elapsedTime, void (*f)(int*, struct Common_Project_Values, int, int&, uint64_t&), int* cArray, struct Common_Project_Values projectVal, int valToSearch, int& checkSearchedValue, uint64_t& computeVal);
void getTimeLapsComputeVariables(size_t& elapsedTime, void (*f)(int*, int, int, int, int&, uint64_t&), int* cArray, int begin, int ending,int valToSearch, int& checkSearchedValue, uint64_t& computeVal);
void getTimeLapsComputeVariables(size_t& elapsedTime, void (*f)(int*, int, int, uint64_t&), int* cArray, int startingPivot, int endingPivot, uint64_t&computeVal);
void getTimeLapsComputeVariables(size_t& elapsedTime, void (*f)(int*, struct Common_Project_Values, uint64_t&), int* cArray, struct Common_Project_Values projectVal, uint64_t& computeVal);

#endif