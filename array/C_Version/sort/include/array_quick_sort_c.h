#pragma once

#include <array_common_c.h>
#include <common_project.h>

#ifdef TEST_FOUND 
#include "CppUTest/MemoryLeakDetectorMallocMacros.h"
#endif

int partitioningArray(int* cArray, int starting, int ending, uint64_t& computVal);

void arrayQuickSort(int* cArray, int starting, int ending, uint64_t& computeVal);

void implementedQuickSort(int* cArray, struct Common_Project_Values projectVal);

void implementedQuickSortC(int* cArray, struct Common_Project_Values projectVal);
void libQSort(int* cArray, struct Common_Project_Values projectVal);

int compare(const void* a, const void* b);
