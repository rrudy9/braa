#pragma once

#include <array_common_c.h>
#include <common_project.h>

#ifdef TEST_FOUND 
#include "CppUTest/MemoryLeakDetectorMallocMacros.h"
#endif

void mergeSortC(int* cArray, int starting, int ending, uint64_t& computeVal);
void arrayMergeSort(int* cArray, int starting, int queuePoint, int ending, uint64_t& computeVal);


void implementedMergeSort(int* cArray, struct Common_Project_Values projectVal);
