#pragma once

#include <array_common_c.h>
#include <common_project.h>


#ifdef TEST_FOUND 
#include "CppUTest/MemoryLeakDetectorMallocMacros.h"
#endif

void bubbleSortC(int* cArray, struct Common_Project_Values projectVal, uint64_t& computeVal);

void implementedBubbleSort(int* cArray, struct Common_Project_Values projectVal);
