#pragma once

#include <array_common_c.h>
#include <common_project.h>

void insertionSortC(int* cArray, struct Common_Project_Values projectVal, uint64_t& computeVal);

void implementedInsertionSort(int* cArray, struct Common_Project_Values projectVal);