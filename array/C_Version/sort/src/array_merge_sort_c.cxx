#include "../include/array_merge_sort_c.h"

void mergeSortC(int* cArray, int starting, int ending, uint64_t& computeVal)
{
	if (starting < ending)
	{			
		int mid = starting + (ending - starting) / 2;
		mergeSortC(cArray, starting, mid, computeVal);
		mergeSortC(cArray, mid + 1, ending, computeVal);
		arrayMergeSort(cArray, starting, mid, ending, computeVal);
	}

	++computeVal;

}


void arrayMergeSort(int* cArray, int starting, int queuePoint, int ending, uint64_t& computeVal)
{
	int n1 = queuePoint - starting + 1;
	int n2 = ending - queuePoint;

	int* left = (int*)malloc(n1 * sizeof(*left));
	int* right = (int*)malloc(n2 * sizeof(*right));

	if (!left || !right)
		exit(0); 

	for (int i = 0; i < n1; ++i)
	{
		left[i] = cArray[starting + i];
	}

	for (int i = 0; i < n2; ++i)
	{
		right[i] = cArray[queuePoint + 1 + i];
	}

	int i = 0, j = 0, k = starting;

	while (i < n1 && j < n2)
	{
		++computeVal;

		if (left[i] <= right[j])
		{
			cArray[k] = left[i];
			++i;
		}
		else
		{
			cArray[k] = right[j];
			++j;
		}
		++k;
	}

	while (i < n1)
	{
		cArray[k] = left[i];
		++i; ++k;
	}

	while (j < n2)
	{
		cArray[k] = right[j];
		++j; ++k;

	}

	free(left);
	free(right);
}




void implementedMergeSort(int* cArray, struct Common_Project_Values projectVal)
{

	uint64_t computeVal = 0;
	size_t timeElapsed = 0;

	if (global_log_level == logLevel::debug)
		showArray(cArray, "Your Allocated Array", projectVal);

	getTimeLapsComputeVariables(timeElapsed, mergeSortC, cArray, 0, projectVal.elementCount - 1, computeVal);

	if (global_log_level == logLevel::debug)
		showArray(cArray, "Your Sorted Array with Merge Sort", projectVal);

	showComputeElapsedTime(computeVal, timeElapsed, "Merge Sort", global_show_compute, global_log_level);
}
