#include "../include/array_quick_sort_c.h"


int partitioningArray(int* cArray, int starting, int ending, uint64_t& computeVal)
{
	int x = cArray[ending];
	int i = starting - 1;

	for (int j = starting; j < ending; ++j)
	{
		++computeVal;
		
		if (cArray[j] <= x)
		{
			i = i + 1;
			int c = cArray[j];
			cArray[j] = cArray[i];
			cArray[i] = c;
		}
	
	}

	int c = cArray[i + 1];
	cArray[i + 1] = cArray[ending];
	cArray[ending] = c;

	return i + 1;
}


void arrayQuickSort(int* cArray, int starting, int ending, uint64_t& computeVal)
{
	if (starting < ending)
	{	
		int q = partitioningArray(cArray, starting, ending, computeVal);
		arrayQuickSort(cArray, starting, q - 1, computeVal);
		arrayQuickSort(cArray, q + 1, ending, computeVal);
	}
	++computeVal;

}

void implementedQuickSort(int* cArray, struct Common_Project_Values projectVal)
{
	int* aCopyFoTestingQSortLib = (int*) malloc (projectVal.elementCount * sizeof(aCopyFoTestingQSortLib));

	memcpy(aCopyFoTestingQSortLib, cArray, sizeof(int) * projectVal.elementCount);

	implementedQuickSortC(cArray, projectVal);

	if (global_log_level == logLevel::debug)
		showArray(aCopyFoTestingQSortLib, "Your Allocated beforeee before quick sort Std", projectVal);

	libQSort(aCopyFoTestingQSortLib, projectVal);

	free(aCopyFoTestingQSortLib);
}

void implementedQuickSortC(int* cArray, struct Common_Project_Values projectVal)
{
	size_t timeElapsed = 0;
	uint64_t computeVal = 0;

	if (global_log_level == logLevel::debug)
		showArray(cArray, "Your Allocated Array before quick sort", projectVal);

	getTimeLapsComputeVariables(timeElapsed, arrayQuickSort, cArray, 0, projectVal.elementCount - 1, computeVal);
	
	if (global_log_level == logLevel::debug)
		showArray(cArray, "Your Sorted Allocated Array with QuickSort", projectVal);

	showComputeElapsedTime(computeVal, timeElapsed, "Quick Sort", global_show_compute, global_log_level);
}


void libQSort(int* cArray, struct Common_Project_Values projectVal)
{
	size_t timeElapsed = 0;

	std::chrono::steady_clock::time_point t1 = std::chrono::high_resolution_clock::now();
	qsort(cArray, projectVal.elementCount, sizeof(*cArray), compare);
	std::chrono::steady_clock::time_point t2 = std::chrono::high_resolution_clock::now();

	timeElapsed = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();

	if (global_log_level == logLevel::debug)
		showArray(cArray, "Your Sorted Allocated Array with qsort (lib)", projectVal);

	if (global_log_level <= logLevel::debug)
		printf("Elapsed time (ms) for Lib QSort: %zu\n\n", timeElapsed);

}

int compare(const void* a, const void* b)
{
	return (*(int*)a - *(int*)b);
}
