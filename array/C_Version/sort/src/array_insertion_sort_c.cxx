#include "../include/array_insertion_sort_c.h"

void insertionSortC(int* cArray, struct Common_Project_Values projectVal, uint64_t& computeVal)
{

	for (int index = 1; index < projectVal.elementCount; ++index)
	{
		int key = cArray[index];
		int j = index - 1;
		
		++computeVal;

		while ((j >= 0) && (cArray[j] > key) )
		{
			++computeVal;

			cArray[j + 1] = cArray[j];
			j = j - 1;
		}
		cArray[j + 1] = key;
	}
}


void implementedInsertionSort(int* cArray, struct Common_Project_Values projectVal)
{

	uint64_t computeVal = 0;
	size_t timeElapsed = 0;

	if (global_log_level == logLevel::debug)
		showArray(cArray, "Your Allocated Array", projectVal);

	getTimeLapsComputeVariables(timeElapsed, insertionSortC, cArray, projectVal, computeVal);

	if (global_log_level == logLevel::debug)
		showArray(cArray, "Your Sorted Array with Insertion Sort", projectVal);

	showComputeElapsedTime(computeVal, timeElapsed, "Insertion Sort", global_show_compute, global_log_level);

}