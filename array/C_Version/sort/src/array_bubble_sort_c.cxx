#include "../include/array_bubble_sort_c.h"


void bubbleSortC(int* cArray, struct Common_Project_Values projectVal, uint64_t& computeVal)
{
	for (int i = 0; i < projectVal.elementCount - 1; ++i)
	{
		for (int j = 0; j < projectVal.elementCount - i - 1; ++j)
		{
			if (cArray[j] > cArray[j + 1])
			{	
				std::swap(cArray[j], cArray[j + 1]);
			}
			
			++computeVal;
		}
	}
}


void implementedBubbleSort(int* cArray, struct Common_Project_Values projectVal)
{

	uint64_t computeVal = 0;
	size_t timeElapsed = 0;

	if (global_log_level == logLevel::debug)
		showArray(cArray, "Your Allocated Array", projectVal);

	getTimeLapsComputeVariables(timeElapsed, bubbleSortC, cArray, projectVal, computeVal);

	if (global_log_level == logLevel::debug)
		showArray(cArray, "Your Sorted Array with Bubble Sort", projectVal);

	showComputeElapsedTime(computeVal, timeElapsed, "Bubble Sort", global_show_compute, global_log_level);

}