#include <stdlib.h>
#include <stdio.h>
#include <common_project.h>
#include <array_search_std.h>
#include <array_quick_sort_std.h>
#include <array_insertion_sort_std.h>
#include <array_bubble_sort_std.h>
#include <array_merge_sort_std.h>
#include <argtable3.h>

#ifdef TEST_FOUND 
#include "CppUTest/MemoryLeakDetectorMallocMacros.h"
#endif

bool global_show_compute = false;
logLevel global_log_level = logLevel::info;
bool global_result_validation = false;


int main(int argc, char* argv[])
{
	struct arg_lit
		* help = NULL,
		* bubbleSort = NULL,
		* insertionSort = NULL,
		* mergeSort = NULL,
		* showCompute = NULL,
		* checkResultValidation = NULL,
		* printVector = NULL,
		* quickSort = NULL;

	struct arg_str
		* logLevelValue = NULL;

	struct arg_int
		* setElementCount = NULL,
		* setRandSeedValue = NULL,
		* searchAlgorithm = NULL;


	struct arg_end* end = NULL;

	struct Common_Project_Values vecCommonValue = { 100000 , 30 };
	std::vector<int> stdVector = {};

	void* argtable[] = {
		help = arg_litn(NULL, "help", 0, 1, "display this help for Vector <Std Version> and exit"),
		showCompute = arg_litn("c","show-compute",0, 1, "Show Compute"),
		checkResultValidation = arg_litn("t","result-validation" ,0, 1, "Show Result Validation for sorting algorithms"),	
		logLevelValue = arg_strn("v", "verbose","<info,debug,error,fatal>", 0, 1, "Set Log Level (info, debug, error, fatal) (Default : info)"),
		setElementCount = arg_intn("a", "array-count","<n>" , 0, 1, "Set Array Number of element (Default Value: 100k), Doesn't work when n = 0 "),
		setRandSeedValue = arg_intn("r", "rand-seed","<n>", 0, 1, "Set value for Seed Random (Default Value: 30)"),
		printVector = arg_litn("p", "print-vector", 0, 1, "Show Vector"),

		searchAlgorithm = arg_intn("s", "search","<value>", 0, 1, "Access Vector Std Search Algorithm implementation"),
		bubbleSort = arg_litn("b", "bubble-sort", 0, 1, "Access Vector Std Bubble Sort Algorithm implementation"),
		insertionSort = arg_litn("i", "insertion-sort", 0, 1, "Access Vector Std Insertion Sort Algorithm implementation"),
		mergeSort = arg_litn("m", "merge-sort", 0, 1, "Access Vector Std Merge Sort Algorithm implementation"),
		quickSort = arg_litn("q", "quick-sort", 0, 1, "Access Vector Std Quick Sort Algorithm implementation"),
		end = arg_end(20)
	};


	int exitcode = 0;
	char progname[] = "arrayStd.exe";

	int nerrors;
	nerrors = arg_parse(argc, argv, argtable);

	if (argc == 1) {
		printf("Please choose one of the available commands\n\n");
		arg_print_glossary(stdout, argtable, "  %-25s %s\n");
		exitcode = 0;
		goto exit;
	}

	if (help->count > 0)
	{
		printf("Usage: %s", progname);
		arg_print_syntax(stdout, argtable, "\n\n");
		arg_print_glossary(stdout, argtable, "  %-25s %s\n");
		exitcode = 0;
		goto exit;
	}


	//If the parser returned any errors then display them and exit 
	if (nerrors > 0)
	{
		// Display the error details contained in the arg_end struct.
		arg_print_errors(stderr, end, progname);
		fprintf(stderr,"Try '%s --help' for more information.\n", progname);
		exitcode = 1;
		goto exit;
	}

	if (showCompute->count > 0)
	{
		global_show_compute = !global_show_compute;
	}

	if (checkResultValidation->count > 0)
	{
		global_result_validation = !global_result_validation;
	}

	if (logLevelValue->count > 0)
	{
		setLogLevel(*(logLevelValue)->sval, global_log_level);

	//	if (global_log_level == logLevel::none) goto exit;
	}

	//set project common value (random seed , and number of element in array
	if (setElementCount->count > 0)
	{
		if (*(setElementCount)->ival == 0) goto exit;

		vecCommonValue.elementCount = *(setElementCount)->ival;
	}

	if (setRandSeedValue->count > 0)
	{
		vecCommonValue.seedRand = *(setRandSeedValue)->ival;
	}

	srand(vecCommonValue.seedRand);
	showCommonProjectValue(vecCommonValue, global_log_level);

	createVector(stdVector, vecCommonValue);

	if (searchAlgorithm->count > 0)
	{
		implementedSearch(stdVector, *(searchAlgorithm)->ival);
	}

	if (bubbleSort->count > 0)
	{
		implementedBubbleSort(stdVector);
	}

	if (insertionSort->count > 0)
	{
		implementedInsertionSort(stdVector);
	}

	if (mergeSort->count > 0)
	{
		implementedMergeSort(stdVector);
	}

	if (quickSort->count > 0)
	{
		implementedQuickSort(stdVector);
	}

	if (printVector->count > 0)
	{
		showVector(stdVector, "Your Vector", stdVector.size());
	}


	arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));

	return exitcode;


exit:
	/* deallocate each non-null entry in argtable[] */
	arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
	return exitcode;
}
	
