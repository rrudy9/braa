file(GLOB array_common_std 
	.cxx .hxx 
	src/*.cxx 
	include/*.h)

add_library(array_common_std_lib ${array_common_std})


set(linkingLibrary 
	${CPPUTEST_LDFLAGS}  
	common_project_lib 
	)

set(linkingDirectories 
	"${CPPUTEST_INCLUDE_DIRS}"
	"${PROJECT_SOURCE_DIR}/common_project/include" 

	)

	
if (CPPUTEST_LIBRARIES)
	add_compile_definitions(TEST_FOUND="test")
endif(CPPUTEST_LIBRARIES)



target_link_libraries(array_common_std_lib PRIVATE ${linkingLibrary})


target_include_directories(array_common_std_lib PRIVATE ${linkingDirectories})

set_property(TARGET array_common_std_lib PROPERTY FOLDER "array/Std_Version/common")
