#include "../include/array_common_std.h"

void createVector(std::vector<int>& stdVector, struct Common_Project_Values projectVal)
{
	stdVector = std::vector<int>(projectVal.elementCount);
	
	for (int indexRandVal = 0; indexRandVal < projectVal.elementCount; ++indexRandVal)
	{
		int randVal = rand() % 10001;
		stdVector[indexRandVal] = randVal;
	}
}

void showVector(std::vector<int>& stdVector, char* hint, size_t elementCount)
{
	printf("%s\n", hint);

	for (std::vector<int>::iterator it = stdVector.begin(); it != stdVector.end(); ++it)
	{
		if (it == stdVector.begin()) printf("[ ");

		if (std::distance(stdVector.begin(), it) == elementCount - 1)
		{                    
			printf("%i", *it);
			printf(" ]\n");
		}
		else
		{
			printf("%i,", *it);
		}
	}
}


void checkIfVectorIsSorted(std::vector<int> veC,char* sortingAlgorithm)
{
	bool checkSortedArray = false;
	for (int i = 0; i < veC.size() - 1; ++i)
	{
		int c1 = veC.at(i);
		int c2 = veC.at(++i);
		if (c1 > c2)
		{
			checkSortedArray = !checkSortedArray;
			fprintf(stderr,"Testing if Vector is sorted for %s sorting algorithm: Vector is not sorted\n", sortingAlgorithm);
			return;
		}
	}
	printf("Testing if Vector is sorted for %s sorting algorithm: Validated - Vector was sorted Successfully\n\n", sortingAlgorithm);

}


void getTimeLapsComputeVariables(size_t& elapsedTime, void (*f)(std::vector<int>, int, int&, uint64_t&), std::vector<int> stdVector, int valToSearch, int& checkSearchedValue, uint64_t& computeVal)
{
	std::chrono::steady_clock::time_point t1 = std::chrono::high_resolution_clock::now();
	f(stdVector, valToSearch,checkSearchedValue, computeVal);
	std::chrono::steady_clock::time_point t2 = std::chrono::high_resolution_clock::now();

	elapsedTime = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();
}

void getTimeLapsComputeVariables(size_t& elapsedTime, void (*f)(std::vector<int>, std::vector<int>::iterator, std::vector<int>::iterator, int, int&, uint64_t&), std::vector<int> stdVector, std::vector<int>::iterator begin, std::vector<int>::iterator ending, int valToSearch, int& checkSearchedValue, uint64_t& computeVal)
{
	std::chrono::steady_clock::time_point t1 = std::chrono::high_resolution_clock::now();
	f(stdVector, begin, ending,valToSearch, checkSearchedValue, computeVal);
	std::chrono::steady_clock::time_point t2 = std::chrono::high_resolution_clock::now();

	elapsedTime = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();
}


void getTimeLapsComputeVariables(size_t& elapsedTime, void (*f)(std::vector<int>::iterator, std::vector<int>::iterator, uint64_t&), std::vector<int>::iterator begin, std::vector<int>::iterator ending, uint64_t& computeVal)
{
	std::chrono::steady_clock::time_point t1 = std::chrono::high_resolution_clock::now();
	f(begin, ending, computeVal);
	std::chrono::steady_clock::time_point t2 = std::chrono::high_resolution_clock::now();

	elapsedTime = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();
}

void getTimeLapsComputeVariables(size_t& elapsedTime, void (*f)(std::vector<int>&, uint64_t&), std::vector<int>& stdVector, uint64_t& computeVal)
{
	std::chrono::steady_clock::time_point t1 = std::chrono::high_resolution_clock::now();
	f(stdVector, computeVal);
	std::chrono::steady_clock::time_point t2 = std::chrono::high_resolution_clock::now();

	elapsedTime = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();
}

