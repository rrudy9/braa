#pragma once

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <ctime>
#include <iterator>
#include <vector>
#include <algorithm>
#include <math.h>
#include <chrono>

#include <common_project.h>

#ifdef TEST_FOUND 
#include "CppUTest/MemoryLeakDetectorMallocMacros.h"
#endif

void createVector(std::vector<int> & stdVector, struct Common_Project_Values projectVal);
void showVector(std::vector<int>& stdVector, char* hint, size_t elementCount);
void checkIfVectorIsSorted(std::vector<int> stdVector, char* sortingAlgorithm);

void getTimeLapsComputeVariables(size_t& elapsedTime, void (*f)(std::vector<int>, int, int&, uint64_t&), std::vector<int> stdVector, int valToSearch, int& checkSearchedValue, uint64_t& computeVal);
void getTimeLapsComputeVariables(size_t& elapsedTime, void (*f)(std::vector<int>, std::vector<int>::iterator, std::vector<int>::iterator, int,int&, uint64_t&), std::vector<int> stdVector, std::vector<int>::iterator begin, std::vector<int>::iterator ending, int valToSearch, int& checkSearchedValue, uint64_t& computeVal);
void getTimeLapsComputeVariables(size_t& elapsedTime, void (*f)(std::vector<int>::iterator, std::vector<int>::iterator, uint64_t&),std::vector<int>::iterator begin, std::vector<int>::iterator ending, uint64_t& computeVal);
void getTimeLapsComputeVariables(size_t& elapsedTime, void (*f)(std::vector<int>&, uint64_t&), std::vector<int>& stdVector, uint64_t& computeVal);

