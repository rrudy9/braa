#include "CppUTest/TestHarness.h"
#include "CppUTest/MemoryLeakDetectorMallocMacros.h"

#include <common_project.h>
#include <array_search_std.h>
#include <array_quick_sort_std.h>
#include <array_insertion_sort_std.h>
#include <array_bubble_sort_std.h>
#include <array_merge_sort_std.h>


void returnCheckTest(std::vector<int> stdVector)
{
	bool checkIfArrayIsSorted = false;

	for (int i = 0; i < stdVector.size() - 1; ++i)
	{
		int c1 = stdVector.at(i);
		int c2 = stdVector.at(++i);
		if (c1 > c2)
		{
			checkIfArrayIsSorted = !checkIfArrayIsSorted;
			CHECK(!checkIfArrayIsSorted);
			return;
		}
	}
}

TEST_GROUP(TEST_ARRAY_STD)
{
	struct Common_Project_Values vecCommonValue = { global_test_count_element, global_test_rand_seed };
	std::vector<int> stdVector = {};

	void setup()
	{
		createVector(stdVector, vecCommonValue);
	}

	void teardown()
	{
	}
};

TEST(TEST_ARRAY_STD, search)
{
	int valToSearch = rand() % 10011 + 1;

	implementedSearch(stdVector, valToSearch);

	FAIL("force failing for search on purpose");

}

TEST(TEST_ARRAY_STD, quickSort)
{
	implementedQuickSort(stdVector);

	returnCheckTest(stdVector);

}


TEST(TEST_ARRAY_STD, mergeSort)
{
	implementedMergeSort(stdVector);

	returnCheckTest(stdVector);

}


TEST(TEST_ARRAY_STD, insertionSort)
{
	implementedInsertionSort(stdVector);

	returnCheckTest(stdVector);


}


TEST(TEST_ARRAY_STD, bubbleSort)
{
	implementedBubbleSort(stdVector);

	returnCheckTest(stdVector);

}



