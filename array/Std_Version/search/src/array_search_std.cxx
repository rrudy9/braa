#include "../include/array_search_std.h"

void naiveSearch(std::vector<int> stdVector, int valToSearch, int& checkSearchedValue, uint64_t& computeVal)
{
	for (std::vector<int>::iterator it = stdVector.begin(); it != stdVector.end(); ++it)
	{
		++computeVal;

		if (*it == valToSearch)
		{	
			if (checkSearchedValue == 0) ++checkSearchedValue;
			
			if (global_log_level <= logLevel::debug)
				printf("value %i found at index %zu \n", *it, std::distance(stdVector.begin(), it));
		}
	}

}

void binarySearch(std::vector<int> stdVector, std::vector<int>::iterator begin, std::vector<int>::iterator ending, int valFoSearch, int& checkSearchedValue, uint64_t& computeVal)
{

	while (begin < ending)
	{
		++computeVal;

		std::vector<int>::iterator mid = begin + ((ending - begin) / 2);
		if (*mid == valFoSearch)
		{
			if (checkSearchedValue == 0) ++checkSearchedValue;

			if (global_log_level <= logLevel::debug)
				printf("value %i found at index %zu \n", *mid, std::distance(stdVector.begin(), mid));
			return; 
		}
		else if (*mid > valFoSearch)
		{	
			ending = mid;
		}
		else
		{
			begin = mid + 1;
		}
	}
}

void implementedSearch(std::vector<int>stdVector, int valToSearch)
{
	
	if (global_log_level == logLevel::debug)
		showVector(stdVector, "Your Allocated Vector", stdVector.size());

	naiveSearchLaunch(stdVector, valToSearch);

	if (global_log_level <= logLevel::debug)
		printf("binary search\n\n");

	binarySearchLaunch(stdVector, valToSearch);

}

void naiveSearchLaunch(std::vector<int> stdVector, int valToSearch)
{
	uint64_t computeVal = 0;
	int checkSearchedValue = 0;
	size_t timeElapsed = 0;

	if (global_log_level <= logLevel::debug)
		printf("searching for value :  %i \n", valToSearch);
	
	getTimeLapsComputeVariables(timeElapsed, naiveSearch, stdVector, valToSearch, checkSearchedValue, computeVal);
	
	showComputeElapsedTime(computeVal, timeElapsed, "Naive Search (Std)", global_show_compute, global_log_level);

}

void binarySearchLaunch(std::vector<int> stdVector, int valToSearch)
{
	int checkSearchedValue = 0;
	uint64_t computeVal = 0;
	size_t timeElapsed = 0;

	if (global_log_level <= logLevel::debug)
	{
		printf("searching for value :  %i \n", valToSearch);
		printf("let's do a binary search, but first we need to sort the array\n");
	}

	arrayQuickSortStd(stdVector.begin(), stdVector.end(), computeVal);

	computeVal = 0;

	getTimeLapsComputeVariables(timeElapsed, binarySearch, stdVector, stdVector.begin(), stdVector.end() , valToSearch, checkSearchedValue, computeVal);

	if (global_log_level <= logLevel::debug && !checkSearchedValue)
		printf("value %i not found\n", valToSearch);

	showComputeElapsedTime(computeVal, timeElapsed, "Binary Search (Std)", global_show_compute, global_log_level);
}