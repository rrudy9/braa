#pragma once

#include <array_quick_sort_std.h>
#include <common_project.h>
#include <array_common_std.h>

void naiveSearch(std::vector<int> stdVector, int valToSearch, int& checkSearchedValue, uint64_t& computeVal);
void binarySearch(std::vector<int> stdVector,std::vector<int>::iterator begin, std::vector<int>::iterator ending, int valFoSearch,int& checkSearchedValue, uint64_t& computeVal);

void implementedSearch(std::vector<int> stdVector, int valToSearch);

void naiveSearchLaunch(std::vector<int> stdVector, int valToSearch);
void binarySearchLaunch(std::vector<int> stdVector, int valToSearch);