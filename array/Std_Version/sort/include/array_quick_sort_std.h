#pragma once

#include <array_common_std.h>
#include <common_project.h>

void arrayQuickSortStd(std::vector<int>::iterator starting, std::vector<int>::iterator ending, uint64_t& computeVal);

void implementedQuickSort(std::vector<int> stdVector);

void implementedQuickSortStd(std::vector<int>& stdVector);

void sortStdLib(std::vector<int>& stdVector);
