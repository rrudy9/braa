#pragma once

#include <array_common_std.h>
#include <common_project.h>

void insertionSortStd(std::vector<int>& stdVector, uint64_t& computeVal);

void implementedInsertionSort(std::vector<int> stdVector);