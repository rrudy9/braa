#pragma once

#include <array_common_std.h>
#include <common_project.h>

void arrayMergeSortStd(std::vector<int>::iterator starting, std::vector<int>::iterator ending, uint64_t& computeVal);

void arrayMergeStd(std::vector<int>::iterator starting, std::vector<int>::iterator mid, std::vector<int>::iterator ending, uint64_t& computeVal);

void implementedMergeSort(std::vector<int> stdVector);