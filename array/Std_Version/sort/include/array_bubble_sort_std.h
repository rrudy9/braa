#pragma once

#include <array_common_std.h>
#include <common_project.h>

#ifdef TEST_FOUND 
#include "CppUTest/MemoryLeakDetectorMallocMacros.h"
#endif

void bubbleSortStd(std::vector<int>::iterator starting, std::vector<int>::iterator ending, uint64_t& computeVal);

void implementedBubbleSort(std::vector<int> stdVector);