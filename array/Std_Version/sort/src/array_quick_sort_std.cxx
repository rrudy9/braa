#include "../include/array_quick_sort_std.h"

void arrayQuickSortStd(std::vector<int>::iterator starting, std::vector<int>::iterator ending, uint64_t& computeVal)
{
	if (ending <= starting)
	{
		return;
	}

	std::vector<int>::iterator pivot = starting, q = starting + 1;
	for (auto i = starting + 1; i < ending; ++i)
	{
		++computeVal;
		if (*i < *pivot)
		{
			std::iter_swap(i, q);
			++q;
		}
	}
	std::iter_swap(starting, q - 1);
	arrayQuickSortStd(starting, q - 1, computeVal);
	arrayQuickSortStd(q, ending, computeVal);
}

void implementedQuickSort(std::vector<int> stdVector)
{

	std::vector<int> aCopyFoTestingSortLibStd = {};

	//copy vec to compare time elapse of std sort and our implemented quick sort algorithm 
	aCopyFoTestingSortLibStd = stdVector;

	implementedQuickSortStd(stdVector);

	sortStdLib(aCopyFoTestingSortLibStd);


}

void implementedQuickSortStd(std::vector<int>& stdVector)
{
	uint64_t computeVal = 0;
	size_t timeElapsed = 0;

	if (global_log_level == logLevel::debug)
		showVector(stdVector, "Your Allocated Vector", stdVector.size());

	getTimeLapsComputeVariables(timeElapsed, arrayQuickSortStd, stdVector.begin(), stdVector.end(), computeVal);


	if (global_result_validation)
		checkIfVectorIsSorted(stdVector, "QuickSort");

	if (global_log_level == logLevel::debug)
		showVector(stdVector, "Your Sorted Vector with our implemented Quick Sort (Std)", stdVector.size());

	showComputeElapsedTime(computeVal, timeElapsed, "Quick Sort(Std)", global_show_compute, global_log_level);

}

void sortStdLib(std::vector<int>& stdVector)
{

	size_t timeElapsed = 0;

	if (global_log_level == logLevel::debug)
		showVector(stdVector, "Your Allocated Vector", stdVector.size());

	std::chrono::steady_clock::time_point t1 = std::chrono::high_resolution_clock::now();
	std::sort(stdVector.begin(), stdVector.end());
	std::chrono::steady_clock::time_point t2 = std::chrono::high_resolution_clock::now();

	if (global_log_level == logLevel::debug)
		showVector(stdVector, "Your Sorted Vector with std sort lib", stdVector.size());

	timeElapsed = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();

}