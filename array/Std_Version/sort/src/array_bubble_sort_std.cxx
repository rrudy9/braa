#include "../include/array_bubble_sort_std.h"

void bubbleSortStd(std::vector<int>::iterator starting, std::vector<int>::iterator ending, uint64_t& computeVal)
{

	for (auto i = starting; i != ending; ++i)
	{
		for (auto j = starting; j < i; ++j)
		{
			++computeVal;

			if (*j > *i)
			{
				std::iter_swap(j, i);
			}
		}
	}
}

void implementedBubbleSort(std::vector<int> stdVector)
{

	uint64_t computeVal = 0;
	size_t timeElapsed = 0;

	if (global_log_level == logLevel::debug)
		showVector(stdVector, "Your Allocated Vector", stdVector.size());

	getTimeLapsComputeVariables(timeElapsed, bubbleSortStd, stdVector.begin(), stdVector.end(), computeVal);

	if (global_result_validation)
		checkIfVectorIsSorted(stdVector, "Bubble Sort"); 

	if (global_log_level == logLevel::debug)
		showVector(stdVector, "Your Sorted Vector with Bubble Sort (Std)", stdVector.size());

	showComputeElapsedTime(computeVal, timeElapsed, "Bubble Sort(Std)", global_show_compute, global_log_level);

}