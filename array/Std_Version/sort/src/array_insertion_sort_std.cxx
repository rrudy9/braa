#include "../include/array_insertion_sort_std.h"


void insertionSortStd(std::vector<int>& stdVector, uint64_t& computeVal)
{
	for (std::vector<int>::iterator it = stdVector.begin() + 1; it != stdVector.end(); ++it)
	{
		++computeVal;

		int key = *it;
		auto j = it - 1;
		while ((j > stdVector.begin()) && (*j > key))
		{
			*(j + 1) = *j;
			--j;
		}

		if ((j == stdVector.begin()) && (*j > key))
		{
			*(j + 1) = *j;
			*j = key;
		}
		else if (*j < key)
		{
			*(j + 1) = key;
		}
	}

}

void implementedInsertionSort(std::vector<int> stdVector)
{
	uint64_t computeVal = 0;
	size_t timeElapsed = 0;

	if (global_log_level == logLevel::debug)
		showVector(stdVector, "Your Allocated Vector", stdVector.size());

	getTimeLapsComputeVariables(timeElapsed, insertionSortStd, stdVector, computeVal);

	if (global_result_validation)
		checkIfVectorIsSorted(stdVector,"Insertion Sort");

	if (global_log_level == logLevel::debug)
		showVector(stdVector, "Your Sorted Vector with Insertion Sort (Std)", stdVector.size());

	showComputeElapsedTime(computeVal, timeElapsed, "Insertion Sort(Std)", global_show_compute, global_log_level);

}