#include "../include/array_merge_sort_std.h"

void arrayMergeSortStd(std::vector<int>::iterator starting, std::vector<int>::iterator ending, uint64_t& computeVal)
{

	size_t distance = std::distance(starting, ending);

	//already sorted
	if (distance < 2) {
		return;
	}

	++computeVal;


	std::vector<int>::iterator mid = starting;
	std::advance(mid, distance / 2);
	arrayMergeSortStd(starting, mid, computeVal);
	arrayMergeSortStd(mid, ending, computeVal);
	arrayMergeStd(starting, mid, ending, computeVal);

}


void arrayMergeStd(std::vector<int>::iterator starting, std::vector<int>::iterator mid, std::vector<int>::iterator ending, uint64_t& computeVal)
{

	size_t distance = std::distance(starting, ending);

	std::vector<int> vectorMerge(distance);

	std::vector<int>::iterator left = starting;
	std::vector<int>::iterator left_bound = mid;
	std::vector<int>::iterator right = mid;
	std::vector<int>::iterator right_bound = ending;
	size_t k = 0;
	
	while (left != left_bound && right != right_bound)
	{
		++computeVal;

		if (*right < *left)
		{
			vectorMerge[k++] = *right;
			++right;
		}
		else
		{
			vectorMerge[k++] = *left;
			++left;
		}
	}

	std::copy(left, left_bound, &vectorMerge[k]);
	std::copy(right, right_bound, &vectorMerge[k]);
	std::copy(vectorMerge.begin(), vectorMerge.end(), starting);
}

void implementedMergeSort(std::vector<int> stdVector)
{
	uint64_t computeVal = 0;
	size_t timeElapsed = 0;

	if (global_log_level == logLevel::debug)
		showVector(stdVector, "Your Allocated Vector", stdVector.size());

	getTimeLapsComputeVariables(timeElapsed, arrayMergeSortStd, stdVector.begin(), stdVector.end(), computeVal);

	if (global_result_validation)
		checkIfVectorIsSorted(stdVector,"Merge Sort");

	if (global_log_level == logLevel::debug)
		showVector(stdVector, "Your Sorted Vector with Merge Sort (Std)", stdVector.size());

	showComputeElapsedTime(computeVal, timeElapsed, "Merge Sort (Std)", global_show_compute, global_log_level);

}