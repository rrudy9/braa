#include <stdlib.h>
#include <stdio.h>
#include <common_project.h>
#include <double_linked_list_deletion_c.h>
#include <double_linked_list_common_c.h>
#include <double_linked_list_insertion_c.h>
#include <double_linked_list_search_c.h>
#include <argtable3.h>
#include <vector>

#ifdef TEST_FOUND 
#include "CppUTest/MemoryLeakDetectorMallocMacros.h"
#endif

bool global_show_compute = false;
logLevel global_log_level = logLevel::info;

int main(int argc, char* argv[])
{
	struct arg_lit
		* help = NULL,
		* showList = NULL,
		* showListBackward = NULL,
		* checkResultValidation = NULL,
		* showCompute = NULL;

	struct arg_str
		* logLevelValue = NULL,
		* insertInIndex = NULL;

	struct arg_int
		* setElementCount = NULL,
		* setRandSeedValue = NULL,
		* search = NULL,
		* insertionBack = NULL,
		* deletion = NULL,
		* insertionFront = NULL;


	struct arg_end* end = NULL;

	struct Common_Project_Values doubleListCommonValue = { 100000 , 30 };
	struct double_linked_list* doubleList = NULL;

	void* argtable[] = {
		help = arg_litn(NULL, "help", 0, 1, "display this help for Double Linked list <C Version> and exit"),
		showCompute = arg_litn("c","show-compute",0, 1, "Show Compute"),
		checkResultValidation = arg_litn("t","run-test" ,0, 1, "Run Test for implemented algorithms"),
		logLevelValue = arg_strn("v", "verbose","<info,debug,error,fatal>", 0, 1, "Set Log Level (info, debug, error, fatal) (Default : info)"),
		setElementCount = arg_intn("l", "list-count","<n>" , 0, 1, "Set List Number of element (Default Value: 100k), Doesn't work when n = 0 "),
		setRandSeedValue = arg_intn("r", "rand-seed","<n>", 0, 1, "Set value for Seed Random (Default Value: 30)"),

		search = arg_intn("s", "search","<value>", 0, 1, "Access Double Linked List C Search Algorithm implementation"),
		insertionBack = arg_intn("b", "insertBack", "<value>" ,0, 1, "Access Double Linked List C Insertion Algorithm implementation (Push Back)"),
		insertInIndex = arg_strn("y", "insert-in-index", "<value,index>" , 0, 1, "Access Double Linked List C Insertion Algorithm implementation (Insert at a following index) "),
		insertionFront = arg_intn("f", "insertFront", "<value>" , 0, 1, "Access Double Linked List C Insertion Algorithm implementation (Push Front) "),
		deletion = arg_intn("d", "deletion","<value>", 0, 1, "Access Double Linked List C Deletion Algorithm implementation"),
		showList = arg_litn("z", "show-list", 0, 1, "Show Double Linked List"),
		showListBackward = arg_litn("m", "show-list-backward", 0, 1, "Show Double Linked List Backward"),

		end = arg_end(20)
	};

	int exitcode = 0;
	char progname[] = "doubleListC.exe";

	int nerrors;
	nerrors = arg_parse(argc, argv, argtable);

	if (argc == 1) {
		printf("Please choose one of the available commands\n\n");
		arg_print_glossary(stdout, argtable, "  %-25s %s\n");
		exitcode = 0;
		goto exit;
	}

	//If the parser returned any errors then display them and exit 
	if (nerrors > 0)
	{
		// Display the error details contained in the arg_end struct.
		arg_print_errors(stderr, end, progname);
		fprintf(stderr,"Try '%s --help' for more information.\n", progname);
		exitcode = 1;
		goto exit;
	}


	if (help->count > 0)
	{
		printf("Usage: %s", progname);
		arg_print_syntax(stdout, argtable, "\n\n");
		arg_print_glossary(stdout, argtable, "  %-25s %s\n");
		exitcode = 0;
		goto exit;
	}


	if (showCompute->count > 0)
	{
		global_show_compute = !global_show_compute;
	}


	if (logLevelValue->count > 0)
	{
		setLogLevel(*(logLevelValue)->sval, global_log_level);
	}

	//set project common value (random seed , and number of element in array
	if (setElementCount->count > 0)
	{
		//if (*(setElementCount)->ival == 0) goto exit;

		doubleListCommonValue.elementCount = *(setElementCount)->ival;
	}

	if (setRandSeedValue->count > 0)
	{
		doubleListCommonValue.seedRand = *(setRandSeedValue)->ival;
	}

	srand(doubleListCommonValue.seedRand);
	showCommonProjectValue(doubleListCommonValue, global_log_level);
	createDoubleLinkedList(&doubleList, doubleListCommonValue);


	if (search->count > 0)
	{
		implementedSearch(doubleList, *(search)->ival);
	}

	if (insertionBack->count > 0)
	{
		implementedInsertionBack(&doubleList, *(insertionBack)->ival);
	}

	if (insertInIndex->count > 0)
	{
		
		std::string value = "";
		std::string index = "";
		int checkCommaInString = 0;
		char* valToInsert = (char*)malloc(strlen(*(insertInIndex)->sval) + 1 * sizeof(char));
		if (valToInsert) strcpy(valToInsert, *(insertInIndex)->sval);
		else
		{
			fprintf(stderr, "Can't allocate your array argument for insertion\n");
			exit(0);
		}

		std::string str = "";
		for (int i = 0; i < strlen(valToInsert); ++i)
		{
			str += valToInsert[i];
			if (valToInsert[i] == ',')
			{
				value = str;
				value[value.size() - 1] = '\0';
				str = "";

				str += valToInsert[i + 1];
				index = str;
				//index[index.size() - 1] = '\0';
				break;
			
			}
		}

		int valueInteger = atoi(value.c_str());
		int indexInteger = atoi(index.c_str());
			
		printf("%i, %i \n", valueInteger, indexInteger);

		implementedInsertionInIndex(&doubleList, valueInteger, indexInteger);

		free(valToInsert);
	}

	if (insertionFront->count > 0)
	{
		implementedInsertionFront(&doubleList, *(insertionFront)->ival);
	}

	if (deletion->count > 0)
	{
		implementedDeletion(&doubleList, *(deletion)->ival);
	}

	if (checkResultValidation->count > 0)
	{
		std::vector<int> elementInListToTest = { };

		struct double_linked_list* listChain = doubleList;
		while (listChain)
		{
			elementInListToTest.push_back(listChain->data);
			listChain = listChain->next;
		}

		int indexOfRandomVal = (elementInListToTest.size() - 1) % rand();
		int pickUpRandomVal = elementInListToTest[indexOfRandomVal];
		
		uint64_t cc = 0; 
		if (!searchDoubleLinkedList(doubleList, pickUpRandomVal,cc))
		{
			fprintf(stderr, "Error in search code\n");
			destroyDoubleList(doubleList);
			goto exit; 
		}

		cc = 0; 
		deleteVal(&doubleList, pickUpRandomVal, cc);

		if (searchDoubleLinkedList(doubleList, pickUpRandomVal, cc))
		{
			fprintf(stderr, "Error in deletion code, your random value was found\n");
			destroyDoubleList(doubleList);
			goto exit;
		}

		listChain = doubleList;

		std::vector<int> elementAfterRunningAlgorithm = { };

		while (listChain)
		{
			elementAfterRunningAlgorithm.push_back(listChain->data);
			listChain = listChain->next;
		}

		for (int i = 0; i < elementAfterRunningAlgorithm.size(); ++i)
		{
			if (i < indexOfRandomVal)
			{
				if (elementAfterRunningAlgorithm[i] != elementInListToTest[i])
				{
					fprintf(stderr, "Error in deletion code, some values are not the same\n");
					destroyDoubleList(doubleList);
					goto exit;
				}
			}
			else
			{
				if (elementAfterRunningAlgorithm[i] != elementInListToTest[i + 1])
				{
					fprintf(stderr, "Error in deletion code, some values are not the same\n");
					destroyDoubleList(doubleList);
					goto exit;
				}
			}
		}

		cc = 0; 
		insertValInIndex(&doubleList, pickUpRandomVal, indexOfRandomVal, cc);
		
		elementAfterRunningAlgorithm = { };

		while (listChain)
		{
			elementAfterRunningAlgorithm.push_back(listChain->data);
			listChain = listChain->next;
		}

		for (int i = 0; i < elementAfterRunningAlgorithm.size(); ++i)
		{
			if (elementAfterRunningAlgorithm[i] != elementInListToTest[i])
			{
				fprintf(stderr, "Error in insertion code, some values are not the same\n");
				destroyDoubleList(doubleList);
				goto exit;
			}
		}

		printf("Test Succeeded for algorithms \n");
	}

	if (showList->count > 0)
	{
	
		showDoubleLinkedList(doubleList, "Your Double Linked List");
	}

	if (showListBackward->count > 0)
	{
		showDoubleLinkedListBackward(doubleList, "Your Double Linked List (Backward)");
	}


	arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
	destroyDoubleList(doubleList);

	return exitcode;

exit:
	/* deallocate each non-null entry in argtable[] */
	arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
	return exitcode;
}