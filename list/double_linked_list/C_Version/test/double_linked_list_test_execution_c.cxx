#include "CppUTest/TestHarness.h"
#include "CppUTest/MemoryLeakDetectorMallocMacros.h"

#include <common_project.h>
#include <double_linked_list_deletion_c.h>
#include <double_linked_list_common_c.h>
#include <double_linked_list_insertion_c.h>
#include <double_linked_list_search_c.h>


int getCount(struct double_linked_list* list)
{
	int count = 0;
	while (list != NULL)
	{
		++count;
		list = list->next;
	}
	return count;
}

TEST_GROUP(TEST_DOUBLE_LINKED_LIST_C)
{
	struct Common_Project_Values listCommonValue = { global_test_count_element, global_test_rand_seed };
	struct double_linked_list* doubleList = NULL;

	void setup()
	{
		showCommonProjectValue(listCommonValue, global_log_level);
		createDoubleLinkedList(&doubleList, listCommonValue);
	}

	void teardown()
	{
		destroyDoubleList(doubleList);
	}
};

TEST(TEST_DOUBLE_LINKED_LIST_C, search)
{
	int valToSearch = rand() % 1001;

	if (global_log_level <= logLevel::debug)
		printf("val to search %i\n", valToSearch);

	implementedSearch(doubleList, valToSearch);

	FAIL("force failing for search on purpose");

}


TEST(TEST_DOUBLE_LINKED_LIST_C, insertionBack)
{

	int valToInsert = rand() % 10101;

	if (global_log_level <= logLevel::debug)
		printf("val to insert (push back) %i\n", valToInsert);

	implementedInsertionBack(&doubleList, valToInsert);
	showDoubleLinkedList(doubleList, "after pushing back");


	int checkCount = getCount(doubleList);

	CHECK(checkCount > listCommonValue.elementCount);

}


TEST(TEST_DOUBLE_LINKED_LIST_C, insertionFront)
{
	int valToInsert = rand() % 10111 + 1;

	if (global_log_level <= logLevel::debug)
		printf("val to insert (push back) %i\n", valToInsert);

	implementedInsertionFront(&doubleList, valToInsert);
	showDoubleLinkedList(doubleList, "after pushing front");

	int checkCount = getCount(doubleList);

	CHECK(checkCount >= listCommonValue.elementCount);
}

TEST(TEST_DOUBLE_LINKED_LIST_C, deletion)
{

	int valToDelete = rand() % 1011 + 1;

	if (global_log_level <= logLevel::debug)
		printf("val to delete %i\n", valToDelete);

	implementedDeletion(&doubleList, valToDelete);

	showDoubleLinkedList(doubleList, "after deletion");

	int checkCount = getCount(doubleList);

	CHECK(checkCount < listCommonValue.elementCount);

}
