#pragma once

#include <double_linked_list_common_c.h>
#include <common_project.h>

#ifdef TEST_FOUND 
#include "CppUTest/MemoryLeakDetectorMallocMacros.h"
#endif


void deleteVal(struct double_linked_list** list, int value, uint64_t& computeVal);


void implementedDeletion(struct double_linked_list** list, int valToDelete);

