#include "../include/double_linked_list_deletion_c.h"


void deleteVal(struct double_linked_list** list, int value, uint64_t& computeVal)
{
	struct double_linked_list* nodeToBeDeleted = *list ,* prev =  NULL;

	++computeVal;
	
	if ((*list)->data == value)
	{
		(*list) = (*list)->next;
		free(nodeToBeDeleted);
		printf("value: %i was successfully deleted\n",value);
		return; 
	}

	while (nodeToBeDeleted != NULL && nodeToBeDeleted->data != value)
	{
		nodeToBeDeleted = nodeToBeDeleted->next;
	}

	if (nodeToBeDeleted == NULL)
	{
		printf("can't find your value, %i\n", value);
		return;
	}

	nodeToBeDeleted->prev->next = nodeToBeDeleted->next;

	free(nodeToBeDeleted);

}



void implementedDeletion(struct double_linked_list** list, int valToDelete)
{
	uint64_t computeVal = 0;
	size_t timeElapsed = 0;
	
	getTimeLapsComputeVariables(timeElapsed, deleteVal, list, valToDelete, computeVal);
	
	if (global_log_level == logLevel::debug && list !=NULL)
		showDoubleLinkedList(*list, "\nafter remove selected value from list");

	showComputeElapsedTime(computeVal, timeElapsed, "Linked List Deletion", global_show_compute, global_log_level);
}

