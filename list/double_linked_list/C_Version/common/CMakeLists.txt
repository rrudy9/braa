file(GLOB double_linked_list_common_c 
	.cxx .hxx 
	src/*.cxx 
	include/*.h)

add_library(double_linked_list_common_c_lib ${double_linked_list_common_c})


set(linkingLibrary 
	${CPPUTEST_LDFLAGS}  
	common_project_lib
	double_linked_list_insertion_c_lib

	)

set(linkingDirectories 
	"${CPPUTEST_INCLUDE_DIRS}"
	"${PROJECT_SOURCE_DIR}/common_project/include" 
	"${PROJECT_SOURCE_DIR}/list/double_linked_list/C_Version/insertion/include" 
	)

	
if (CPPUTEST_LIBRARIES)
	add_compile_definitions(TEST_FOUND="test")
endif(CPPUTEST_LIBRARIES)




target_link_libraries(double_linked_list_common_c_lib PRIVATE ${linkingLibrary})


target_include_directories(double_linked_list_common_c_lib PRIVATE ${linkingDirectories})

set_property(TARGET double_linked_list_common_c_lib PROPERTY FOLDER "list/double_linked_list/C_Version/common")
