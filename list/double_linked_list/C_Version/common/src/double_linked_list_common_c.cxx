#include "../include/double_linked_list_common_c.h"
#include <double_linked_list_insertion_c.h>

void createDoubleLinkedList(struct double_linked_list** list, struct Common_Project_Values projectVal)
{
	for (int i = 0; i < projectVal.elementCount; ++i)
	{
		int randVal = rand() % 10001;
		uint64_t cc = 0; 
		pushBack(list, randVal, cc);
	}

}


int isListEmpty(struct double_linked_list* list)
{
	if (list == NULL)
		return 1; 

	return 0;
}

void destroyDoubleList(struct double_linked_list* list)
{
	while (list)
	{
		struct double_linked_list* nodeToDelete = list;
		list = list->next;
		free(nodeToDelete);
	}
}

void showDoubleLinkedList(struct double_linked_list* list, char* hint)
{
	printf("%s\n", hint);

	printf("[ ");
	while (list != NULL)
	{
		if (list->next != NULL) printf("%i,", list->data);
		else printf("%i ]", list->data);
		list = list->next;
	}
	
	printf("\n"); 
}

void showDoubleLinkedListBackward(struct double_linked_list* list, char* hint)
{
	struct double_linked_list* tail = list;

	while (tail->next != NULL) {
		tail = tail->next;
	}
	printf("%s\n", hint);
	
	printf("[ ");
	while (tail != NULL)
	{
		if (tail->prev != NULL) printf("%i,", tail->data);
		else printf("%i ]", tail->data);
		tail = tail->prev;
	}
	printf("\n");
}

bool getTimeLapsComputeVariables(size_t& elapsedTime, bool (*f)(struct double_linked_list**, int, uint64_t&), struct double_linked_list** list, int value, uint64_t& computeVal)
{
	bool valueFound = false;

	std::chrono::steady_clock::time_point t1 = std::chrono::high_resolution_clock::now();
	valueFound = f(list, value, computeVal);
	std::chrono::steady_clock::time_point t2 = std::chrono::high_resolution_clock::now();

	elapsedTime = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();

	return valueFound;
}

bool getTimeLapsComputeVariables(size_t& elapsedTime, bool (*f)(struct double_linked_list*, int, uint64_t&), struct double_linked_list* list, int value, uint64_t& computeVal)
{
	bool valueFound = false;

	std::chrono::steady_clock::time_point t1 = std::chrono::high_resolution_clock::now();
	valueFound = f(list, value, computeVal);
	std::chrono::steady_clock::time_point t2 = std::chrono::high_resolution_clock::now();

	elapsedTime = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();

	return valueFound;
}

void getTimeLapsComputeVariables(size_t& elapsedTime, void (*f)(struct double_linked_list**, int, uint64_t&), struct double_linked_list** list, int value, uint64_t& computeVal)
{

	std::chrono::steady_clock::time_point t1 = std::chrono::high_resolution_clock::now();
	f(list, value, computeVal);
	std::chrono::steady_clock::time_point t2 = std::chrono::high_resolution_clock::now();

	elapsedTime = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();
}

int getTimeLapsComputeVariables(size_t& elapsedTime, int (*f)(struct double_linked_list*, int, uint64_t&), struct double_linked_list* list, int value, uint64_t& computeVal)
{
	int found = 0; 
	std::chrono::steady_clock::time_point t1 = std::chrono::high_resolution_clock::now();
	found = f(list, value, computeVal);
	std::chrono::steady_clock::time_point t2 = std::chrono::high_resolution_clock::now();

	elapsedTime = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();
	
	return found; 
}


int getCountForValidation(struct double_linked_list* list)
{
	int count = 0;
	while (list != NULL)
	{
		++count;
		list = list->next;
	}
	return count;
}
