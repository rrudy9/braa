#ifndef DOUBLE_LINKED_LIST_COMMON_C
#define DOUBLE_LINKED_LIST_COMMON_C

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <ctime>
#include <algorithm>
#include <math.h>
#include <chrono>

#include <common_project.h>

#ifdef TEST_FOUND 
#include "CppUTest/MemoryLeakDetectorMallocMacros.h"
#endif


struct double_linked_list
{
	int data;
	struct double_linked_list* prev;
	struct double_linked_list* next;
};

void createDoubleLinkedList(struct double_linked_list** list, struct Common_Project_Values projectVal);

void destroyDoubleList(struct double_linked_list* list);

int isListEmpty(struct double_linked_list* list);

void showDoubleLinkedList(struct double_linked_list* list, char* hint);
void showDoubleLinkedListBackward(struct double_linked_list* list, char* hint);

bool getTimeLapsComputeVariables(size_t& elapsedTime, bool (*f)(struct double_linked_list**, int, uint64_t&), struct double_linked_list** list, int value, uint64_t& computeVal);
bool getTimeLapsComputeVariables(size_t& elapsedTime, bool (*f)(struct double_linked_list*, int, uint64_t&), struct double_linked_list* list, int value, uint64_t& computeVal);
void getTimeLapsComputeVariables(size_t& elapsedTime, void (*f)(struct double_linked_list**, int, uint64_t&), struct double_linked_list** list, int value, uint64_t& computeVal);
int  getTimeLapsComputeVariables(size_t& elapsedTime, int  (*f)(struct double_linked_list*, int, uint64_t&), struct double_linked_list* list, int value, uint64_t& computeVal);

int getCountForValidation(struct double_linked_list* list);


#endif