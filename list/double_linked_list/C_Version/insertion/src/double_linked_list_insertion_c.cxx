#include "../include/double_linked_list_insertion_c.h"
#include <double_linked_list_common_c.h>

void pushBack(struct double_linked_list** list, int value, uint64_t& computeVal)
{
	struct double_linked_list* newNode = (struct double_linked_list*)malloc(sizeof(struct double_linked_list));
	
	if (newNode == NULL)
	{
		fprintf(stderr, "can't allocate your new node\n");
		exit(1);
	}

	newNode->data = value;
	newNode->next = NULL;
	newNode->prev = NULL;

	if (isListEmpty(*list))
	{
		*list = newNode; 
		return; 
	}
	struct double_linked_list* prev = NULL; 
	struct double_linked_list* addedNode = *list;
	while (addedNode != NULL)
	{
		prev = addedNode;
		addedNode = addedNode->next;

	}
	addedNode = newNode;
	addedNode->prev = prev;
	prev->next = addedNode;
}


void pushFrontNode(struct double_linked_list** list, int value, uint64_t& computeVal)
{
	struct double_linked_list* newNode = (struct double_linked_list*)malloc(sizeof(struct double_linked_list));

	if (newNode == NULL)
	{
		fprintf(stderr, "can't allocate your new node\n");
		exit(1);
	}

	newNode->data = value;
	newNode->next = NULL;
	newNode->prev = NULL;

	if ((*list) != NULL)
	{
		++computeVal;

		newNode->next = (*list);
		(*list)->prev = newNode;
	}

	(*list) = newNode;

}

void insertValInIndex(struct double_linked_list** list, int value, int index, uint64_t computeVal)
{
	int i = 1; 

	struct double_linked_list* listChain = *list; 

	struct double_linked_list* newNode = (struct double_linked_list*)malloc(sizeof(struct double_linked_list));

	newNode->data = value;
	newNode->next = NULL;
	newNode->prev = NULL;

	++computeVal;
	if (!(*list))
	{
		*list = newNode; 
		return; 
	}
	
	struct double_linked_list* prevChain = NULL; 

	while (listChain)
	{
		++computeVal;
		if (i == index)
		{
			prevChain->next = newNode; 
			newNode->prev = prevChain;
			newNode->next = listChain;
			listChain->prev = newNode;
		}

		prevChain = listChain; 
		listChain = listChain->next;
		++i; 
	}
}



void implementedInsertionBack(struct double_linked_list** list, int valToInsert)
{
	uint64_t computeVal = 0;
	size_t timeElapsed = 0;

	if (global_log_level == logLevel::debug)
		showDoubleLinkedList(*list, "Your Allocated Double linked list");

	getTimeLapsComputeVariables(timeElapsed, pushBack, list, valToInsert, computeVal);

	if (global_log_level == logLevel::debug)
		showDoubleLinkedList(*list, "Your Double linked list after pushing back value");

	showComputeElapsedTime(computeVal, timeElapsed, "Pushing back Double Linked List",global_show_compute, global_log_level);


}

void implementedInsertionFront(struct double_linked_list** list, int valToInsert)
{
	uint64_t computeVal = 0;
	size_t timeElapsed = 0;

	if (global_log_level == logLevel::debug)
		showDoubleLinkedList(*list, "Your Allocated Double linked list");

	getTimeLapsComputeVariables(timeElapsed, pushFrontNode, list, valToInsert, computeVal);

	if (global_log_level == logLevel::debug)
		showDoubleLinkedList(*list, "Your Double linked list after pushing front value");

	showComputeElapsedTime(computeVal, timeElapsed, "Pushing front Double Linked List ",global_show_compute, global_log_level);

}

void implementedInsertionInIndex(struct double_linked_list** list, int valToInsert, int index)
{
	uint64_t computeVal = 0;
	size_t timeElapsed = 0;


	std::chrono::steady_clock::time_point t1 = std::chrono::high_resolution_clock::now();
	insertValInIndex(list, valToInsert, index, computeVal);
	std::chrono::steady_clock::time_point t2 = std::chrono::high_resolution_clock::now();

	timeElapsed = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();

	showComputeElapsedTime(computeVal, timeElapsed, "Insertion In index Double Linked List ", global_show_compute, global_log_level);


}
