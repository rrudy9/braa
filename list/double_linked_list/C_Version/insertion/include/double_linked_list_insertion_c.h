#pragma once

#include <common_project.h>

#ifdef TEST_FOUND 
#include "CppUTest/MemoryLeakDetectorMallocMacros.h"
#endif

struct double_linked_list;

enum class insertionChoice { none, pushBack, pushFront };


void pushBack(struct double_linked_list** list, int value, uint64_t& computeVal);

void pushFrontNode(struct double_linked_list** list, int value, uint64_t& computeVal);

void insertValInIndex(struct double_linked_list** list, int value, int index, uint64_t computeVal);

void implementedInsertionBack(struct double_linked_list** list, int valToInsert);

void implementedInsertionFront(struct double_linked_list** list, int valToInsert);

void implementedInsertionInIndex(struct double_linked_list** list, int valToInsert, int index); 
