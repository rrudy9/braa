#include "../include/double_linked_list_search_c.h"

int searchDoubleLinkedList(struct double_linked_list* list, int value, uint64_t& computeVal)
{
	int index = 0;

	int found = 0; 

	while (list != NULL)
	{
		++computeVal;
		++index;
		if (list->data == value)
		{
			printf("\nvalue : %d found at position %d\n", value, index);
			
			
			if (found == 0) ++ found; 
			//we comment this to find all the value of searched item 
			//return;
		}

		if (list->next != NULL)
		{
			list = list->next;
		}
		else
			break;
	}

	// we can return an array of value later, but just for test .. 
	return found; 

	//if (!found && global_log_level <= logLevel::debug)
		//printf("\nwe're sorry we can't find your value < %i > in the list\n", value);


}



void implementedSearch(struct double_linked_list* list, int valToSearch)
{
	uint64_t computeVal = 0;
	size_t timeElapsed = 0;

	if (global_log_level == logLevel::debug)
		showDoubleLinkedList(list, "Your Allocated Double linked list");
	
	if (!getTimeLapsComputeVariables(timeElapsed, searchDoubleLinkedList, list, valToSearch, computeVal))
	{
		printf("\nwe're sorry we can't find your value < %i > in the list\n", valToSearch);
	}

	showComputeElapsedTime(computeVal, timeElapsed, "Linked List Search", global_show_compute, global_log_level);

}