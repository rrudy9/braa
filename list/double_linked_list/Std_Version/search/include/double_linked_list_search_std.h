#pragma once

#include <double_linked_list_common_std.h>
#include <common_project.h>

#ifdef TEST_FOUND 
#include "CppUTest/MemoryLeakDetectorMallocMacros.h"
#endif


void searchList(std::list<int>& list, int value, uint64_t& computeVal);

void implementedSearch(std::list<int>& list, int valToSearch);


