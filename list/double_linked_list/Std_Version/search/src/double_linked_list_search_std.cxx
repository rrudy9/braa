#include "../include/double_linked_list_search_std.h"

void searchList(std::list<int>& list, int value, uint64_t& computeVal)
{
	int index = 0;
	int found = 0;

	for (auto it = list.begin(); it != list.end(); ++it)
	{
		++computeVal;

		++index;
		if (*it == value)
		{
			if (found == 0) ++found;

			printf("\nyour value < %i > is found in the list at position: %i \n", value, index);
			//return;
		}
	}

	if (!found && global_log_level <= logLevel::debug)
		printf("\nwe can't find your value %i in the list\n", value);
}


void implementedSearch(std::list<int>& list, int valToSearch)
{
	if (global_log_level == logLevel::debug)
		printlist(list, "Your Std list");

	uint64_t computeVal = 0;
	size_t timeElapsed = 0;

	getTimeLapsComputeVariables(timeElapsed, searchList, list, valToSearch, computeVal);

	showComputeElapsedTime(computeVal, timeElapsed, "Std List Search", global_show_compute, global_log_level);
}