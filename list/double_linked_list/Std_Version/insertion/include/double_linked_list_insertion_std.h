#pragma once

#include <double_linked_list_common_std.h>
#include <common_project.h>

#ifdef TEST_FOUND 
#include "CppUTest/MemoryLeakDetectorMallocMacros.h"
#endif

enum class insertionChoice { none, pushBack, pushFront };


void pushBackStd(std::list<int>& fL, int value, uint64_t& computeVal);

void pushFrontStd(std::list<int>& fL, int value, uint64_t& computeVal);

void implementedInsertionBack(std::list<int>& list, int valToInsert);

void implementedInsertionFront(std::list<int>& list, int valToInsert);
