#include "../include/double_linked_list_insertion_std.h"


void pushBackNode(std::list<int>& fL, int value)
{
	fL.push_back(value);

}

void pushFrontNode(std::list<int>& fL, int value)
{
	fL.push_front(value);

}


void implementedInsertionBack(std::list<int>& list, int valToInsert)
{
	uint64_t computeVal = 0;
	size_t timeElapsed = 0;

	if (global_log_level == logLevel::debug)
		printlist(list, "Your Std list");

	int countBeforeInsertion = 0;
	if (global_result_validation)
		countBeforeInsertion = list.size();

	getTimeLapsComputeVariables(timeElapsed, pushBackNode, list, valToInsert);

	if (global_result_validation)
	{
		int countAfterInsertion = list.size();
		if (countAfterInsertion > countBeforeInsertion) printf("Testing Result : Insertion Back Validation\n");
		else printf("error in insertion algorithm\n");
	}

	if (global_log_level == logLevel::debug)
		printlist(list, "Your Std list after pushing back");

	showComputeElapsedTime(computeVal, timeElapsed, "Pushing back std List", global_show_compute, global_log_level);

}

void implementedInsertionFront(std::list<int>& list, int valToInsert)
{
	uint64_t computeVal = 0;
	size_t timeElapsed = 0;

	if (global_log_level == logLevel::debug)
		printlist(list, "Your Std list");

	int countBeforeInsertion = 0;
	if (global_result_validation)
		countBeforeInsertion = list.size();

	getTimeLapsComputeVariables(timeElapsed, pushFrontNode, list, valToInsert);

	if (global_result_validation)
	{
		int countAfterInsertion = list.size();
		if (countAfterInsertion > countBeforeInsertion) printf("Testing Result : Insertion Front Validation\n");
		else printf("error in insertion algorithm\n");
	}


	if (global_log_level == logLevel::debug)
		printlist(list, "Your Std list after pushing front");

	showComputeElapsedTime(computeVal, timeElapsed, "Pushing front std List ", global_show_compute, global_log_level);

}


