#pragma once

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <ctime>
#include <algorithm>
#include <math.h>
#include <chrono>
#include <list>
#include <common_project.h>

#ifdef TEST_FOUND 
#include "CppUTest/MemoryLeakDetectorMallocMacros.h"
#endif


void createList(std::list<int>& list, struct Common_Project_Values projectVal);

void printlist(std::list<int> list, char* hint);

bool getTimeLapsComputeVariables(size_t& elapsedTime, bool (*f)(std::list<int>&, int, uint64_t&), std::list<int>& list, int value, uint64_t& computeVal);
void getTimeLapsComputeVariables(size_t& elapsedTime, void (*f)(std::list<int>&, int), std::list<int>& list, int value);
void getTimeLapsComputeVariables(size_t& elapsedTime, void (*f)(std::list<int>&, int, uint64_t&), std::list<int>& list, int value, uint64_t& computeVal);

