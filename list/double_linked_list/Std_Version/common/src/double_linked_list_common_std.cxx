#include "../include/double_linked_list_common_std.h"


void createList(std::list<int>& list, struct Common_Project_Values projectVal)
{
	list = std::list<int>(projectVal.elementCount);

	for (auto it = list.begin(); it != list.end(); ++it)
	{
		int randVal = rand() % 10001;
		*it = randVal;
	}

}

void printlist(std::list<int> list, char* hint)
{
	printf("%s\n", hint);
	for (auto it = list.begin(); it != list.end(); ++it)
	{
		if (it == list.begin()) printf("[ ");

		if (std::distance(list.begin(), it) == list.size() - 1)
		{
			printf("%i", *it);
			printf(" ]\n");
		}
		else
		{
			printf("%i,", *it);
		}
	}
}

bool getTimeLapsComputeVariables(size_t& elapsedTime, bool (*f)(std::list<int>&, int, uint64_t&), std::list<int>& list, int value, uint64_t& computeVal)
{
	bool valueFound = false;

	std::chrono::steady_clock::time_point t1 = std::chrono::high_resolution_clock::now();
	valueFound = f(list, value, computeVal);
	std::chrono::steady_clock::time_point t2 = std::chrono::high_resolution_clock::now();

	elapsedTime = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();

	return valueFound;
}


void getTimeLapsComputeVariables(size_t& elapsedTime, void (*f)(std::list<int>&, int), std::list<int>& list, int value)
{
	std::chrono::steady_clock::time_point t1 = std::chrono::high_resolution_clock::now();
	f(list, value);
	std::chrono::steady_clock::time_point t2 = std::chrono::high_resolution_clock::now();

	elapsedTime = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();
}

void getTimeLapsComputeVariables(size_t& elapsedTime, void (*f)(std::list<int>&, int, uint64_t&), std::list<int>& list, int value, uint64_t& computeVal)
{
	std::chrono::steady_clock::time_point t1 = std::chrono::high_resolution_clock::now();
	f(list, value, computeVal);
	std::chrono::steady_clock::time_point t2 = std::chrono::high_resolution_clock::now();

	elapsedTime = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();
}

