#include "CppUTest/TestHarness.h"
#include "CppUTest/MemoryLeakDetectorMallocMacros.h"

#include <common_project.h>
#include <double_linked_list_deletion_std.h>
#include <double_linked_list_common_std.h>
#include <double_linked_list_insertion_std.h>
#include <double_linked_list_search_std.h>


TEST_GROUP(TEST_DOUBLE_LINKED_LIST_STD)
{
	struct Common_Project_Values doubleListCommonValue = { global_test_count_element, global_test_rand_seed };
	std::list<int> stdList = {};

	void setup()
	{
		showCommonProjectValue(doubleListCommonValue, global_log_level);
		createList(stdList, doubleListCommonValue);
	}

	void teardown()
	{
	}
};



TEST(TEST_DOUBLE_LINKED_LIST_STD, search)
{
	int valToSearch = rand() % 101 + 1;

	if (global_log_level <= logLevel::debug)
		printf("val to search %i\n", valToSearch);

	implementedSearch(stdList, valToSearch);

	FAIL("force failing for search on purpose");

}


TEST(TEST_DOUBLE_LINKED_LIST_STD, insertionBack)
{

	int valToInsert = rand() % 1001 + 1;

	if (global_log_level <= logLevel::debug)
		printf("val to insert (push back) %i\n", valToInsert);

	implementedInsertionBack(stdList, valToInsert);


	CHECK(stdList.size() >= doubleListCommonValue.elementCount);

}


TEST(TEST_DOUBLE_LINKED_LIST_STD, insertionFront)
{
	int valToInsert = rand() % 10101 + 1;

	if (global_log_level <= logLevel::debug)
		printf("val to insert (push back) %i\n", valToInsert);

	implementedInsertionFront(stdList, valToInsert);

	CHECK(stdList.size() >= doubleListCommonValue.elementCount);
}

TEST(TEST_DOUBLE_LINKED_LIST_STD, deletion)
{

	int valToDelete = rand() % 10011 + 1;

	if (global_log_level <= logLevel::debug)
		printf("val to delete %i\n", valToDelete);

	implementedDeletion(stdList, valToDelete);

	CHECK(stdList.size() < doubleListCommonValue.elementCount);
}
