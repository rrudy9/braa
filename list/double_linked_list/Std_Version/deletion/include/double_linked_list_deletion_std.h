#pragma once

#include <double_linked_list_common_std.h>
#include <common_project.h>

#ifdef TEST_FOUND 
#include "CppUTest/MemoryLeakDetectorMallocMacros.h"
#endif


bool deletionList(std::list<int>& list, int value, uint64_t& computeVal);

void implementedDeletion(std::list<int>& list, int valToDelete);

