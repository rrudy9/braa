#include "../include/double_linked_list_deletion_std.h"



bool deletionList(std::list<int>& list, int value, uint64_t& computeVal)
{
	
	for (auto it = list.begin(); it != list.end(); ++it)
	{
		++computeVal;

		if (*it == value)
		{

			list.erase(it);
			return true;
		}
	}

	return false;
}


void implementedDeletion(std::list<int>& list, int valToDelete)
{
	uint64_t computeVal = 0;
	size_t timeElapsed = 0;

	if (global_log_level == logLevel::debug)
		printlist(list, "Your Std list");

	
	int countBeforeDeletion = 0;
	if (global_result_validation)
		countBeforeDeletion = list.size();

	if (getTimeLapsComputeVariables(timeElapsed, deletionList, list, valToDelete, computeVal))
	{
		if (global_log_level <= logLevel::debug)
			printf("value %i was deleted successfully\n", valToDelete);

		if (global_log_level == logLevel::debug && !list.empty())
			printlist(list, "\nafter remove selected value from list");


		if (global_result_validation)
		{
			int countAfterDeletion = list.size();
			if (countAfterDeletion < countBeforeDeletion) printf("Testing Result : Deletion Validation\n");
			else printf("error in deletion algorithm\n");
		}
	}
	else
	{
		if (global_log_level <= logLevel::debug)
			printf("value %i was not found\n", valToDelete);
	}

	showComputeElapsedTime(computeVal, timeElapsed, "Linked List Deletion", global_show_compute, global_log_level);

}