#pragma once

#include <linked_list_common_c.h>
#include <common_project.h>

#ifdef TEST_FOUND 
#include "CppUTest/MemoryLeakDetectorMallocMacros.h"
#endif

int searchLinkedList(struct linked_list* list, int value, uint64_t& computeVal);

void implementedSearch(struct linked_list* list, int valToSearch);
