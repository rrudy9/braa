#include "../include/linked_list_search_c.h"

int searchLinkedList(struct linked_list* list, int value, uint64_t& computeVal)
{
	int valueWasFound = 0; 
	while (list != NULL)
	{
		++computeVal;
		if (list->data == value)
		{
			printf("value: %i was found\n", value);
			++valueWasFound;
		}
		list = list->next;
	}

	return valueWasFound;
}

void implementedSearch(struct linked_list* list, int valToSearch)
{
	uint64_t computeVal = 0;
	size_t timeElapsed = 0;


	if (global_log_level == logLevel::debug)
		showLinkedList(list, "Your Allocated linked list");

	if (!getTimeLapsComputeVariables(timeElapsed, searchLinkedList, list, valToSearch, computeVal))
	{
		printf("Value: %i was not found in linked list\n", valToSearch);
	}

	showComputeElapsedTime(computeVal, timeElapsed, "Linked List Deletion", global_show_compute, global_log_level);
}