#pragma once

#include <common_project.h>

struct linked_list;

#ifdef TEST_FOUND 
#include "CppUTest/MemoryLeakDetectorMallocMacros.h"
#endif

void pushBack(struct linked_list** list, int value, uint64_t& computeVal);

void insertValInIndex(struct linked_list** list, int value,int index, uint64_t& computeVal);

void implementedInsertion(struct linked_list* list, int valToInsert);
