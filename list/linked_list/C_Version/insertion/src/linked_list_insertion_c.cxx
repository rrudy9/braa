#include "../include/linked_list_insertion_c.h"
#include <linked_list_common_c.h>


void pushBack(struct linked_list** list, int value, uint64_t& computeVal)
{
	struct linked_list* newNode = (struct linked_list*)malloc(sizeof(struct linked_list));
	if (!newNode)
	{ 
		fprintf(stderr, "Can't allocate your node\n");
		exit(0);
	}

	newNode->data = value;
	newNode->next = NULL;


	if (isListEmpty(*list))
	{
		*list = newNode;
		return;
	}

	struct linked_list* addNode = *list; 

	while (addNode->next !=NULL)
	{
		addNode = addNode->next;
		++computeVal;
	}

	addNode->next = newNode; 
}

void insertValInIndex(struct linked_list** list, int value, int index, uint64_t& computeVal)
{
	int i = 1; 

	struct linked_list* newNode = (struct linked_list*)malloc(sizeof(struct linked_list));
	if (!newNode)
	{
		fprintf(stderr, "Can't allocate your node\n");
		exit(0);
	}

	newNode->data = value;
	newNode->next = NULL;

	if (!*list)
	{
		*list = newNode;
		return; 
	}

	struct linked_list* listChain = *list;
	struct linked_list* prev = NULL; 

	while (listChain)
	{
		if (i == index)
		{
			prev->next = newNode; 
			newNode->next = listChain; 
		}
		prev = listChain; 
		listChain = listChain->next; 
	}

}


void implementedInsertion(struct linked_list* list, int valToInsert)
{
	size_t timeElasped = 0;
	uint64_t computeVal = 0;

	if (global_log_level == logLevel::debug)
		showLinkedList(list, "Your Allocated linked list before insertion");

	getTimeLapsComputeVariables(timeElasped, pushBack, &list, valToInsert, computeVal);
	
	if (global_log_level == logLevel::debug)
		showLinkedList(list, "Your Allocated linked list after insertion");
	
	showComputeElapsedTime(computeVal, timeElasped, "Linked List Insertion", global_show_compute, global_log_level);
}