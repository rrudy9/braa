#ifndef LINKED_LIST_COMMON_C
#define LINKED_LIST_COMMON_C

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <ctime>
#include <algorithm>
#include <math.h>
#include <chrono>

#include <common_project.h>

#ifdef TEST_FOUND 
#include "CppUTest/MemoryLeakDetectorMallocMacros.h"
#endif


struct linked_list
{
	int data;
	struct linked_list* next;
};


void initializeList(struct linked_list** list);
void createLinkedList(struct linked_list** list, struct Common_Project_Values projectVal);
void showLinkedList(struct linked_list* list, char* hint);

int isListEmpty(struct linked_list* list);

void destroyList(struct linked_list* list);

int getTimeLapsComputeVariables(size_t& elapsedTime,  int (*f)(struct linked_list*, int, uint64_t&), struct linked_list* list, int value, uint64_t& computeVal);
void getTimeLapsComputeVariables(size_t& elapsedTime, void (*f)(struct linked_list**, uint64_t&), struct linked_list** list, uint64_t& computeVal);
void getTimeLapsComputeVariables(size_t& elapsedTime, void (*f)(struct linked_list**,int, uint64_t&), struct linked_list** list,int value, uint64_t& computeVal);

int getCountForValidation(struct linked_list* list);

#endif

