#include "../include/linked_list_common_c.h"
#include <linked_list_insertion_c.h>


void initializeList(struct linked_list** list)
{
	*list = (struct linked_list*)malloc(sizeof(struct linked_list));

	if (*list == NULL)
	{
		fprintf(stderr, "can't allocate memory for linked list\n");
		exit(1);
	}

	(*list)->data = 0;
	(*list)->next = NULL;

}


void createLinkedList(struct linked_list** list, struct Common_Project_Values projectVal)
{
	//initializeList(list);
	uint64_t cc = 0; 
	//*list = (struct linked_list*)malloc(sizeof(struct linked_list));

	for (int i = 0; i < projectVal.elementCount; ++i)
	{
		int randVal = rand() % 10001;
		pushBack(list, randVal, cc);
	
	}
}

int isListEmpty(struct linked_list* list)
{
	if (list == NULL)
		return 1; 
	
	return 0;
}

void showLinkedList(struct linked_list* list, char* hint)
{
	
	printf("%s\n", hint);

	printf("[ ");
	while (list != NULL)
	{
		if (list->next != NULL) printf("%i,", list->data);
		else printf("%i ]", list->data);
		list = list->next;
	
	}
	
	printf("\n"); 
}


void destroyList(struct linked_list* list)
{
	while (list != NULL)
	{
		struct linked_list* itemToDelete = list;

		list = list->next;
		free(itemToDelete);
	}

}

int getTimeLapsComputeVariables(size_t& elapsedTime, int (*f)(struct linked_list*, int, uint64_t&), struct linked_list* list, int value, uint64_t& computeVal)
{
	int valFound = 0; 
	std::chrono::steady_clock::time_point t1 = std::chrono::high_resolution_clock::now();
	valFound = f(list,value, computeVal);
	std::chrono::steady_clock::time_point t2 = std::chrono::high_resolution_clock::now();

	elapsedTime = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();
	
	return valFound; 
}

void getTimeLapsComputeVariables(size_t& elapsedTime, void (*f)(struct linked_list**, uint64_t&), struct linked_list** list, uint64_t& computeVal)
{
	std::chrono::steady_clock::time_point t1 = std::chrono::high_resolution_clock::now();
	f(list, computeVal);
	std::chrono::steady_clock::time_point t2 = std::chrono::high_resolution_clock::now();

	elapsedTime = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();

}

void getTimeLapsComputeVariables(size_t& elapsedTime, void (*f)(struct linked_list**, int, uint64_t&), struct linked_list** list, int value, uint64_t& computeVal)
{
	std::chrono::steady_clock::time_point t1 = std::chrono::high_resolution_clock::now();
	f(list,value, computeVal);
	std::chrono::steady_clock::time_point t2 = std::chrono::high_resolution_clock::now();

	elapsedTime = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();
}



int getCountForValidation(struct linked_list* list)
{
	int count = 0;
	while (list != NULL)
	{
		++count;
		list = list->next;
	}
	return count;
}
