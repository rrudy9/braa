#pragma once

#include <linked_list_common_c.h>
#include <common_project.h>

#ifdef TEST_FOUND 
#include "CppUTest/MemoryLeakDetectorMallocMacros.h"
#endif


void popFront(struct linked_list** list, uint64_t& computeVal);

void deleteValFromList(struct linked_list** list, int value, uint64_t& computeVal);

void implementedDeletion(struct linked_list** list, int valToDelete);

void implementedPopFront(struct linked_list** list);

