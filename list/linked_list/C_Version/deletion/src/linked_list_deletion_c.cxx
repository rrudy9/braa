#include "../include/linked_list_deletion_c.h"

void deleteValFromList(struct linked_list** list, int value, uint64_t& computeVal)
{
	struct linked_list* nodeToBeDeleted = *list, * prev = NULL;

	++computeVal;

	if ((*list)->data == value)
	{
		*list = (*list)->next;
		
		printf("Your value was deleted successfully %i\n", nodeToBeDeleted->data);
		free(nodeToBeDeleted);
		return; 
	}

	while (nodeToBeDeleted != NULL && nodeToBeDeleted->data != value)
	{
		prev = nodeToBeDeleted;
		nodeToBeDeleted = nodeToBeDeleted->next;
	}

	if (nodeToBeDeleted == NULL)
	{
		printf("can't find your value, %i\n", value);
		return;
	}
	prev->next = nodeToBeDeleted->next;

	free(nodeToBeDeleted);
}


void popFront(struct linked_list** list, uint64_t& computeVal)
{
	if (!list)
	{
		if (global_log_level <= logLevel::error)
			printf("Stack overflow\n");
		exit(0);
	} 

	if (!(*list)->next)
	{
		free(*list);
		*list = NULL; 
		return;
	}

	struct linked_list* followingChain = *list;

	*list = (*list)->next; 
	free(followingChain);
	
	++computeVal;
}


void implementedDeletion(struct linked_list** list, int valToDelete)
{

	uint64_t computeVal = 0;
	size_t timeElapsed = 0;

	if (global_log_level == logLevel::debug)
		showLinkedList(*list, "Your Allocated linked list");

	getTimeLapsComputeVariables(timeElapsed, deleteValFromList, list, valToDelete, computeVal);
	
	if (global_log_level == logLevel::debug)
		showLinkedList(*list, "\nafter remove selected value from list");
	
	showComputeElapsedTime(computeVal, timeElapsed, "Linked List Deletion", global_show_compute, global_log_level);

}

void implementedPopFront(struct linked_list** list)
{
	uint64_t computeVal = 0;
	size_t timeElapsed = 0;

	if (global_log_level == logLevel::debug)
		showLinkedList(*list, "Your Allocated linked list");

	getTimeLapsComputeVariables(timeElapsed, popFront, list, computeVal);

	if (global_log_level == logLevel::debug)
		showLinkedList(*list, "\nYour list after poping the front");

	showComputeElapsedTime(computeVal, timeElapsed, "Popping Linked List Front", global_show_compute, global_log_level);

}