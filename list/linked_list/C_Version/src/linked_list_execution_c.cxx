#include <stdlib.h>
#include <stdio.h>
#include <common_project.h>
#include <linked_list_deletion_c.h>
#include <linked_list_common_c.h>
#include <linked_list_insertion_c.h>
#include <linked_list_search_c.h>
#include <argtable3.h>
#include <vector>

#ifdef TEST_FOUND 
#include "CppUTest/MemoryLeakDetectorMallocMacros.h"
#endif

bool global_show_compute = false;
logLevel global_log_level = logLevel::info;


int main(int argc, char* argv[])
{

	struct arg_lit
		* help = NULL,
		* showCompute = NULL,
		* showList = NULL,
		* checkResultValidation = NULL,
		* deletionPopFront = NULL;

	struct arg_str
		* logLevelValue = NULL;

	struct arg_int
		* setElementCount = NULL,
		* setRandSeedValue = NULL,
		* search= NULL,
		* insertion = NULL,
		* deletion = NULL;

	struct arg_end* end = NULL;

	struct Common_Project_Values listCommonValue = { 100000 , 30 };
	struct linked_list* list = NULL;

	void* argtable[] = {
		help = arg_litn(NULL, "help", 0, 1, "display this help for Linked List <C Version> and exit"),
		showCompute = arg_litn("c","show-compute",0, 1, "Show Compute"),
		checkResultValidation = arg_litn("t","run-test" ,0, 1, "Run Test for implemented algorithms"),
		logLevelValue = arg_strn("v", "verbose","<info,debug,error,fatal>", 0, 1, "Set Log Level (info, debug, error, fatal) (Default : info)"),
		setElementCount = arg_intn("l", "list-count","<n>" , 0, 1, "Set List Number of element (Default Value: 100k), Doesn't work when n = 0 "),
		setRandSeedValue = arg_intn("r", "rand-seed","<n>", 0, 1, "Set value for Seed Random (Default Value: 30)"),

		search = arg_intn("s", "search","<value>", 0, 1, "Access Linked List C Search Algorithm implementation"),
		insertion = arg_intn("i", "insertion", "<value>" ,0, 1, "Access Linked List C Insertion Algorithm implementation"),
		deletion = arg_intn("d", "deletion","<value>", 0, 1, "Access Linked List C Deletion Algorithm implementation"),
		deletionPopFront = arg_litn("p", "pop", 0, 1, "Pop Front Value of Linked List"),
		showList = arg_litn("z", "show-list", 0, 1, "Show Linked List"),

		end = arg_end(20)
	};


	int exitcode = 0;
	char progname[] = "listC.exe";

	int nerrors;
	nerrors = arg_parse(argc, argv, argtable);

	if (argc == 1) {
		printf("Please choose one of the available commands\n\n");
		arg_print_glossary(stdout, argtable, "  %-25s %s\n");
		exitcode = 0;
		goto exit;
	}

	//If the parser returned any errors then display them and exit 
	if (nerrors > 0)
	{
		// Display the error details contained in the arg_end struct.
		arg_print_errors(stderr, end, progname);
		fprintf(stderr,"Try '%s --help' for more information.\n", progname);
		exitcode = 1;
		goto exit;
	}


	if (help->count > 0)
	{
		printf("Usage: %s", progname);
		arg_print_syntax(stdout, argtable, "\n\n");
		arg_print_glossary(stdout, argtable, "  %-25s %s\n");
		exitcode = 0;
		goto exit;
	}


	if (showCompute->count > 0)
	{
		global_show_compute = !global_show_compute;
	}

	if (logLevelValue->count > 0)
	{
		setLogLevel(*(logLevelValue)->sval, global_log_level);
	}

	//set project common value (random seed , and number of element in array
	if (setElementCount->count > 0)
	{
	//	if (*(setElementCount)->ival == 0) goto exit;

		listCommonValue.elementCount = *(setElementCount)->ival;
	}

	if (setRandSeedValue->count > 0)
	{
		listCommonValue.seedRand = *(setRandSeedValue)->ival;
	}

	srand(listCommonValue.seedRand);
	showCommonProjectValue(listCommonValue, global_log_level);
	createLinkedList(&list, listCommonValue);

	if (search->count > 0)
	{
		implementedSearch(list, *(search)->ival);
	}

	if (insertion->count > 0)
	{
		implementedInsertion(list, *(insertion)->ival);
	}

	if (deletion->count > 0)
	{
		implementedDeletion(&list, *(deletion)->ival);
	}

	if (deletionPopFront->count > 0)
	{
		implementedPopFront(&list);
	}

	//sometimes it will fail if we have same value multiple time 
	// we don't pop anything here (its not a queue, so maybe it has same value but different definition
	// se we keep all values .. 
	if (checkResultValidation->count > 0)
	{

		std::vector<int> elementInListToTest = { };

		struct linked_list* listChain = list;
		while (listChain)
		{
			elementInListToTest.push_back(listChain->data);
			listChain = listChain->next;
		}

		int indexOfRandomVal = (elementInListToTest.size() - 1) % rand();
		int pickUpRandomVal = elementInListToTest[indexOfRandomVal];

		uint64_t cc = 0;
		
		if (!searchLinkedList(list, pickUpRandomVal, cc))
		{
			fprintf(stderr, "Error in search code\n");
			destroyList(list);
			goto exit;
		}

		cc = 0;

		deleteValFromList(&list,pickUpRandomVal,cc);
		if (searchLinkedList(list, pickUpRandomVal, cc))
		{
			fprintf(stderr, "Error in deletion code, your random value was found\n");
			destroyList(list);
			goto exit;
		}

		listChain = list;

		std::vector<int> elementAfterRunningAlgorithm = { };

		while (listChain)
		{
			elementAfterRunningAlgorithm.push_back(listChain->data);
			listChain = listChain->next;
		}

		for (int i = 0; i < elementAfterRunningAlgorithm.size(); ++i)
		{
			if (i < indexOfRandomVal)
			{
				if (elementAfterRunningAlgorithm[i] != elementInListToTest[i])
				{
					fprintf(stderr, "Error in deletion code, some values are not the same\n");
					destroyList(list);
					goto exit;
				}
			}
			else
			{
				if (elementAfterRunningAlgorithm[i] != elementInListToTest[i + 1])
				{
					fprintf(stderr, "Error in deletion code, some values are not the same\n");
					destroyList(list);
					goto exit;
				}
			}
		}

		cc = 0;
		insertValInIndex(&list, pickUpRandomVal, indexOfRandomVal, cc);

		elementAfterRunningAlgorithm = { };

		while (listChain)
		{
			elementAfterRunningAlgorithm.push_back(listChain->data);
			listChain = listChain->next;
		}

		for (int i = 0; i < elementAfterRunningAlgorithm.size(); ++i)
		{
			if (elementAfterRunningAlgorithm[i] != elementInListToTest[i])
			{
				fprintf(stderr, "Error in insertion code, some values are not the same\n");
				destroyList(list);
				goto exit;
			}
		}

		printf("Test Succeeded for algorithms \n");


	}

	if (showList->count > 0)
	{
		showLinkedList(list, "Your Linked List");
	}



	arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
	destroyList(list);

	return exitcode;


exit:
	/* deallocate each non-null entry in argtable[] */
	arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
	return exitcode;
}

