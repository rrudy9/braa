

set (TEST_SOURCES
	linked_list_test_execution_c.cxx
	main.cpp
	)



set(linkingLibrary 
	${CPPUTEST_LDFLAGS}  
	common_project_lib
	linked_list_deletion_c_lib	
	linked_list_insertion_c_lib	
	linked_list_search_c_lib	
	linked_list_common_c_lib
	argtable
	)


set(linkingDirectories 
	"${CPPUTEST_INCLUDE_DIRS}"
	"${PROJECT_SOURCE_DIR}/argtable"
	"${PROJECT_SOURCE_DIR}/common_project/include"
	"${PROJECT_SOURCE_DIR}/list/linked_list/C_Version/common/include" 
	"${PROJECT_SOURCE_DIR}/list/linked_list/C_Version/deletion/include" 
	"${PROJECT_SOURCE_DIR}/list/linked_list/C_Version/search/include" 
	"${PROJECT_SOURCE_DIR}/list/linked_list/C_Version/insertion/include" 
	${CPPUTEST_INCLUDE_DIRS}
	
	)

add_executable(testListC ${TEST_SOURCES})

target_link_libraries(testListC PRIVATE ${linkingLibrary})

target_include_directories(testListC PRIVATE  ${linkingDirectories})


set_property(TARGET testListC PROPERTY FOLDER "list/linked_list/C_Version/test")

set_target_properties(testListC PROPERTIES RUNTIME_OUTPUT_DIRECTORY ${PROJECT_BINARY_DIR}/bin/list/linked_list/c_version)

