#include "CppUTest/TestHarness.h"
#include "CppUTest/MemoryLeakDetectorMallocMacros.h"

#include <common_project.h>
#include <linked_list_deletion_c.h>
#include <linked_list_common_c.h>
#include <linked_list_insertion_c.h>
#include <linked_list_search_c.h>


int getCount(struct linked_list* list)
{
	int count = 0;  
	while (list != NULL)
	{
		++count;
		list = list->next;
	}
	return count;
}


TEST_GROUP(TEST_LINKED_LIST_C)
{
	struct Common_Project_Values listCommonValue = { global_test_count_element, global_test_rand_seed };
	struct linked_list* list = NULL;

	void setup()
	{
		showCommonProjectValue(listCommonValue, global_log_level);
		createLinkedList(&list, listCommonValue);
	}

	void teardown()
	{
		destroyList(list);
	}
};

TEST(TEST_LINKED_LIST_C, search)
{
	int valToSearch = rand() % 10011 + 1;

	if (global_log_level <= logLevel::debug)
		printf("val to search %i\n", valToSearch);

	implementedSearch(list, valToSearch);

	FAIL("force failing for search on purpose");

}





TEST(TEST_LINKED_LIST_C, poppingFront)
{	
	implementedPopFront(&list);

	int checkCount = getCount(list);

	CHECK(checkCount < listCommonValue.elementCount);
}

TEST(TEST_LINKED_LIST_C, insertion)
{
	
	int valToInsert = rand() % 1001 + 1;

	if (global_log_level <= logLevel::debug)
		printf("val to insert %i\n", valToInsert);

	implementedInsertion(list, valToInsert);
	

	int checkCount = getCount(list);

	CHECK(checkCount >= listCommonValue.elementCount);


}

TEST(TEST_LINKED_LIST_C, deletion)
{

	int valToDelete = rand() % 10001 + 1;

	if (global_log_level <= logLevel::debug)
		printf("val to delete %i\n", valToDelete);

	implementedDeletion(&list, valToDelete);

	int checkCount = getCount(list);

	CHECK(checkCount < listCommonValue.elementCount);

}


