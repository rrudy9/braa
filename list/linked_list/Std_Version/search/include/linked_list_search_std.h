#pragma once

#include <linked_list_common_std.h>
#include <common_project.h>

#ifdef TEST_FOUND 
#include "CppUTest/MemoryLeakDetectorMallocMacros.h"
#endif

bool searchForwardList(std::forward_list<int>& list, int value, uint64_t& computeVal);

void implementedSearch(std::forward_list<int>& list, int valToSearch);
