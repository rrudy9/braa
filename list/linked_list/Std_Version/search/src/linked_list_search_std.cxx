#include "../include/linked_list_search_std.h"

bool searchForwardList(std::forward_list<int>& list, int value, uint64_t& computeVal)
{
	for (auto it = list.begin(); it != list.end(); ++it)
	{
		++computeVal;
		if (*it == value)
		{
			return true;
		}
	}
	return false;
}


void implementedSearch(std::forward_list<int>& list, int valToSearch)
{
	if (global_log_level == logLevel::debug)
		showForwardList(list, "Your forward list (std)");

	uint64_t computeVal = 0;
	size_t timeElapsed = 0;

	if (getTimeLapsComputeVariables(timeElapsed, searchForwardList, list, valToSearch, computeVal))
	{
		// we can show index of the value if we pass another var .. 
		if (global_log_level <= logLevel::debug)
			printf("value %i was successfully found\n", valToSearch);
	}
	else
	{
		if (global_log_level <= logLevel::debug)
			printf("value %i was not found\n", valToSearch);
	}

	showComputeElapsedTime(computeVal, timeElapsed, "Forward List (std) Search", global_show_compute, global_log_level);
}