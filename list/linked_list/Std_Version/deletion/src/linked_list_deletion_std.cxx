#include "../include/linked_list_deletion_std.h"

bool deleteValFromList(std::forward_list<int>& list, int value, uint64_t& computeVal)
{
	std::forward_list<int>::iterator check;
	for (auto it = list.begin(); it != list.end(); ++it)
	{
		++computeVal;
		
		if (*it == value)
		{	
			if (it == list.begin())
				list.remove(*it);
			else
				list.erase_after(check);

			return true;
		}
		check = it;
	}

	return false;

}


void implementedDeletion(std::forward_list<int>& fL, int valToDelete)
{
	uint64_t computeVal = 0;
	size_t timeElapsed = 0;

	if (global_log_level == logLevel::debug)
		showForwardList(fL, "Your forward list (std)");

	if (fL.empty())
	{
		printf("List is Empty, you can't delete values , try something else\n");
		
		exit(0);
		return; 
	}
	
	int countBeforeDeletion = 0;
	if (global_result_validation)
		countBeforeDeletion = getCountForValidation(fL);

	if (getTimeLapsComputeVariables(timeElapsed, deleteValFromList, fL, valToDelete, computeVal))
	{
		// we can show index of the value if we pass another var .. 
		if (global_log_level <= logLevel::debug)
			printf("value %i was successfully deleted\n", valToDelete);


		if (global_result_validation)
		{
			int countAfterDeletion = getCountForValidation(fL);
			if (countAfterDeletion < countBeforeDeletion) printf("Testing Result : Deletion Validation\n");
			else printf("error in deletion algorithm\n");
		}
	}
	else
	{
		if (global_log_level <= logLevel::debug)
			printf("value %i was not found\n", valToDelete);
	}

	if (global_log_level == logLevel::debug)
		showForwardList(fL, "Your forward list (std) after deletion");

	showComputeElapsedTime(computeVal, timeElapsed, "Forward List (std) Deletion", global_show_compute, global_log_level);
}

