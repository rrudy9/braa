file(GLOB linked_list_deletion_std 
	.cxx .hxx 
	src/*.cxx 
	include/*.h)

add_library(linked_list_deletion_std_lib ${linked_list_deletion_std})


set(linkingLibrary 
	${CPPUTEST_LDFLAGS}  
	common_project_lib
	linked_list_common_std_lib
	)

set(linkingDirectories 
	"${CPPUTEST_INCLUDE_DIRS}"
	"${PROJECT_SOURCE_DIR}/common_project/include" 
	"${PROJECT_SOURCE_DIR}/list/linked_list/Std_Version/common/include" 
	)

	
if (CPPUTEST_LIBRARIES)
	add_compile_definitions(TEST_FOUND="test")
endif(CPPUTEST_LIBRARIES)

target_link_libraries(linked_list_deletion_std_lib PRIVATE ${linkingLibrary})

target_include_directories(linked_list_deletion_std_lib PRIVATE ${linkingDirectories})

set_property(TARGET linked_list_deletion_std_lib PROPERTY FOLDER "list/linked_list/Std_Version/deletion")
