#include "CppUTest/TestHarness.h"
#include "CppUTest/MemoryLeakDetectorMallocMacros.h"

#include <common_project.h>
#include <linked_list_deletion_std.h>
#include <linked_list_common_std.h>
#include <linked_list_insertion_std.h>
#include <linked_list_search_std.h>


TEST_GROUP(TEST_LINKED_LIST_STD)
{
	struct Common_Project_Values listCommonValue = { global_test_count_element, global_test_rand_seed };
	std::forward_list<int> fL = {};

	void setup()
	{
		showCommonProjectValue(listCommonValue, global_log_level);
		createForwardList(fL, listCommonValue);
	}

	void teardown()
	{
	}
};

TEST(TEST_LINKED_LIST_STD, search)
{
	int valToSearch = rand() % 10011 + 1;

	if (global_log_level <= logLevel::debug)
		printf("val to search %i\n", valToSearch);

	implementedSearch(fL, valToSearch);

	FAIL("force failing for search on purpose");

}


TEST(TEST_LINKED_LIST_STD, insertion)
{

	int valToInsert = rand() % 1001 + 1;

	if (global_log_level <= logLevel::debug)
		printf("val to insert %i\n", valToInsert);

	implementedInsertion(fL, valToInsert);

	CHECK(fL.max_size() >= listCommonValue.elementCount);
}

TEST(TEST_LINKED_LIST_STD, deletion)
{

	int valToDelete = rand() % 10001 + 1;

	if (global_log_level <= logLevel::debug)
		printf("val to delete %i\n", valToDelete);

	implementedDeletion(fL, valToDelete);

	CHECK(fL.max_size() < listCommonValue.elementCount);

}

