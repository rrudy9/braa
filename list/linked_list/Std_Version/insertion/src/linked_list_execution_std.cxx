#include "../include/linked_list_insertion_std.h"

void insertNodePushBack(std::forward_list<int>& fL, int value, uint64_t& computeVal)
{
	auto before_end = fL.before_begin();
	for (int& it : fL)
	{
		++before_end;

		// it will be n value of list until the end (calculate just in case) 
		++computeVal;
	}

	fL.insert_after(before_end, value);

}


void implementedInsertion(std::forward_list<int>& fL, int valToInsert)
{
	size_t timeElasped = 0;
	uint64_t computeVal = 0;

	if (global_log_level == logLevel::debug)
		showForwardList(fL, "Your forward list (std) before insertion");

	int countBeforeInsertion = 0;
	if (global_result_validation)
		countBeforeInsertion = getCountForValidation(fL);

	getTimeLapsComputeVariables(timeElasped, insertNodePushBack, fL, valToInsert, computeVal);

	if (global_result_validation)
	{
		int countAfterInsertion = getCountForValidation(fL);

		if (countAfterInsertion > countBeforeInsertion) printf("Testing Result : Insertion Validation\n");
		else printf("error in insertion algorithm\n");
	}

	if (global_log_level == logLevel::debug)
		showForwardList(fL, "Your forward list (std) after insertion");
	
	showComputeElapsedTime(computeVal, timeElasped, "Forward List (std) Insertion", global_show_compute, global_log_level);

}