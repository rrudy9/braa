#pragma once

#include <linked_list_common_std.h>
#include <common_project.h>

#ifdef TEST_FOUND 
#include "CppUTest/MemoryLeakDetectorMallocMacros.h"
#endif

void insertNodePushBack(std::forward_list<int>& fL, int value, uint64_t& computeVal);


void implementedInsertion(std::forward_list<int>& fL, int valToInsert);
