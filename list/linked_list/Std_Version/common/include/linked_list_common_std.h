#pragma once 

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <ctime>
#include <algorithm>
#include <math.h>
#include <chrono>
#include <forward_list>

#include <common_project.h>

#ifdef TEST_FOUND 
#include "CppUTest/MemoryLeakDetectorMallocMacros.h"
#endif

void createForwardList(std::forward_list<int>& fL, struct Common_Project_Values projectVal);

void showForwardList(std::forward_list<int> fL, char* hint);

bool getTimeLapsComputeVariables(size_t& elapsedTime, bool (*f)(std::forward_list<int>&, int, uint64_t&), std::forward_list<int>& list, int value, uint64_t& computeVal);
void getTimeLapsComputeVariables(size_t& elapsedTime, void (*f)(std::forward_list<int>&, int, uint64_t&), std::forward_list<int>& list, int value, uint64_t& computeVal);

int getCountForValidation(std::forward_list<int> fL);