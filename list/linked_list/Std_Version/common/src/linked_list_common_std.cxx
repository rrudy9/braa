#include "../include/linked_list_common_std.h"

void createForwardList(std::forward_list<int>& fL, struct Common_Project_Values projectVal)
{
	fL = std::forward_list<int>(projectVal.elementCount);

	for (auto it = fL.begin(); it != fL.end(); ++it)
	{
		int randVal = rand() % 10001;
		*it = randVal;
	}
}

void showForwardList(std::forward_list<int> fL, char* hint)
{
	printf("%s\n", hint);
	for (auto it = fL.begin(); it != fL.end(); ++it)
	{
		if (it == fL.begin()) printf("[ ");

		printf("%i,", *it);
	}
	printf("\n");

}

bool getTimeLapsComputeVariables(size_t& elapsedTime, bool (*f)(std::forward_list<int>&, int, uint64_t&), std::forward_list<int>& list, int value, uint64_t& computeVal)
{
	bool valueFound = false;

	std::chrono::steady_clock::time_point t1 = std::chrono::high_resolution_clock::now();
	valueFound = f(list, value, computeVal);
	std::chrono::steady_clock::time_point t2 = std::chrono::high_resolution_clock::now();

	elapsedTime = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();

	return valueFound;
}

void getTimeLapsComputeVariables(size_t& elapsedTime, void (*f)(std::forward_list<int>& , int, uint64_t&), std::forward_list<int>& list, int value, uint64_t& computeVal)
{

	std::chrono::steady_clock::time_point t1 = std::chrono::high_resolution_clock::now();
	f(list, value, computeVal);
	std::chrono::steady_clock::time_point t2 = std::chrono::high_resolution_clock::now();

	elapsedTime = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();

}

int getCountForValidation(std::forward_list<int> fL)
{
	int count = 0;
	 
	for (auto it = fL.begin(); it != fL.end(); ++it)
	{
		++count;
	}
	return count;
}
