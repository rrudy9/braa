#include <stdlib.h>
#include <stdio.h>
#include <common_project.h>
#include <linked_list_deletion_std.h>
#include <linked_list_common_std.h>
#include <linked_list_insertion_std.h>
#include <linked_list_search_std.h>
#include "argtable3.h"


#ifdef TEST_FOUND 
#include "CppUTest/MemoryLeakDetectorMallocMacros.h"
#endif


bool global_show_compute = false;
logLevel global_log_level = logLevel::info;
bool global_result_validation = false;


int main(int argc, char* argv[])
{
	struct arg_lit
		* help = NULL,
		* showList = NULL,
		* checkResultValidation = NULL,
		* showCompute = NULL;
	  
	struct arg_str
		* logLevelValue = NULL;

	struct arg_int
		* setElementCount = NULL,
		* setRandSeedValue = NULL,
		* search = NULL,
		* insertion = NULL,
		* deletion = NULL;
	
	struct arg_end* end = NULL;

	struct Common_Project_Values listCommonValue = { 100000 , 30 };
	std::forward_list<int> fL = {};


	void* argtable[] = {
		help = arg_litn(NULL, "help", 0, 1, "display this help for linked list <Std Version> and exit"),
		showCompute = arg_litn("c","show-compute",0, 1, "Show Compute"),
		checkResultValidation = arg_litn("t","result-validation" ,0, 1, "Show Result Validation for implemented algorithms"),
		logLevelValue = arg_strn("v", "verbose","<info,debug,error,fatal>", 0, 1, "Set Log Level (info, debug, error, fatal) (Default : info)"),
		setElementCount = arg_intn("l", "list-count","<n>" , 0, 1, "Set Linked List Number of element (Default Value: 100k), Doesn't work when n = 0 "),
		setRandSeedValue = arg_intn("r", "rand-seed","<n>", 0, 1, "Set value for Seed Random (Default Value: 30)"),

		search = arg_intn("s", "search","<value>", 0, 1, "Access Linked List Std Search Algorithm implementation"),
		insertion = arg_intn("i", "insertion", "<value>" ,0, 1, "Access Linked List Std Insertion Algorithm implementation"),
		deletion = arg_intn("d", "deletion","<value>", 0, 1, "Access Linked List Std Deletion Algorithm implementation"),
		showList = arg_litn("z", "show-list", 0, 1, "Show Linked List"),

		end = arg_end(20)
	};

	int exitcode = 0;
	char progname[] = "listStd.exe";

	int nerrors;
	nerrors = arg_parse(argc, argv, argtable);

	if (argc == 1) {
		printf("Please choose one of the available commands\n\n");
		arg_print_glossary(stdout, argtable, "  %-25s %s\n");
		exitcode = 0;
		goto exit;
	}

	//If the parser returned any errors then display them and exit 
	if (nerrors > 0)
	{
		// Display the error details contained in the arg_end struct.
		arg_print_errors(stderr, end, progname);
		fprintf(stderr,"Try '%s --help' for more information.\n", progname);
		exitcode = 1;
		goto exit;
	}


	if (help->count > 0)
	{
		printf("Usage: %s", progname);
		arg_print_syntax(stdout, argtable, "\n\n");
		arg_print_glossary(stdout, argtable, "  %-25s %s\n");
		exitcode = 0;
		goto exit;
	}


	if (showCompute->count > 0)
	{
		global_show_compute = !global_show_compute;
	}

	if (checkResultValidation->count > 0)
	{
		global_result_validation = !global_result_validation;
	}

	if (logLevelValue->count > 0)
	{
		setLogLevel(*(logLevelValue)->sval, global_log_level);
	}

	//set project common value (random seed , and number of element in array
	if (setElementCount->count > 0)
	{
		if (*(setElementCount)->ival == 0) goto exit;

		listCommonValue.elementCount = *(setElementCount)->ival;
	}

	if (setRandSeedValue->count > 0)
	{
		listCommonValue.seedRand = *(setRandSeedValue)->ival;
	}

	srand(listCommonValue.seedRand);
	showCommonProjectValue(listCommonValue, global_log_level);
	createForwardList(fL, listCommonValue);

	if (search->count > 0)
	{
		implementedSearch(fL, *(search)->ival);
	}

	if (insertion->count > 0)
	{
		implementedInsertion(fL, *(insertion)->ival);
	}

	if (deletion->count > 0)
	{
		implementedDeletion(fL, *(deletion)->ival);
	}

	if (showList->count > 0)
	{
		showForwardList(fL, "Your Linked List");
	}



	arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
	return exitcode;

exit:
	/* deallocate each non-null entry in argtable[] */
	arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
	return exitcode;

}
