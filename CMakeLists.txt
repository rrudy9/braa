cmake_minimum_required(VERSION 3.10)


if(MSVC)
  #add_definitions(-D_CRT_SECURE_NO_WARNINGS)
endif()


project(Liste_Dev)

if (DEFINED ENV{CPPUTEST_HOME})
	message(STATUS "Using CppUTest home: $ENV{CPPUTEST_HOME} ")
	set(CPPUTEST_INCLUDE_DIRS $ENV{CPPUTEST_HOME}/include)
	set(CPPUTEST_LIBRARIES $ENV{CPPUTEST_HOME}/lib)
	set(CPPUTEST_LDFLAGS CppUTestd )
	set(CMAKE_C_FLAGS $ENV{CPPUTEST_HOME}/include/CppUTest/MemoryLeakDetectorMallocMacros.h)
	set(CMAKE_CXX_FLAGS $ENV{CPPUTEST_HOME}/include/CppUTest/MemoryLeakDetectorNewMacros.h)
else()
	message(STATUS "Warning ! You must install CppUTest to run tests -- Warning")
endif()



set_property(GLOBAL PROPERTY USE_FOLDERS ON)


link_directories(${CPPUTEST_LIBRARIES})

add_executable(Liste_Dev src/main.cpp)


add_subdirectory(argtable)
add_subdirectory(common_project)
add_subdirectory(array)
add_subdirectory(list)
add_subdirectory(fifo)  
add_subdirectory(tree)
add_subdirectory(hash_table)
add_subdirectory(graph)


set(linkingLibrary 
	${CPPUTEST_LDFLAGS}  
	argtable
	common_project_lib
	)

set(linkingDirectories 
	"${PROJECT_SOURCE_DIR}/argtable"
	"${PROJECT_SOURCE_DIR}/common_project/include"
	"${CPPUTEST_INCLUDE_DIRS}"
	)

add_compile_definitions(BINARY_DIR="${PROJECT_BINARY_DIR}/bin/")

target_link_libraries(Liste_Dev PRIVATE ${linkingLibrary})

target_include_directories(Liste_Dev PRIVATE ${linkingDirectories})
 

# invalid macro definition with CPPUTEST while using the command below , if cpputest is not defined, it works perfectly 

#set_target_properties(Liste_Dev PROPERTIES COMPILE_FLAGS "-DRR")

if (CPPUTEST_LIBRARIES)
	#the command below also conflict with CPPUTEST
	#add_definitions(-DTEST_FOUND)
	add_compile_definitions(TEST_FOUND="test")
endif(CPPUTEST_LIBRARIES)


